package com.reminder.app.service;

public interface EventsName {
    String ACTION_CONNECT = "ACTION_CONNECT";
    String ACTION_DISCONNECT = "ACTION_DISCONNECT";
    String ACTION_ERROR = "ACTION_ERROR";
    String ACTION_TIMEOUT = "ACTION_TIMEOUT";

    String INBOX_LIST = "inbox_list";
    String GET_MESSAGE_HISTORY = "get_message_history";
    String SEND_MESSAGE = "send_message";

    String GET_USER_THREAD = "get_user_thread";
    String GET_OTHER_PROFILE = "get_other_profile";
    String USER_BLOCK_UNBLOCK = "user_block_unblock";
    String CHECK_USER_STATUS = "check_user_status";
    String READ_MESSAGE_UPDATE = "read_message_update";

    String INBOX_LIST_RESPONSE = "inbox_list_response";
    String GET_MESSAGE_HISTORY_RESPONSE = "get_message_history_response";

    String MULTI_USER = "multi_user";
    String RECEIVE_MESSAGE = "receive_message";
    String USER_BLOCK_UNBLOCK_RESPONSE = "user_block_unblock_response";
    String GET_OTHER_PROFILE_RESPONSE = "get_other_profile_response";
    String GET_USER_THREAD_RESPONSE = "get_user_thread_response";
    String CHECK_USER_STATUS_RESPONSE = "check_user_status_response";
    String READ_MESSAGE_UPDATE_RESPONSE = "read_message_update_response";
}