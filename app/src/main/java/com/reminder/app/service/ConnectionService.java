package com.reminder.app.service;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reminder.app.R;
import com.reminder.app.activity.HomeActivity;
import com.reminder.app.activity.MessageActivity;
import com.reminder.app.models.EventBusModel;
import com.reminder.app.models.MessageModel;
import com.reminder.app.models.UserResponse;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by rgg on 18/12/2020.
 */
public class ConnectionService extends Service {
    public static final String TAG = "ConnectionService***";

    Timer timer;

    private void postEvent(String action, String response) {
        Log.d(TAG, action + "\n" + response);

        EventBusModel model = new EventBusModel();
        model.setAction(action);
        model.setResponse(response);
        EventBus.getDefault().post(model);
    }

    private Thread mThread;

    @Override
    public void onDestroy() {
        stopSocket();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startTimer();
        return Service.START_NOT_STICKY;
    }

    public void startTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                startSocket();
            }
        }, 0, 10000);
    }


    void doSocketWork() {
        try {
            Socket socket = MainApp.getInstance().getSocket();
            if ((socket != null && !socket.connected())) {
                Log.d("rgg", "trying socket connection");
                stopSocket();
                socket.on(Socket.EVENT_CONNECT, eventConnect);
                socket.on(Socket.EVENT_DISCONNECT, eventDisconnect);
                socket.on(Socket.EVENT_CONNECT_ERROR, eventError);
                socket.on(Socket.EVENT_CONNECT_TIMEOUT, eventConnectionTimeOut);

                socket.on(EventsName.INBOX_LIST_RESPONSE, inbox_list_response);
                socket.on(EventsName.GET_MESSAGE_HISTORY_RESPONSE, get_message_history_response);

                socket.on(EventsName.GET_USER_THREAD_RESPONSE, get_user_thread_response);
//                socket.on(EventsName.MULTI_USER, receive_message);
                socket.on(EventsName.RECEIVE_MESSAGE, receive_message);
                socket.on(EventsName.GET_OTHER_PROFILE_RESPONSE, get_other_profile_response);
                socket.on(EventsName.USER_BLOCK_UNBLOCK_RESPONSE, user_block_unblock_response);
                socket.on(EventsName.CHECK_USER_STATUS_RESPONSE, check_user_status_response);
                socket.on(EventsName.READ_MESSAGE_UPDATE_RESPONSE, read_message_update_response);

                socket.connect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startSocket() {
        if (mThread != null) {
            mThread.interrupt();
            mThread = null;
        }
        mThread = new Thread(this::doSocketWork);
        mThread.start();
    }

    public void stopSocket() {
        Socket socket = MainApp.getInstance().getSocket();
        if (socket != null) {
            UserResponse userModel = new Gson().fromJson(Common.getPreferences(this, "data"), UserResponse.class);
            if (!TextUtils.isEmpty(userModel.getData().getId()+"")) {
                socket.disconnect();
                socket.off(Socket.EVENT_CONNECT, eventConnect);
                socket.off(Socket.EVENT_DISCONNECT, eventDisconnect);
                socket.off(Socket.EVENT_CONNECT_ERROR, eventError);
                socket.off(Socket.EVENT_CONNECT_TIMEOUT, eventConnectionTimeOut);

                socket.off(EventsName.INBOX_LIST_RESPONSE, inbox_list_response);
                socket.off(EventsName.GET_MESSAGE_HISTORY_RESPONSE, get_message_history_response);

                socket.off(EventsName.GET_USER_THREAD_RESPONSE, get_user_thread_response);
//                socket.off(EventsName.MULTI_USER, receive_message);
                socket.off(EventsName.RECEIVE_MESSAGE, receive_message);
                socket.off(EventsName.GET_OTHER_PROFILE_RESPONSE, get_other_profile_response);
                socket.off(EventsName.USER_BLOCK_UNBLOCK_RESPONSE, user_block_unblock_response);
                socket.off(EventsName.CHECK_USER_STATUS_RESPONSE, check_user_status_response);
                socket.off(EventsName.READ_MESSAGE_UPDATE_RESPONSE, read_message_update_response);
            }
        }
    }

    Emitter.Listener get_message_history_response = args -> {
        if (args[0] != null) {
            postEvent(EventsName.GET_MESSAGE_HISTORY_RESPONSE, args[0].toString());
        }
    };

    Emitter.Listener get_user_thread_response = args -> {
        if (args[0] != null) {
            postEvent(EventsName.GET_USER_THREAD_RESPONSE, args[0].toString());
        }
    };

    Emitter.Listener get_other_profile_response = args -> {
        if (args[0] != null) {
            postEvent(EventsName.GET_OTHER_PROFILE_RESPONSE, args[0].toString());
        }
    };

    Emitter.Listener check_user_status_response = args -> {
        if (args[0] != null) {
            postEvent(EventsName.CHECK_USER_STATUS_RESPONSE, args[0].toString());
        }
    };

    Emitter.Listener read_message_update_response = args -> {
        if (args[0] != null) {
            postEvent(EventsName.READ_MESSAGE_UPDATE_RESPONSE, args[0].toString());
        }
    };

    Emitter.Listener user_block_unblock_response = args -> {
        if (args[0] != null) {
            postEvent(EventsName.USER_BLOCK_UNBLOCK_RESPONSE, args[0].toString());
        }
    };
    Emitter.Listener inbox_list_response = args -> {
        if (args[0] != null) {
            postEvent(EventsName.INBOX_LIST_RESPONSE, args[0].toString());
        }
    };

    Emitter.Listener receive_message = args -> {
        if (args[0] != null) {
            postEvent(EventsName.RECEIVE_MESSAGE, args[0].toString());


            UserResponse userModel = new Gson().fromJson(Common.getPreferences(this, "data"), UserResponse.class);


            MessageModel resultModel = new Gson().fromJson(args[0].toString()
                    , new TypeToken<MessageModel>() {
                    }.getType());


            boolean isSameUser = userModel.getData().getId()==resultModel.getUserId();
            if (isSameUser)
                return;
//            if (!TextUtils.isEmpty(userModel.getId()) && !TextUtils.isEmpty(resultModel.getOtherUserId()))
//                if (!userModel.getId().equals(resultModel.getUserId()+""))
//            todo enable notification
//            sendNotification(resultModel.getName(), resultModel.getMessage(), resultModel.getUserId() + "");
        }
    };
    String CHANNEL_ID = "channel_02";

    private void sendNotification(String username, String msg, String otherUserId) {
        // Get an instance of the Notification manager
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH);

            mChannel.setShowBadge(true);
            mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }

        // Create an explicit content Intent that starts the main Activity.
        Intent notificationIntent = new Intent(getApplicationContext(), MessageActivity.class);
        notificationIntent.putExtra(Common.socketParameter.other_user_image, otherUserId);
        notificationIntent.putExtra(Common.socketParameter.other_user_name, username);
        // TODO: 18-Feb-21 add mobile;
        notificationIntent.putExtra(Common.socketParameter.other_user_image, "8952886955");

        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(HomeActivity.class);

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);

        // Get a PendingIntent containing the entire back stack.
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get a notification builder that's compatible with platform versions >= 4
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);

        // Define the notification settings.
        builder.setSmallIcon(R.mipmap.ic_launcher)
                // In a real app, you may want to use a library like Volley
                // to decode the Bitmap.
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setColor(Color.RED)
                .setContentTitle(username)
                .setContentText(msg)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setContentIntent(notificationPendingIntent)
                .setPriority(Notification.PRIORITY_MAX);
        if (Build.VERSION.SDK_INT >= 21) builder.setVibrate(new long[0]);

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        // Dismiss notification once the user touches it.
        builder.setAutoCancel(true);

        // Issue the notification
        mNotificationManager.notify((int) (Math.random() * 50 + 1), builder.build());
    }

    Emitter.Listener eventDisconnect = args -> {
        postEvent(EventsName.ACTION_DISCONNECT, null);
    };

    Emitter.Listener eventError = args -> {
        postEvent(EventsName.ACTION_ERROR, null);
    };

    Emitter.Listener eventConnectionTimeOut = args -> {
        postEvent(EventsName.ACTION_TIMEOUT, null);
    };

    Emitter.Listener eventConnect = args -> {
        postEvent(EventsName.ACTION_CONNECT, null);
    };
}