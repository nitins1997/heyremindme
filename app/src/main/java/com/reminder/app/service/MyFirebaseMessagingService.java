package com.reminder.app.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.reminder.app.R;
import com.reminder.app.activity.MessageActivity;
import com.reminder.app.activity.Splash;
import com.reminder.app.utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static final String TAG = "firebase";
    String vibration = "";
    private Intent notificationIntent;
    private String type;
    private String title;
    private String body;

    private Uri defaultSound;

    private NotificationCompat.Builder notificationBuilder;

    public MyFirebaseMessagingService() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        return;
                    }
                    String token = task.getResult().getToken();

                });
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("newToken", s);
        Common.setIntro(getApplicationContext(), "device_token", s);


    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String Fcmdata = remoteMessage.getData().toString();
        Log.d("remoteMessage", Fcmdata);

        if (remoteMessage.getData().size() > 0) {
            JSONObject js = null;
            try {
                js = new JSONObject(remoteMessage.getData().toString());
                type = js.getJSONObject("data").getString("type");
                notificationIntent = new Intent(this, Splash.class);
                try{
                    notificationIntent.putExtra("reminderdata",js.getJSONObject("data").getString("reminderdata"));
                    notificationIntent.putExtra("type","noti");
                }catch (Exception e){
                    e.printStackTrace();
                }
                showNotification(js, type);
            } catch (JSONException e) {
                e.printStackTrace();

                try {
                    if (remoteMessage.getData().get("type").equals("CHAT_MESSAGE")) {
                        if (!MessageActivity.is_show) {
//                            if (Common.isAppIsInBackground(getApplicationContext())) {
                                showNotificationChat(remoteMessage, type);
//                            }
                        }
                    }


                } catch (Exception w) {
                    w.printStackTrace();
                }


            }


//            if (type.equalsIgnoreCase("Set Reminder")) {

//            }

        }
    }

    private void showNotificationChat(RemoteMessage remoteMessage, String type) {
        int notiId = -1;
//        Log.d("remoteMessage", remoteMessage.toString());

        title = remoteMessage.getData().get("sender_name");
        body = remoteMessage.getData().get("body");


        Intent notificationIntent = new Intent(getApplicationContext(), MessageActivity.class);
        notificationIntent.putExtra(Common.socketParameter.other_user_id, remoteMessage.getData().get("sender_id"));
        notificationIntent.putExtra(Common.socketParameter.other_user_name, remoteMessage.getData().get("sender_name"));
        // TODO: 18-Feb-21 add mobile;
        notificationIntent.putExtra(Common.socketParameter.other_user_image, remoteMessage.getData().get("sender_image"));

        final PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis() /* Request code */, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Random rand = new Random();
        int random = rand.nextInt(1000);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = getString(R.string.default_notification_channel_id) + random;


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();


            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "BabbleBox", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("BabbleBox");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);

            notificationChannel.enableVibration(true);
            if (vibration.equals("1")) {
                notificationChannel.setVibrationPattern(new long[]{500, 500, 500, 500, 500});//for good vibration
                notificationChannel.enableVibration(true);
            } else {
                notificationChannel.setVibrationPattern(new long[]{0L});// for no vibration
                notificationChannel.enableVibration(false);
            }

            defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationChannel.setSound(defaultSound, audioAttributes);

//            notificationChannel.setVibrationPattern(new long[]{0});
//
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.spalsh)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentText(body)
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_MAX);

        if (vibration.equals("1")) {
            notificationBuilder.setVibrate(new long[]{500, 500, 500, 500, 500});//for good vibration
        } else {
            notificationBuilder.setVibrate(new long[]{0L});// for no vibration
        }


        defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSound(defaultSound);


        if (notiId == -1) {
            notificationManager.notify(/*notification id*/random, notificationBuilder.build());
        } else {
            notificationManager.notify(/*notification id*/notiId, notificationBuilder.build());
        }


    }


    private void showNotification(JSONObject remoteMessage, String type) {
        int notiId = -1;
//        Log.d("remoteMessage", remoteMessage.toString());
        try {
            JSONObject notificationObj = remoteMessage.getJSONObject("notification");

            title = notificationObj.getString("title");
            body = notificationObj.getString("body");

            final PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis() /* Request code */, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Random rand = new Random();
            int random = rand.nextInt(1000);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String NOTIFICATION_CHANNEL_ID = getString(R.string.default_notification_channel_id) + random;


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .build();


                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "BabbleBox", NotificationManager.IMPORTANCE_DEFAULT);
                notificationChannel.setDescription("BabbleBox");
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);

                notificationChannel.enableVibration(true);
                if (vibration.equals("1")) {
                    notificationChannel.setVibrationPattern(new long[]{500, 500, 500, 500, 500});//for good vibration
                    notificationChannel.enableVibration(true);
                } else {
                    notificationChannel.setVibrationPattern(new long[]{0L});// for no vibration
                    notificationChannel.enableVibration(false);
                }

                defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                notificationChannel.setSound(defaultSound, audioAttributes);

//            notificationChannel.setVibrationPattern(new long[]{0});
//
                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.spalsh)
                    .setContentTitle(title)
                    .setAutoCancel(true)
                    .setColor(getResources().getColor(R.color.colorPrimary))
                    .setContentText(body)
                    .setContentIntent(pendingIntent)
                    .setWhen(System.currentTimeMillis())
                    .setPriority(Notification.PRIORITY_MAX);

            if (vibration.equals("1")) {
                notificationBuilder.setVibrate(new long[]{500, 500, 500, 500, 500});//for good vibration
            } else {
                notificationBuilder.setVibrate(new long[]{0L});// for no vibration
            }


            defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationBuilder.setSound(defaultSound);


            if (notiId == -1) {
                notificationManager.notify(/*notification id*/random, notificationBuilder.build());
            } else {
                notificationManager.notify(/*notification id*/notiId, notificationBuilder.build());
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
