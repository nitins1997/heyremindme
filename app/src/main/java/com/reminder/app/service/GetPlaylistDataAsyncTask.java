package com.reminder.app.service;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.PlaylistListResponse;
import com.reminder.app.utils.Constant;

import java.io.IOException;

public class GetPlaylistDataAsyncTask extends AsyncTask<String[], Void, PlaylistListResponse> {
    private static final String YOUTUBE_PLAYLIST_PART = "snippet";
    private static final String YOUTUBE_PLAYLIST_FIELDS = "items(snippet(title))";

    private YouTube mYouTubeDataApi;

    public GetPlaylistDataAsyncTask(YouTube api) {
        mYouTubeDataApi = api;
    }

    @Override
    protected PlaylistListResponse doInBackground(String[]... params) {

        final String[] playlistIds = params[0];

        PlaylistListResponse playlistListResponse;
        try {
            playlistListResponse = mYouTubeDataApi.playlists()
                    .list(YOUTUBE_PLAYLIST_PART)
//                    .setId(TextUtils.join(",", playlistIds))
//                    .setFields(YOUTUBE_PLAYLIST_FIELDS)
                    .setPart("snippet")
                    .setOauthToken("641543783059-notcces64hou57qkvbgc8q4je4plv7c2.apps.googleusercontent.com")
                    .setKey(Constant.YOUTUBE_KEY) //Here you will have to provide the keys
                    .setMine(true) //Here you will have to provide the keys
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return playlistListResponse;
    }
}