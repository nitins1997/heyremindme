package com.reminder.app.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.BindingAdapter;

import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.reminder.app.R;
import com.reminder.app.models.UserResponse;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Common {

    public static void openLocation(Activity activity, String lat, String lng, String title) {
        String geoUri = "http://maps.google.com/maps?q=loc:" + lat + "," + lng + " (" + title + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
        activity.startActivity(intent);
    }
    public interface DateFormatsApp {
        String DEFAULT_DATE = "dd MMM yyyy";
        String DEFAULT_TIME = "hh:mm a";
        String DEFAULT_DATE_TIME = "dd MMM yyyy h:mm a";
    }

    public static String getCurrentTimeZone(){
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        return tz.getID();
    }

    @BindingAdapter({ "android:reminderId","android:status"})
    public static void editVisiblity(ImageView textView,int editVisiblity,int status){
        UserResponse userResponse = new Gson().fromJson(Common.getPreferences(textView.getContext(), "data"), UserResponse.class);
        if(userResponse.getData().getId()==editVisiblity){
            if(status==2){
                textView.setVisibility(View.GONE);
            }else{
                textView.setVisibility(View.VISIBLE);
            }

        }else {
            textView.setVisibility(View.GONE);
        }

    }
    //time picker
    public static void
    timePicker(Activity activity, TextView textView, String date) {
        // TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(activity, (timePicker, selectedHour, selectedMinute) -> {
            if (Common.FormatDate(Common.getCurrentDate2()).equals(date)) {
                Calendar datetime = Calendar.getInstance();
                Calendar c = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, selectedMinute);
                if (datetime.getTimeInMillis() >= c.getTimeInMillis()) {
                    String min = "";
                    if (selectedMinute < 10) {
                        min = "0" + selectedMinute;
                    } else {
                        min = String.valueOf(selectedMinute);
                    }

                    textView.setText(formatDateList(selectedHour + ":" + min));
                } else {
                    //it's before current'
                    Toast.makeText(activity, "Please Select Future Time.", Toast.LENGTH_LONG).show();
                }

            } else {
                String min = "";
                if (selectedMinute < 10) {
                    min = "0" + selectedMinute;
                } else {
                    min = String.valueOf(selectedMinute);
                }

                textView.setText(formatDateList(selectedHour + ":" + min));
            }


        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");

        mTimePicker.show();
    }


    //date Picker
    public static void dateSelection(Activity activity, TextView textView, TextView timetect) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        @SuppressLint("SetTextI18n") DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                (view1, year, monthOfYear, dayOfMonth) -> {
                    textView.setText(FormatDate(year + "/" + formatday(monthOfYear + 1) + "/" + formatday(dayOfMonth)));
                    timetect.setText("");
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public static String formatday(int day) {
        if (day < 10) {
            return "0" + day;
        } else {
            return "" + day;
        }
    }

    //open google voice typing
    public static void voiceTyping(Activity activity) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        activity.startActivityForResult(intent, Common.onActivityCode.requestCodeSpeech);
    }

    public static void shareText(Activity activity, String text) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("*/*");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(text) + " \n \n Hey please check this application " + "https://play.google.com/store/apps/details?id=com.babblebox.app");
        activity.startActivity(Intent.createChooser(i, "Share"));
    }

    public static long calculateDays(String fromDate, String toDate) {
        long diff = 0;
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        String inputString1 = fromDate;
        String inputString2 = toDate;

        try {
            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            diff = date2.getTime() - date1.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    //hide the keypad of mobile device
    public static void hideKeyboard(Activity _activity) {

        try {
            View view = _activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) _activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //show snake bar  message
    public static void showSnackbar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }

    //show toast message
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /*    this method return the status of internet connectivity*/
    public static boolean getConnectivityStatus(Context activity) {
        try {
            ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = connManager.getActiveNetworkInfo();
            if (info != null)
                return info.isConnected();
            else
                return false;
        } catch (Exception e) {
            return false;
        }

    }

    public static boolean isValidEmail(String email) {

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        return Pattern.compile(EMAIL_STRING).matcher(email).matches();

    }


    //********************Validation********************

    public static boolean isValidMobile(String phone) {
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() == 10;
        }
        return false;
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    /***************************************************/

    public static void INIT_PIX_WITH_OPTIONS(AppCompatActivity activity, int code) {
        Options options = Options.init()
                .setRequestCode(code)                                                 //Request code for activity results
                .setCount(5)                                                         //Number of images to restict selection count
                .setFrontfacing(false)                                                //Front Facing camera on start
                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
                .setPath("/babblebox/images");

        Pix.start(activity, options);
    }

    public static void INIT_PIX(AppCompatActivity activity, int code) {
        Pix.start(activity, Options.init().setRequestCode(code));
    }

    public static void INIT_PICASSO(Activity activity, String path, ImageView iv) {
        try {
            String imagePath = "";
            if (path.contains("http")) {
                imagePath = path;
            } else {
                imagePath = Constant.BASE_URL + path;
            }

            Log.d("imagePath", imagePath);
            Picasso.with(activity).load(imagePath)
                    .error(R.drawable.ic_launcher_background)
                    .placeholder(R.drawable.ic_launcher_background)
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(iv);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public static void INIT_CONTACT_PICASSO(Activity activity, String path, ImageView iv) {
        try {
            String imagePath = "";
            if (path.contains("http")) {
                imagePath = path;
            } else {
                imagePath = Constant.BASE_URL + path;
            }

            Log.d("imagePath", imagePath);
            Picasso.with(activity).load(imagePath)
                    .error(R.drawable.ic_user_profile)
                    .placeholder(R.drawable.ic_user_profile)
                    .into(iv);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }


    public static void INIT_ROUND_PICASSO(Activity activity, String path, ImageView iv) {

        String imagePath = "";
        if (path.contains("http")) {
            imagePath = path;
        } else {
            imagePath = Constant.BASE_URL + path;
        }

        Log.d("imagePath", imagePath);

        Picasso.with(activity).load(imagePath)
                .error(R.drawable.ic_launcher_background)
                .placeholder(R.drawable.ic_launcher_background)
//                .transform(new RoundedTransformation(65, 0))

//                .fit()  // Fix centerCrop issue: http://stackoverflow.com/a/20824141/1936697
//                .centerCrop()
                .into(iv);
    }

    public static void INIT_CIRCLE_PICASSO(Activity activity, String path, ImageView iv) {
        try {
            String imagePath = "";
            if (path.contains("http")) {
                imagePath = path;
            } else {
                imagePath = Constant.BASE_URL + path;
            }

            Picasso.with(activity).load(imagePath)
                    .error(R.drawable.ic_launcher_background)
                    .placeholder(R.drawable.ic_launcher_background)
                    .transform(new CircleTransform())
                    .into(iv);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public static void INIT_CIRCLE_CONTACT_PICASSO(Activity activity, String path, ImageView iv) {
        try {
            String imagePath = "";
            if (path.contains("http")) {
                imagePath = path;
            } else {
                imagePath = Constant.BASE_URL + path;
            }

            Picasso.with(activity).load(imagePath)
                    .error(R.drawable.ic_launcher_background)
                    .placeholder(R.drawable.ic_launcher_background)
                    .transform(new CircleTransform())
                    .into(iv);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public static void INIT_GROUP_PICASSO(Activity activity, String path, ImageView iv) {
        try {
            String imagePath = "";
            if (path.contains("http")) {
                imagePath = path;
            } else {
                imagePath = Constant.BASE_URL + path;
            }

            Picasso.with(activity).load(imagePath)
//                    .error(R.drawable.ic_group)
                    .error(R.drawable.ic_launcher_background)

//                    .placeholder(R.drawable.ic_group)
                    .placeholder(R.drawable.ic_launcher_background)
                    .transform(new CircleTransform())
                    .into(iv);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public static void INIT_DEFAULT_GROUP_PICASSO(Activity activity, String path, ImageView iv) {
        try {
            String imagePath = "";
            if (path.contains("http")) {
                imagePath = path;
            } else {
                imagePath = Constant.BASE_URL + path;
            }

            Picasso.with(activity).load(imagePath)
                    .error(R.drawable.ic_launcher_background)

                    .placeholder(R.drawable.ic_launcher_background)
                    .transform(new CircleTransform())
                    .into(iv);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public static void Contact(Activity activity, String path, ImageView iv) {
        try {
            String imagePath = "";
            if (path.contains("http")) {
                imagePath = path;
            } else {
                imagePath = Constant.BASE_URL + path;
            }

            Picasso.with(activity).load(imagePath)
                    .error(R.drawable.ic_user_profile)
                    .placeholder(R.drawable.ic_user_profile)
                    .transform(new CircleTransform())
                    .into(iv);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public static void share(Activity activity) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "text");
        activity.startActivity(Intent.createChooser(sharingIntent, "Share with"));
    }

    public static void setIntro(Context con, String key, String value) {
        SharedPreferences preferences = con.getSharedPreferences("prefs_firstime", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getIntro(Context con, String key) {
        SharedPreferences sharedPreferences = con.getSharedPreferences("prefs_firstime", 0);
        String value = sharedPreferences.getString(key, "0");
        return value;

    }

    public static void setPreferences(Context con, String key, String value) {
        SharedPreferences preferences = con.getSharedPreferences("prefs_login", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void clearPreferences(Context con) {
        SharedPreferences preferences = con.getSharedPreferences("prefs_login", Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

    public static String getPreferences(Context con, String key) {
        SharedPreferences sharedPreferences = con.getSharedPreferences("prefs_login", 0);
        String value = sharedPreferences.getString(key, "0");
        return value;

    }

    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static boolean isServiceRunning(Class<?> backgroundLocationServiceClass, Context activity) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (backgroundLocationServiceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /*  convert  2018-12-13 to 13-12-2018 format*/
    public static String formatDateBooking(String updated_at) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date newDate = null;
        try {
            newDate = format.parse(updated_at);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault());
        String date = format.format(newDate);

        return date;
    }

    public static String getFileNameFromURL(String url) {
        if (url == null) {
            return "";
        }
        try {
            URL resource = new URL(url);
            String host = resource.getHost();
            if (host.length() > 0 && url.endsWith(host)) {
                // handle ...example.com
                return "";
            }
        } catch (MalformedURLException e) {
            return "";
        }

        int startIndex = url.lastIndexOf('/') + 1;
        int length = url.length();

        // find end index for ?
        int lastQMPos = url.lastIndexOf('?');
        if (lastQMPos == -1) {
            lastQMPos = length;
        }

        // find end index for #
        int lastHashPos = url.lastIndexOf('#');
        if (lastHashPos == -1) {
            lastHashPos = length;
        }

        // calculate the end index
        int endIndex = Math.min(lastQMPos, lastHashPos);
        return url.substring(startIndex, endIndex);
    }

    public static String FormatDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
        Date newDate = null;
        try {
            newDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault());
        String date2 = format.format(newDate);
        return date2;
    }


    @BindingAdapter({"android:imageFromUrl"})
    public static void setImageXml(ImageView imageView, String path) {
        try {
            String imagePath = "";
            if (path != null) {
                if (path.contains("http")) {
                    imagePath = path;
                } else {
                    imagePath = Constant.BASE_URL + path;
                }

                Log.d("imagePath", imagePath);
                Picasso.with(imageView.getContext()).load(imagePath)
                        .error(R.drawable.ic_user_profile)
                        .placeholder(R.drawable.ic_user_profile)
                        .into(imageView);
            } else {
                imageView.setImageResource(R.drawable.ic_user_profile);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @BindingAdapter({"android:imageFromYoutubeUrl"})
    public static void imageFromYoutubeUrl(ImageView imageView, String path) {
        try {
            String imagePath = "";
            if (path != null) {
                if (path.contains("http")) {
                    imagePath = path;
                } else {
                    imagePath = Constant.BASE_URL + path;
                }

                Log.d("imagePath", imagePath);
                Picasso.with(imageView.getContext()).load(imagePath)
                        .error(R.drawable.ic_youtube)
                        .placeholder(R.drawable.ic_youtube)
                        .into(imageView);
            } else {
                imageView.setImageResource(R.drawable.ic_youtube);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @BindingAdapter({"android:showCreatedBy"})
    public static void showCreatedBy(TextView imageView, int userId) {
        try {
            UserResponse userResponse = new Gson().fromJson(Common.getPreferences(imageView.getContext(), "data"), UserResponse.class);
            if (userResponse.getData().getId() == userId) {
                imageView.setVisibility(View.GONE);
            } else {
                imageView.setVisibility(View.VISIBLE);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }


    public static String FormatDateList(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date newDate = null;
        try {
            newDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault());
        String date2 = format.format(newDate);
        return date2;
    }


    public static String FormatDateApi(String date) {
        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault());
        Date newDate = null;
        try {
            newDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return format.format(newDate);
    }

    public static String FormatDateUtcApi(String date) {
        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd, yyyyhh:mm a", Locale.ENGLISH);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));


        Date newDate = null;
        try {
            newDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date2 = format.format(newDate);
        return date2;
    }


    public static String formatDateForApi(String created) {
//        Calendar cal = Calendar.getInstance();
//        TimeZone tz = cal.getTimeZone();
//        int timeSaving=tz.getDSTSavings();

        DateFormat outputFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        outputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        DateFormat inputFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        Date date = null;
        try {
            date = inputFormat.parse(created);
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(date);
//            cal2.add(Calendar.HOUR,-timeSaving);
//            cal2.add(Calendar.MILLISECOND,-timeSaving);

            String outputDateStr = outputFormat.format(cal2.getTime());
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "error";
        }
    }


    public static String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }


    public static String formatCommentDate(String updated_at) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Date newDate = null;
        try {
            newDate = format.parse(updated_at);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("EEEE, MMMM dd", Locale.getDefault());
        String date = format.format(newDate);

        return "Today is " + date;
    }


    public static String formatCommentDate2(String updated_at) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date newDate = null;
        try {
            newDate = format.parse(updated_at);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault());
        String date = format.format(newDate);

        return date;
    }

    /*  convert  2018-12-13 02:25:23 to 13-12-2018 format*/
    public static String formatDateNew(String created) {
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = inputFormat.parse(created);
            String outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "error";
        }
    }

    /*  convert  2018-12-13 02:25:23 to 13-12-2018 08:00pm format*/
    public static String formatDateSave(String created) {
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = inputFormat.parse(created);
            String outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    /*  convert  2018-12-13 02:25:23 to 13-12-2018 08:00pm format*/
    public static String formatDateLastSeen(String created) {
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = inputFormat.parse(created);
            String outputDateStr = outputFormat.format(date);
            return "Last seen at " + outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    /*  convert  2018-12-13 02:25:23 to 11:40am format*/
    public static String formatTimeChat(String created) {
        DateFormat outputFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = inputFormat.parse(created);
            String outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "error";
        }
    }

    /*  convert  2018-12-13 02:25:23 to 13-12-2018 format*/
    public static String formatPollDateNew(String created) {
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = null;
        try {
            date = inputFormat.parse(created);
            String outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "error";
        }
    }

    /*  convert  2018-12-13 02:25:23 to Created 13/12/2018 at 08:33pm format*/
    public static String formatCreatedDate(String created) {
        DateFormat outputFormat = new SimpleDateFormat("EEEE, MMMM dd", Locale.getDefault());
        DateFormat timFormate = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = null;
        try {
            date = inputFormat.parse(created);
            String outputDateStr = outputFormat.format(date);
            String timeDateStr = timFormate.format(date);
//            return "Created " + outputDateStr + " at " + timeDateStr;
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "error";
        }
    }

    public static String formatDateStart(String created) {
        DateFormat outputFormat = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        Date date = null;
        try {
            date = inputFormat.parse(created);
            String outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "error";
        }
    }

    public static String formatDateEnd(String created) {
        DateFormat outputFormat = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("MMMM dd yyyy", Locale.getDefault());
        Date date = null;
        try {
            date = inputFormat.parse(created);
            String outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "error";
        }
    }


    public static String getTomorrowDate(){
        DateFormat outputFormat = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault());

        // get a calendar instance, which defaults to "now"
        Calendar calendar = Calendar.getInstance();

        // add one day to the date/calendar
        calendar.add(Calendar.DAY_OF_YEAR, 1);

        // now get "tomorrow"
        Date tomorrow = calendar.getTime();

        String outputDateStr = outputFormat.format(tomorrow);
        return outputDateStr;


    }
    public static String getCurrentDate2() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getCurrentYear() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("a", Locale.getDefault());
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String formatDateList(String created) {
        DateFormat outputFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        Date date = null;
        try {
            date = inputFormat.parse(created);
            String outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "error";
        }
    }


    public static String getAm() {
        DateFormat outputFormat = new SimpleDateFormat("a", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        Date date = null;
        try {
            date = inputFormat.parse("11:47");
            String outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "error";
        }
    }

    public static String getPm() {
        DateFormat outputFormat = new SimpleDateFormat("a", Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        Date date = null;
        try {
            date = inputFormat.parse("23:47");
            String outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException e) {
            e.printStackTrace();
            return "error";
        }
    }

    public static boolean isAppIsInBackground(Context context) {///false if app is not in background
        // true if app is in background
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    public static String getTimeWithFormat(String date) {
        try {
//            2018-07-10 06:47:55
            SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df2.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date chatDate = df2.parse(date);
            SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = new Date();
            Date nowDate = outputFmt.parse(outputFmt.format(date1));

            long diff = nowDate.getTime() - chatDate.getTime();
            long sec = diff / 1000;
            long min = sec / 60;
            long hour = min / 60;
            long days = hour / 24;

            AppLog.SHOW_LOG("time is:" + days + "days " + hour + "hours " + min + " mins" + sec + " sec");
            if (days > 0) {
                return days + " days ago";
            } else if (hour > 0) {
                return hour + " hours ago";
            } else if (min > 0) {
                return min + " min ago";
            } else {
                if (sec <= 1) {
                    return "Just now";
                } else {
                    return sec + " sec ago";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "error!";
        }
    }

//    public static void SHOW_DIALOG(Activity activity, String msg) {
//        final Dialog dialog = new Dialog(activity);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.dialog);
//
//        TextView text = dialog.findViewById(R.id.text_dialog);
//        Button yes = dialog.findViewById(R.id.btn_dialog_yes);
//        Button no = dialog.findViewById(R.id.btn_dialog_no);
//
//        text.setText(msg);
//
//        yes.setOnClickListener(view -> dialog.dismiss());
//
//        no.setOnClickListener(view -> dialog.dismiss());
//        dialog.show();
//
//    }

    public static String getDate(String date, String type) {
        DateFormat format2 = null;
        String finalDay = null;
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            Date dt1 = format1.parse(date);
            if (type.equalsIgnoreCase("day")) {
                format2 = new SimpleDateFormat("dd");
            } else if (type.equalsIgnoreCase("month")) {
                format2 = new SimpleDateFormat("MM");
            } else if (type.equalsIgnoreCase("year")) {
                format2 = new SimpleDateFormat("yyyy");
            } else if (type.equalsIgnoreCase("hour")) {
                format2 = new SimpleDateFormat("hh");
            } else if (type.equalsIgnoreCase("minute")) {
                format2 = new SimpleDateFormat("mm");
            }
            finalDay = format2.format(dt1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalDay;
    }

    public Uri getLocalBitmapUri(Activity activity, Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public void setUpFocusWork(EditText etOtp1, EditText etOtp2, EditText etOtp3, EditText etOtp4) {

        etOtp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etOtp1.getText().toString().trim().length() > 0) {
                    etOtp1.clearFocus();
                    etOtp2.requestFocus();
                }
            }
        });
        etOtp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etOtp2.getText().toString().trim().length() > 0) {
                    etOtp2.clearFocus();
                    etOtp3.requestFocus();
                }
                if (etOtp2.getText().toString().trim().equals("")) {
                    etOtp2.clearFocus();
                    etOtp1.requestFocus();
                }

            }
        });
        etOtp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etOtp3.getText().toString().trim().length() > 0) {
                    etOtp3.clearFocus();
                    etOtp4.requestFocus();
                }
                if (etOtp3.getText().toString().trim().equals("")) {
                    etOtp3.clearFocus();
                    etOtp2.requestFocus();
                }
            }
        });
        etOtp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etOtp4.getText().toString().trim().equals("")) {
                    etOtp4.clearFocus();
                    etOtp3.requestFocus();
                }
            }
        });
    }

    private static Common utilProject;

    public static Common getInstance() {
        if (utilProject == null) utilProject = new Common();
        return utilProject;

    }

    public String changeFormat(String date, String inputDateFormat, String outPutDateFormat, @Nullable TimeZone inputTimeZone, @Nullable TimeZone outputTimeZone) {
        Date initDate = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(inputDateFormat, Locale.getDefault());
            if (inputTimeZone != null) simpleDateFormat.setTimeZone(inputTimeZone);
            initDate = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outputFormatter = new SimpleDateFormat(outPutDateFormat, Locale.getDefault());
        if (outputTimeZone != null) outputFormatter.setTimeZone(outputTimeZone);
        return outputFormatter.format(initDate);
    }

    public String getRelativeTime(String dateStr, String inputDateFormat) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputDateFormat);
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = inputFormat.parse(dateStr);
            if (Math.abs(Calendar.getInstance().getTimeInMillis() - date.getTime()) > 60000) {
                return (String) DateUtils.getRelativeTimeSpanString(date.getTime(), Calendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS);
            } else return "Just now";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
    public boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public interface socketParameter {
        String user_id = "user_id";
        String other_user_id = "other_user_id";
        String other_user_image = "other_user_image";
        String other_user_name = "other_user_name";
    }

    public interface key {
        String address = "address";
        String isPremium = "isPremium";
        String signUp = "signUp";
        String isFrom = "isFrom";
        String setting = "setting";
        String data = "data";
        String audioSearch = "audioSearch";
        String addReminder = "addReminder";
        String date = "date";
        String time = "time";
        String remindData = "remindData";
        String lat = "lat";
        String lng = "lng";
        String selectionList = "slectionList";
        String accountType = "accountType";
        String password = "password";
        String username = "username";
        String description = "description";
        String inboxList = "inboxList";
    }

    public interface onActivityCode {
        int requestCodeSpeech = 120;
        int requestPlaceAuto = 121;
        int reqestLocation = 122;
        int resultLocation = 123;
        int requestUser = 124;
        int resultUser = 125;
        int savePassRequest = 126;
        int savePassResult = 127;
    }

    public interface permissionCode {
        int locationPermission = 120;
    }

}

