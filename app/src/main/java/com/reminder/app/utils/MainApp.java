package com.reminder.app.utils;


import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.reminder.app.R;
import com.reminder.app.models.UserResponse;
import com.reminder.app.webserviceRetro.ApiClient;
import com.reminder.app.webserviceRetro.ApiServices;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.engineio.client.transports.WebSocket;


public class MainApp extends Application {

    public static String DISTANCE_KEY = "buy_distance";
    public static String CAT_KEY = "buy_cat";
    private static MainApp sInstance;
    public Socket mSocket;
    private String token;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ApiServices apiServices;
    static Resources appRes;

    public static Resources getAppRes() {
        return appRes;
    }

    public static MainApp getInstance() {
        return sInstance;
    }

    public ApiServices getApiServices() {
        return apiServices;
    }

    public void logException(Exception exception, Class clsObj) {
        logException(exception, clsObj, false);
    }

    public void logException(Exception exception, Class clsObj, boolean sendOnCrashlytics) {
        try {
            if (sendOnCrashlytics) {
                final int PRIORITY_ERROR = 6;
//                Crashlytics.log(PRIORITY_ERROR, clsObj.getName(), exception.getMessage());
//                Crashlytics.logException(exception);
            } else Log.d(clsObj.getSimpleName(), exception.getMessage(), exception);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))//enable logging when app is in debug mode
                .twitterAuthConfig(new TwitterAuthConfig(getResources().getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), getResources().getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)))//pass the created app Consumer KEY and Secret also called API Key and Secret
                .debug(true)//enable debug mode
                .build();

        //finally initialize twitter with created configs
        Twitter.initialize(config);

        sInstance = this;
        appRes = getResources();
        apiServices = ApiClient.getApiInterface(this, null);


        /*stripe payment*/
//        PaymentConfiguration.init(getApplicationContext(),"pk_test_TYooMQauvdEDq54NiTphI7jx");

        printHashKey();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("rgg", "getInstanceId failed", task.getException());
                        return;
                    }
                    token = task.getResult().getToken();
                    Common.setIntro(getApplicationContext(), "device_token", token);
                    AppLog.SHOW_LOG("token::" + token);
                });

        getSocket();

    }

    public Socket getSocket() {
        if (!Common.getPreferences(this, "data").equals("0")) {


            UserResponse userModel = new Gson().fromJson(Common.getPreferences(this, "data"), UserResponse.class);
//        UserResponse userModel = null;

            if (userModel != null && !TextUtils.isEmpty(String.valueOf(userModel.getData().getId()))) {
                if (mSocket != null) return mSocket;
                try {

                    IO.Options options = new IO.Options();
                    options.transports = new String[]{WebSocket.NAME};
//                options.sslContext = mySSLContext;
//                options.hostnameVerifier = myHostnameVerifier;
                    options.timeout = 10000;
                    options.reconnection = true;
                    options.reconnectionAttempts = 10;
                    options.reconnectionDelay = 1000;
                    options.forceNew = true;
                    try {
                        String SocketUrl = Constant.CHAT_URL
                                + "?user_id=" + userModel.getData().getId();
                        mSocket = IO.socket(SocketUrl, options);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return mSocket;
    }


//    public  OkHttpClient httpClient(){
//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
//        OkHttpClient httpClient = new OkHttpClient();
//        httpClient.retryOnConnectionFailure();
//        httpClient.connectTimeoutMillis();
//        httpClient.readTimeoutMillis();
//        httpClient.writeTimeoutMillis();
//        OkHttpClient.Builder builder = new OkHttpClient.Builder();
//        builder.connectTimeout(30, TimeUnit.SECONDS);
//        builder.readTimeout(30, TimeUnit.SECONDS);
//        builder.writeTimeout(30, TimeUnit.SECONDS);
//        httpClient = builder.build();
//        return httpClient;
//    }


    void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }

    }

}
