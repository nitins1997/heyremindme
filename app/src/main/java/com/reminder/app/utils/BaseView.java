package com.reminder.app.utils;

public interface BaseView {

    void showProgressBar();

    void hideProgressBar();

    void errorMessage(String message);

}