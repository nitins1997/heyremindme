package com.reminder.app.utils;

import com.google.api.services.youtube.YouTubeScopes;

public class Constant {
    public static final int IMAGE = 0x1F4F7;
    public static final int POLL = 0x1F4CA;
    public static final int VIDEO = 0x1F4F9;
    //    public static final int LOCATION = 0x1F5FA;
//    public static final int LOCATION = 0x1F4CD;
    public static final int LOCATION = 0x1F4CC;
    public static final int EVENT = 0x1F4C5;
    public static final int RIGHT = 0X2713;
    //    public static final int CANCEL = 0X2716		;
    public static final int CANCEL = 0X274C;

    public static final int SERVICE_TYPE_GET = 1;
    public static final int SERVICE_TYPE_POST = 2;
    public static final String DEVICE_TYPE = "ANDROID";
    public static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    public static final int LOCATION_REQUEST = 1000;
    public static final int GPS_REQUEST = 1001;
    public static final String SIGNUP = "register-user";
    public static final String SIGNIN = "login-user";
    public static final String FORGOTPASSWORD = "forget-password";
    public static final String UPDATEPROFILE = "edit-profile";

    public static final String UPDATEPROFILEIMAGE = "edit-profile-image";
    public static final String CHANGEPASSWORD = "change-password";
    public static final String SOCIALLOGIN = "social-login";
    public static final String ADD_REMINDER = "add-reminder";
    public static final String EDIT_REMINDER = "edit-reminder";
    public static final String REMINDER_LIST = "reminder-list";
    public static final String CHAT_ACTIONS = "com.reminder.app";
    public static final String CHAT_ACTIONS_SEND_MESSAGE = "send_message";
    public static final String CHAT_ACTIONS_SEND_MESSAGE_RESPONSE = "send_message_response";
    public static final String CHAT_ACTIONS_RECEIVE_MESSAGE = "receive_response";
    public static final String CHAT_ACTIONS_THREAD_AGAIN = "get_chat_thread_again";
    public static final String CHAT_ACTIONS_MESSAGE_HISTORY = "get_message_history_response";
    public static final String CHAT_READ_MESSAGE_UPDATE = "read_message_update_response";
    public static final String CHAT_ACTIONS_CONNECT = "show_connect_icon";
    public static final String CHAT_ACTIONS_DISCONNECT = "show_disconnect_icon";
    public static final String CHAT_ACTIONS_ERROR = "show_error_icon";
    public static final String CHAT_ACTIONS_TIMEOUT = "show_timout_icon";
    public static final String SEND_MESSAGE = "send_message";
    public static final String STATUS = "status";
    public static final String YOUTUBE_KEY = "AIzaSyC-5T_1O-Vj1r4Lzc1dsdhUH9BWszZfp60";
    public static String croppedVideoURI;
    public static String BASE_URL = "http://3.130.123.66/";
    public static String CHAT_URL = "http://3.130.123.66:3939/";
    public static String MESSAGE_DATE_TIME_FORMAT_INPUT_DEMO = "yyyy-MM-dd HH:mm:ss";

    public static final String APP_NAME = "Hey Hey Remind Me!";

    public static final String[] SCOPES = {YouTubeScopes.YOUTUBE, YouTubeScopes.YOUTUBE_READONLY, YouTubeScopes.YOUTUBE_FORCE_SSL};

    public static final String API_KEY = "Insert your API key here";
    public static String MESSAGE_DATE_TIME_FORMAT_INPUT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

}
