package com.reminder.app.webserviceRetro;

import androidx.annotation.Nullable;

import com.reminder.app.R;
import com.reminder.app.utils.MainApp;


public enum ErrorType {
    TokenExpire(R.string.error),
    Error(R.string.error),
    RequestTimeoutError(R.string.timeoutError),
    NoConnectionError(R.string.noConnectionError),
    AuthFailureError(R.string.authFailureError),
    ServerError(R.string.serverError),
    NetworkError(R.string.networkError),
    BadRequestError(R.string.badRequestError),
    ForbiddenError(R.string.forbiddenError),
    NotFoundError(R.string.notFoundError),
    UnsupportedMediaType(R.string.unsupportedMediaType),
    MethodNotAllowedError(R.string.methodNotAllowedError),
    ParseError(R.string.parsing_error),;
    int message;

    ErrorType(int message) {
        this.message = message;
    }

    public String getMessage() {
        return MainApp.getInstance().getString(message);
    }


    public static @Nullable
    ErrorType getErrorTypeByVolleyError(int errorCode) {
        ErrorType errorType = null;
        switch (errorCode) {
            case 400:
                errorType = ErrorType.BadRequestError;
                break;
            case 401:
                errorType = ErrorType.AuthFailureError;
                break;
            case 403:
                errorType = ErrorType.ForbiddenError;
                break;
            case 404:
                errorType = ErrorType.NotFoundError;
                break;
            case 408:
                errorType = ErrorType.RequestTimeoutError;
                break;
            case 500:
            case 501:
            case 502:
            case 503:
            case 504:
            case 505:
                errorType = ErrorType.ServerError;
                break;
            default:
                errorType = ErrorType.Error;
        }
        return errorType;
    }
}
