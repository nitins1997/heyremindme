package com.reminder.app.webserviceRetro;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.reminder.app.utils.AppLog;
import com.reminder.app.utils.Common;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.reminder.app.webserviceRetro.ApiServices.BASE_API_URL;


public class ApiClient {

    public static ApiServices getApiInterface(Context context, @Nullable String headerAuth) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .client(getHeaderRetro(context, headerAuth))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(ApiServices.class);
    }

    public static OkHttpClient getHeaderRetro(Context context, @Nullable String headerAuth) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okClient = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
//                .connectTimeout(Constant.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .addNetworkInterceptor(
                        chain -> {
                            Request.Builder requestBuilder = chain.request().newBuilder();
                            String token = Common.getPreferences(context, "token");
                            AppLog.SHOW_LOG(token);
                            if (!TextUtils.isEmpty(token)) {
                                requestBuilder.addHeader("Authorization", "Bearer " + token);
                            }
                            return chain.proceed(requestBuilder.build());
                        })
                .build();
        return okClient;
    }
}
