package com.reminder.app.webserviceRetro;


import com.reminder.app.BuildConfig;
import com.reminder.app.models.AllUserList;
import com.reminder.app.models.ChatLastMessageModel;
import com.reminder.app.models.ChatModel;
import com.reminder.app.models.FollowList;
import com.reminder.app.models.RemiderSearchModel;
import com.reminder.app.models.ReminderListModel;
import com.reminder.app.models.SavePassword;
import com.reminder.app.models.UserResponse;
import com.reminder.app.webserviceRetro.response.ApiResponse;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;


public interface ApiServices {
    String BASE_API_URL = BuildConfig.URL_BASE;
    String BASE_IMAGE_URL = BuildConfig.URL_BASE + "api/" + "public/";

//    @POST("reminder-list")
//    Call<ApiResponse> addReminder(@Body UserModel model);

    //    @Multipart
//    @POST("index.php")
//    Call<ApiResponse<UserModel>> updateProfile(
//            @Part MultipartBody.Part filePart,
//            @PartMap
//                    Map<String, RequestBody> map);
//

    @Multipart
    @POST("add-reminder")
    Call<ApiResponse> addReminder(
            @Part MultipartBody.Part reminderData,
            @PartMap() Map<String, String> map
    );

    @POST("logout")
    @FormUrlEncoded
    Call<ApiResponse> logout(
            @FieldMap() Map<String, String> map
    );


    @POST("reminder-status")
    @FormUrlEncoded
    Call<ApiResponse> updateStaus(
            @FieldMap() Map<String, String> map
    );

    @POST("reminder-list")
    @FormUrlEncoded
    Call<ApiResponse<ReminderListModel>> reminderList(
            @FieldMap() Map<String, String> map
    );


    @POST("delete-reminder")
    @FormUrlEncoded
    Call<ApiResponse> deleteApi(
            @FieldMap() Map<String, String> map
    );

    @POST("get-profile")
    @FormUrlEncoded
    Call<ApiResponse<UserResponse.Data>> getProfile(
            @FieldMap() Map<String, String> map
    );

    @POST("get-plan")
    @FormUrlEncoded
    Call<ApiResponse> getPlan(
            @FieldMap() Map<String, String> map
    );


    @POST("cancel-plan")
    @FormUrlEncoded
    Call<ApiResponse> cancelPlan(
            @FieldMap() Map<String, String> map
    );


    @POST("purchase-plan")
    @FormUrlEncoded
    Call<ApiResponse> update_plan(
            @FieldMap() Map<String, String> map
    );

    @POST("get-last-chat")
    @FormUrlEncoded
    Call<ApiResponse<ChatLastMessageModel>> lastChat(
            @FieldMap() Map<String, String> map
    );

    @GET("saved-password-list")
    Call<ApiResponse<List<SavePassword>>> savedPasswordList();

    @POST("reminder-list")
    @FormUrlEncoded
    Call<ApiResponse<List<RemiderSearchModel>>> reminderSearchList(
            @FieldMap() Map<String, String> map
    );

    @POST("show-password")
    @FormUrlEncoded
    Call<ApiResponse> showPassword(
            @FieldMap() Map<String, String> map
    );

    @POST("save-password")
    @FormUrlEncoded
    Call<ApiResponse> savePassword(
            @FieldMap() Map<String, String> map
    );

    @POST("follower-list")
    @FormUrlEncoded
    Call<ApiResponse<FollowList>> followList(
            @FieldMap() Map<String, String> map
    );

    @POST("app-user-list")
    @FormUrlEncoded
    Call<ApiResponse<AllUserList>> allUser(
            @FieldMap() Map<String, String> map
    );

    @POST("follow-unfollow-user")
    @FormUrlEncoded
    Call<ApiResponse> followUnfollow(
            @FieldMap() Map<String, String> map
    );
}


