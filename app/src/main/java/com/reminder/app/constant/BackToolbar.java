package com.reminder.app.constant;

import android.app.Activity;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.reminder.app.R;

import java.util.Objects;

public class BackToolbar extends AppCompatActivity {

    String type1="";
    ImageView img_noti;
    ImageView img_user;
    TextView noti_count;
    Activity activity;


    public void initToolbar(String title, String type){
        type1=type;
        Toolbar myToolbar = null;
        TextView txt_title = null;


        /*type
         *0=>White white_tool_bar
         * 1=>Light Gray Toolbar
         * 2=>White toolbar without back icon
         * 4=>add new task tool bar*/
        if (type.equals("1")) {
            /*white tool bar with notification and user profile icon*/
            /*gray tool bar only with back button*/
            myToolbar = findViewById(R.id.gray_toolbar);
            txt_title = findViewById(R.id.gray_action_bar_text);
            setSupportActionBar(myToolbar);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);

            setSupportActionBar(myToolbar);

        } else if (type.equals("2")) {


        }
        txt_title.setText(title);

        myToolbar.setTitle("");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
