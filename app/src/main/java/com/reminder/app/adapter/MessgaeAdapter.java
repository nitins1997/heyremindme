package com.reminder.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.reminder.app.R;
import com.reminder.app.models.MessageModel;
import com.reminder.app.models.UserResponse;
import com.reminder.app.utils.Common;

import java.util.ArrayList;
import java.util.List;


public class MessgaeAdapter extends RecyclerView.Adapter<MessgaeAdapter.VH> {

    List<MessageModel> itemList = new ArrayList<>();

    Activity activity;

    public MessgaeAdapter(List<MessageModel> itemList, Activity activity) {
        this.itemList = itemList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_message, parent, false);
        return new VH(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        MessageModel messageModel = itemList.get(position);
        UserResponse userModel = new Gson().fromJson(Common.getPreferences(activity, "data"), UserResponse.class);

        if (messageModel.getOtherUserId().equals(userModel.getData().getId() + "")) {
            holder.llMine.setVisibility(View.GONE);
            holder.llOther.setVisibility(View.VISIBLE);
        } else {
            holder.llMine.setVisibility(View.VISIBLE);
            holder.llOther.setVisibility(View.GONE);

        }

        holder.txtMine.setText(messageModel.getMessage());
        holder.txtOther.setText(messageModel.getMessage());
    }

    public List<MessageModel> getItemList() {
        return itemList;
    }

    public void add(MessageModel item) {
        itemList.add(item);
        notifyItemInserted(itemList.size() - 1);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class VH extends RecyclerView.ViewHolder {

        LinearLayout llMine;
        LinearLayout llOther;
        TextView txtOther;
        TextView txtMine;

        public VH(@NonNull View itemView) {
            super(itemView);
            llOther = itemView.findViewById(R.id.llOther);
            llMine = itemView.findViewById(R.id.llMine);
            txtOther = itemView.findViewById(R.id.txtOther);
            txtMine = itemView.findViewById(R.id.txtMine);
        }
    }

}
