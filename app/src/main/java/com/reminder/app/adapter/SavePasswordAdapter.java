package com.reminder.app.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.reminder.app.R;
import com.reminder.app.RecordActivity;
import com.reminder.app.activity.ShowPasswordActivity;



public class SavePasswordAdapter extends RecyclerView.Adapter<SavePasswordAdapter.VH> {

    Activity activity;

    public SavePasswordAdapter(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_save_password, parent, false);
        return new VH(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        holder.llAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subscribePopUp(activity);
            }
        });
    }

    private void subscribePopUp(Activity activity) {
        activity.startActivity(new Intent(activity, ShowPasswordActivity.class));

    }


    @Override
    public int getItemCount() {
        return 10;
    }

    public class VH extends RecyclerView.ViewHolder {

        ConstraintLayout llAll;

        public VH(@NonNull View itemView) {
            super(itemView);
            llAll = itemView.findViewById(R.id.llAll);

        }
    }

}
