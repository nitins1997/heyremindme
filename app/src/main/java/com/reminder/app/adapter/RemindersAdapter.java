package com.reminder.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.reminder.app.R;
import com.reminder.app.models.ReminderListModel;

public class RemindersAdapter extends RecyclerView.Adapter<RemindersAdapter.VH> {

    Activity activity;
    ReminderListModel reminderListModel;

    public RemindersAdapter(Activity activity, ReminderListModel reminderListModel) {
        this.activity = activity;
        this.reminderListModel = reminderListModel;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_reminder, parent, false);
        return new VH(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        ReminderListModel.DataBean listItem = reminderListModel.getData().get(position);


    }

    @Override
    public int getItemCount() {
        return reminderListModel.getData().size();
    }

    public class VH extends RecyclerView.ViewHolder {


        LinearLayout llAll;

        public VH(@NonNull View itemView) {
            super(itemView);
            llAll = itemView.findViewById(R.id.llAll);
        }
    }

}
