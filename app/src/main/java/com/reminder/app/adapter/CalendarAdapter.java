package com.reminder.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.reminder.app.BR;
import com.reminder.app.R;
import com.reminder.app.callback.OnItemClickListener;
import com.reminder.app.fragment.FragmentCalendar;
import com.reminder.app.models.ReminderListModel;

import java.util.ArrayList;
import java.util.List;

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.VH> implements Filterable {

    FragmentCalendar activity;
    List<ReminderListModel.DataBean> reminderListModel;
    ReminderListModel mStringFilterList;
    private OnItemClickListener onItemClickListener;
    private ValueFilter valueFilter;
    private String searchString = "";


    public CalendarAdapter(FragmentCalendar activity, List<ReminderListModel.DataBean> reminderListModellist,ReminderListModel reminderListModel, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.reminderListModel = reminderListModellist;
        this.mStringFilterList = reminderListModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_week_task, parent, false);
        return new VH(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.binding.setVariable(BR.item, reminderListModel.get(position));
        if (onItemClickListener != null) {
            holder.binding.setVariable(BR.onItemClickListener, onItemClickListener);
        }

    }

    @Override
    public int getItemCount() {
        return reminderListModel.size();
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    public class VH extends RecyclerView.ViewHolder {


        ViewDataBinding binding;

        public VH(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            searchString = constraint.toString();

            final FilterResults results = new FilterResults();
            final ArrayList<ReminderListModel.DataBean> filterList = new ArrayList<ReminderListModel.DataBean>();
            if (constraint != null && constraint.length() > 0) {

                for (final ReminderListModel.DataBean g : mStringFilterList.getData()) {
                    if (g.getTitle().toLowerCase().trim().contains(constraint.toString().toLowerCase()))
                        filterList.add(g);

                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mStringFilterList.getData().size();
                results.values = mStringFilterList.getData();
            }

//            if (searchString.isEmpty()) {
//                for (final ReminderListModel.DataBean g : mStringFilterList.getData()) {
//                    filterList.add(g);
//                }
//                results.count = filterList.size();
//                results.values = filterList;
//            }

            return results;

        }


        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            reminderListModel = (ArrayList<ReminderListModel.DataBean>) results.values;
            notifyDataSetChanged();

            try {
                if (reminderListModel.size() == 0) {
                    activity.binding.txtNoData.setText("No Search Result Found..");
                    activity.binding.txtNoData.setVisibility(View.VISIBLE);
                    activity.binding.recyclerView.setVisibility(View.GONE);
                } else {
                    activity.binding.txtNoData.setVisibility(View.GONE);
                    activity.binding.recyclerView.setVisibility(View.VISIBLE);


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
