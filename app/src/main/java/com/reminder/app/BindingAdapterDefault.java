package com.reminder.app;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.reminder.app.adapter.GeneralAdapter;
import com.reminder.app.callback.OnItemClickListener;
import com.reminder.app.utils.MainApp;

import java.io.File;
import java.util.List;

/**
 * Created by Dyzbee on 11/11/2017.
 */
public class BindingAdapterDefault {

    @BindingAdapter(value = {"android:src", "placeholderImage", "errorImage"}, requireAll = false)
    public static void loadImageWithGlide(ImageView imageView, Object src, Object placeholder, Object errorImage) {
        if (src == null) {
            if (placeholder != null) {
                if (placeholder instanceof Drawable)
                    imageView.setImageDrawable((Drawable) placeholder);
                if (placeholder instanceof Integer)
                    imageView.setImageResource((Integer) placeholder);
            }
            return;
        }

        RequestOptions options = new RequestOptions();

        if (placeholder != null) {
            if (placeholder instanceof Drawable) options.placeholder((Drawable) placeholder);
            if (placeholder instanceof Integer) options.placeholder((Integer) placeholder);
        }
        if (errorImage != null) {
            if (errorImage instanceof Drawable) options.error((Drawable) errorImage);
            if (errorImage instanceof Integer) options.error((Integer) errorImage);
        }

        RequestManager manager = Glide.with(MainApp.getInstance()).
                applyDefaultRequestOptions(options);
        RequestBuilder<Drawable> builder;

        if (src instanceof String) {
            builder = manager.load((String) src);
        } else if (src instanceof Uri)
            builder = manager.load((Uri) src);
        else if (src instanceof Drawable)
            builder = manager.load((Drawable) src);
        else if (src instanceof Bitmap)
            builder = manager.load((Bitmap) src);
        else if (src instanceof Integer)
            builder = manager.load((Integer) src);
        else if (src instanceof File)
            builder = manager.load((File) src);
        else if (src instanceof byte[])
            builder = manager.load((byte[]) src);
        else builder = manager.load(src);
        builder.listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                e.printStackTrace();
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(imageView);
    }

    @BindingAdapter({"imageLocalPath"})
    public static void loadImageFromStorage(ImageView imageView, String path) {
        if (path != null) {
            Glide.with(imageView)
                    .load(Uri.fromFile(new File(path)))
                    .skipMemoryCache(true)
                    .into(imageView);
        } else {
            imageView.setImageResource(R.drawable.img_no_image);
        }
    }

    @BindingAdapter(value = {"rvItemLayout", "rvList", "rvOnItemClick"}, requireAll = false)
    public static void setRvAdapter(RecyclerView recyclerView, int rvItemLayout, List rvList, @Nullable OnItemClickListener onItemClickListener) {
        if (rvItemLayout != 0 && rvList != null && rvList.size() > 0)
            recyclerView.setAdapter(new GeneralAdapter(rvItemLayout, rvList, onItemClickListener));
    }

    @BindingAdapter("android:visible")
    public static void setVisibilityByBoolean(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
