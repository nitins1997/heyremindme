package com.reminder.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reminder.app.R;
import com.reminder.app.adapter.MessgaeAdapter;
import com.reminder.app.databinding.ActivityMessageBinding;
import com.reminder.app.enums.MessageType;
import com.reminder.app.models.EventBusModel;
import com.reminder.app.models.MessageModel;
import com.reminder.app.models.MessageResultModel;
import com.reminder.app.models.UserResponse;
import com.reminder.app.service.ConnectionService;
import com.reminder.app.service.EventsName;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;
import com.reminder.app.utils.MainApp;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;



import de.hdodenhof.circleimageview.CircleImageView;

public class MessageActivity extends AppCompatActivity {
    public static boolean is_show;

    private final int last_id = 0;

    String other_user_id = "";
    UserResponse userModel;
    MessgaeAdapter adapter;
    LinearLayoutManager layoutManager;
    boolean fetchMore = true;
    private int page = 1;
    RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int pastVisibleItems = layoutManager.findFirstCompletelyVisibleItemPosition();
            if (pastVisibleItems == 0) {
                if (fetchMore)
                    getChat();
            }
        }
    };

    ActivityMessageBinding binding;

    @Override
    protected void onPause() {
        super.onPause();
        is_show = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_message);
        /*setup event bus*/
        EventBus.getDefault().register(this);

        userModel = new Gson().fromJson(Common.getPreferences(this, "data"), UserResponse.class);

        /*set other user data*/
        setAdapter();
        otherUserData();
        getChat();

        binding.imageView3.setOnClickListener(view -> {
            finish();
        });

        binding.imgSend.setOnClickListener(v -> {
            if (!binding.edtMessage.getText().toString().trim().isEmpty()) {
                sendMessage(binding.edtMessage.getText().toString(), null, MessageType.TEXT.getType());
            }
        });

    }

    public boolean isEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    private void removeScrollListener() {
        binding.recyclerView.removeOnScrollListener(scrollListener);
    }

    private void sendMessage(String msg, String thumb_image, String type) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("device_token", Common.getIntro(this, "device_token"));
            jsonObject.put("device_type", Constant.DEVICE_TYPE);
            jsonObject.put("type", type);
            jsonObject.put("message", msg);
            jsonObject.put("other_data", "");
            jsonObject.put("other_user_id", other_user_id);
            if (thumb_image != null)
                jsonObject.put("thumb_image", "");
            jsonObject.put("user_id", userModel.getData().getId() + "");
            MainApp.getInstance().getSocket().emit(EventsName.SEND_MESSAGE, jsonObject);
            binding.edtMessage.setText(null);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(EventBusModel model) {
        switch (model.getAction()) {
            case EventsName.GET_MESSAGE_HISTORY_RESPONSE: {
                MessageResultModel<List<MessageModel>> resultModel = new Gson().fromJson(model.getResponse()
                        , new TypeToken<MessageResultModel<List<MessageModel>>>() {
                        }.getType());
                fetchMore = resultModel.getTotal_pages() >= page;
                if (isEmpty(resultModel.getData())) return;
                for (MessageModel messageModel : resultModel.getData()) {
                    messageModel.setSent(messageModel.getOtherUserId().equals(userModel.getData().getId()));
                }
                Collections.reverse(resultModel.getData());
                boolean wasEmpty = adapter.getItemCount() == 0;
                removeScrollListener();
                adapter.getItemList().addAll(0, resultModel.getData());
                adapter.notifyItemRangeInserted(
                        0
                        , resultModel.getData().size());
//            adapter.add(resultModel.getResponseData());
                if (wasEmpty) {
                    scrollToBottom();
                } else scrollToTop(resultModel.getData().size());
                new Handler().postDelayed(() -> {
                    addScrollListener();
                }, 200);
//                readMessageEvent(resultModel.getResponseData());
                break;
            }
            case EventsName.RECEIVE_MESSAGE: {
                MessageModel resultModel = new Gson().fromJson(model.getResponse()
                        , new TypeToken<MessageModel>() {
                        }.getType());
                resultModel.setSent(resultModel.getOtherUserId().equals(userModel.getData().getId()));

                if (resultModel.getUserId() != userModel.getData().getId()) {
                    if (resultModel.getUserId() != Integer.parseInt(other_user_id)) {
                        return;
                    }
                }

//                todo gopi sir said to check incoming msg id for resolving duplicatet msg
//                for (MessageModel messageModel : adapter.getItemList()) {
//                    if (messageModel.getId() == resultModel.getId()) {
//                        return;
//                    }
//                }

                adapter.add(resultModel);
                scrollToBottom();

                // TODO: 20-Feb-21 for read message event
//                ArrayList<MessageModel> list = new ArrayList<>();
//                list.add(resultModel);
//                readMessageEvent(list);
                break;
            }
        }

    }

    private void scrollToTop(int size) {
        binding.recyclerView.scrollToPosition(size < 10 ? 0 : 10);
    }

    private void scrollToBottom() {
        new Handler().postDelayed(() -> {
            binding.recyclerView.scrollToPosition(adapter.getItemList().size() - 1);
        }, 100);
    }

    private void setAdapter() {
        ArrayList<MessageModel> models = new ArrayList<>();
        adapter = new MessgaeAdapter(models, this);
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setSmoothScrollbarEnabled(true);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setAdapter(adapter);
        addScrollListener();
    }

    private void addScrollListener() {
        binding.recyclerView.addOnScrollListener(scrollListener);
    }

    private void otherUserData() {
        binding.txtName.setText(getIntent().getStringExtra(Common.socketParameter.other_user_name));
        Common.INIT_CIRCLE_CONTACT_PICASSO(this, getIntent().getStringExtra(Common.socketParameter.other_user_image), binding.cvProfile);
        other_user_id = getIntent().getStringExtra(Common.socketParameter.other_user_id);

    }

    @Override
    protected void onResume() {
        is_show = true;

        if (!Common.getInstance().isMyServiceRunning(this, ConnectionService.class))
            startService(new Intent(this, ConnectionService.class));
        super.onResume();

    }

    private void getChat() {
        try {
            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("last_id", last_id);
//            jsonObject.put("limit", 20);
//            jsonObject.put("type", "before");
            jsonObject.put("page", page++);
            jsonObject.put("other_user_id", other_user_id);
            jsonObject.put("user_id", userModel.getData().getId() + "");
            MainApp.getInstance().getSocket().emit(EventsName.GET_MESSAGE_HISTORY, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}