package com.reminder.app.activity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.reminder.app.R;
import com.reminder.app.databinding.ActivityAddReminderDetailBinding;
import com.reminder.app.models.ReminderListModel;
import com.reminder.app.utils.Common;

import java.io.IOException;

public class AddReminderDetailActivity extends BaseActivity implements View.OnClickListener {
    ActivityAddReminderDetailBinding binding;
    String reminderData = "";
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_reminder_detail);

        if (getIntent() != null) {
            if (getIntent().getSerializableExtra(Common.key.remindData) != null) {
                ReminderListModel.DataBean remiderSearchModel = (ReminderListModel.DataBean) getIntent().getSerializableExtra(Common.key.remindData);
                reminderData = remiderSearchModel.getReminderdata();
                binding.setTitle(remiderSearchModel.getTitle());
                binding.setDescription(remiderSearchModel.getDescription());
                binding.setDate(remiderSearchModel.getReminder_date());
                binding.setTime(Common.formatDateList(remiderSearchModel.getReminder_time()));
                binding.setLocation(remiderSearchModel.getAddress());
                if (remiderSearchModel.getAddress() == null) {
                    binding.txtLoc.setVisibility(View.GONE);
                    binding.flLoc.setVisibility(View.GONE);
                }
//                binding.setReminderData(remiderSearchModel.getre());

                String members = "";
                for (int i = 0; i < remiderSearchModel.getOthers().size(); i++) {
                    if (members.isEmpty()) {
                        members = remiderSearchModel.getOthers().get(i).getName();
                    } else {
                        members = members + ", " + remiderSearchModel.getOthers().get(i).getName();
                    }
                }
                binding.setMembers(members);
                if (members.isEmpty()) {
                    binding.flMember.setVisibility(View.GONE);
                    binding.txtMemberTitle.setVisibility(View.GONE);
                }

                binding.edtLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Common.openLocation(AddReminderDetailActivity.this, remiderSearchModel.getLat(), remiderSearchModel.getLng(), remiderSearchModel.getAddress());
                    }
                });


                binding.txtAddVoice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (binding.txtAddVoice.getText().toString().equals(getString(R.string.play_recording))) {
                            binding.txtAddVoice.setText(getString(R.string.stop_recording));
                            playAudio();
//                showTimer();
                        } else {
//                stopTime();
                            binding.txtAddVoice.setText(getString(R.string.play_recording));
                            stopAudio();
                        }
                    }
                });


            }
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        stopAudio();
    }

    private void stopAudio() {
        try {
            if (mediaPlayer.isPlaying()) {
                // pausing the media player if media player
                // is playing we are calling below line to
                // stop our media player.
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();

                // below line is to display a message
                // when media player is paused.
                Toast.makeText(this, "Audio has been paused", Toast.LENGTH_SHORT).show();
                binding.txtAddVoice.setText(getString(R.string.play_recording));
            } else {
                // this method is called when media
                // player is not playing.
                Toast.makeText(this, "Audio has not played", Toast.LENGTH_SHORT).show();
                binding.txtAddVoice.setText(getString(R.string.play_recording));
            }
        }catch (Exception e){
            e.printStackTrace();
            binding.txtAddVoice.setText(getString(R.string.play_recording));

        }

    }

    private void playAudio() {
        String audioUrl = reminderData;
        // initializing media player
        mediaPlayer = new MediaPlayer();
        // below line is use to set the audio
        // stream type for our media player.
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        // below line is use to set our
        // url to our media player.
        try {
            mediaPlayer.setDataSource(audioUrl);
            // below line is use to prepare
            // and start our media player.
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(mp -> binding.txtAddVoice.setText(getString(R.string.play_recording)));
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // below line is use to display a toast message.
        Toast.makeText(this, "Audio started playing..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.imgBack:
                finish();
                break;


        }
    }


}