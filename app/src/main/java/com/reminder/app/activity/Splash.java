package com.reminder.app.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.reminder.app.R;
import com.reminder.app.utils.Common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Splash extends AppCompatActivity {
    String isLogin;
    String TAG = "";

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("", "printHashKey()", e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        isLogin = Common.getPreferences(this, "isLogin");


        Handler handler = new Handler();
        handler.postDelayed(() -> {
            if (isLogin.equalsIgnoreCase("true")) {
                if (getIntent().getExtras() != null) {
                    if (getIntent().hasExtra("type") && getIntent().getStringExtra("type") != null) {
                        if (getIntent().getStringExtra("type").equals("CHAT_MESSAGE")) {
                            Intent intent = new Intent(Splash.this, MessageActivity.class);
                            intent.putExtra(Common.socketParameter.other_user_id, getIntent().getStringExtra("sender_id") + "");
                            intent.putExtra(Common.socketParameter.other_user_name, getIntent().getStringExtra("sender_name"));
                            intent.putExtra(Common.socketParameter.other_user_image, getIntent().getStringExtra("sender_image"));
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        } else if (getIntent().getStringExtra("reminderdata")!=null) {
                            Intent intent = new Intent(Splash.this, HomeActivity.class);
                            intent.putExtra("reminderdata", getIntent().getStringExtra("reminderdata") + "");
                            intent.putExtra(Common.key.isFrom, "recordPlay");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        } else {
                            homeActivityIntent();
                        }
                    } else {
                        homeActivityIntent();
                    }
                } else {
                    homeActivityIntent();
                }
            } else {
                startActivity(new Intent(Splash.this, LoginActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }

        }, 3000);
    }

    private void homeActivityIntent() {
        startActivity(new Intent(Splash.this, HomeActivity.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

}