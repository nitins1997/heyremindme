package com.reminder.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.reminder.app.R;
import com.reminder.app.databinding.ThanksPopBinding;
import com.reminder.app.utils.Common;

public class ShowPasswordActivity extends Activity {

    ThanksPopBinding binding;

    String pass = "xxxxxxxx";
    String showPass = "xxxxxxxx";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.thanks_pop);
        Intent getint = getIntent();
        binding.setAccountType(getint.getStringExtra(Common.key.accountType));

        showPass = getint.getStringExtra(Common.key.password);


        binding.setPassword(pass);

        binding.setDescription(getint.getStringExtra(Common.key.description));
        binding.setUsername(getint.getStringExtra(Common.key.username));
        binding.imgBackshowpassword.setOnClickListener(view -> {
            finish();
        });

        binding.imgShow.setOnClickListener(view -> {
            if (binding.txtPassword.getText().toString().equals(pass)) {
                binding.imgShow.setImageResource(R.drawable.ic_hide);
                binding.setPassword(showPass);
            } else {
                binding.setPassword(pass);
                binding.imgShow.setImageResource(R.drawable.ic_eye);
            }
        });
    }
}
