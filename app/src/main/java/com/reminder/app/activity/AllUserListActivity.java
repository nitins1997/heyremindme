package com.reminder.app.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.reminder.app.R;
import com.reminder.app.adapter.GeneralAdapter;
import com.reminder.app.databinding.ActivityAllUserListBinding;
import com.reminder.app.models.AllUserList;
import com.reminder.app.network.ApiClass;
import com.reminder.app.utils.BaseView;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;

import java.util.HashMap;

public class AllUserListActivity extends BaseActivity implements View.OnClickListener {

    int page = 1;
    boolean loadAgain = false;
    ActivityAllUserListBinding binding;
    GeneralAdapter generalAdapter;
    AllUserList allUserList;
    String searchTxt = "";

    boolean isChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_all_user_list);

        callAllUserList();
        initScrollListener();

        binding.edtSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {



            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchTxt = binding.edtSearchText.getText().toString() ;
                if (searchTxt.length() >= 3) {
                    allUserList = null;
                    page = 1;
                    callAllUserList();
                }

                if (searchTxt.isEmpty()) {
                    allUserList = null;
                    page = 1;
                    callAllUserList();
                }

            }
        });

    }

    private void callAllUserList() {
        HashMap<String, String> request = new HashMap<>();
        request.put(ApiClass.PAGE, String.valueOf(page));

        request.put(ApiClass.KEYWORD, searchTxt);

        new RetroService().call((BaseView) this, MainApp.getInstance().getApiServices().allUser(request),
                result -> {
                    if (result.isStatus()) {
                        if (allUserList == null) {
                            allUserList = result.getData();
                            if (allUserList.getData().size() == 0) {
                                binding.recv.setVisibility(View.GONE);
                            } else {
                                binding.recv.setVisibility(View.VISIBLE);
                                setAdapterOnRecycleView();
                                loadAgain = true;
                            }
                        } else {
                            binding.recv.setVisibility(View.VISIBLE);
                            AllUserList reminderListModel1 = result.getData();
                            if (reminderListModel1.getData().size() == 0) {
                                loadAgain = false;
                            } else {
                                generalAdapter.add(reminderListModel1.getData());
                                loadAgain = true;

                            }
                        }
                    } else {
                        Common.showToast(this, result.getMessage());
                    }
                });
    }

    private void setAdapterOnRecycleView() {
        generalAdapter = new GeneralAdapter(R.layout.row_follow_item, allUserList.getData(), (view, object1) -> {
            AllUserList.DataBean singleUser = (AllUserList.DataBean) object1;
            switch (view.getId()) {
                case R.id.txtBtnFollow:
                    HashMap<String, String> requestFollow = new HashMap<>();
                    requestFollow.put(ApiClass.OTHER_USER_ID, singleUser.getId() + "");
                    if (singleUser.getFollow_status() == 1) {
                        requestFollow.put(ApiClass.STATUS, "0");
                        singleUser.setFollow_status(0);
                    } else {
                        requestFollow.put(ApiClass.STATUS, "1");
                        singleUser.setFollow_status(1);
                    }
                    new RetroService().call((BaseView) this, MainApp.getInstance().getApiServices().followUnfollow(requestFollow), result1 -> {
                        Common.showToast(AllUserListActivity.this, result1.getMessage());
                        isChange = true;

                    });


                    break;
            }

        });
        binding.recv.setLayoutManager(new LinearLayoutManager(this));
        binding.recv.setAdapter(generalAdapter);

    }


    private void initScrollListener() {
        binding.recv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                try {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == allUserList.getData().size() - 1) {
                        Log.d("lastitem", "visible");
                        if (loadAgain) {
                            page = page + 1;
                            callAllUserList();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isChange) {
            setResult(Common.onActivityCode.resultUser);
        }
        finish();
    }
}