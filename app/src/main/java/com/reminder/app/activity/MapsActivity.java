package com.reminder.app.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.reminder.app.R;
import com.reminder.app.databinding.ActivityMapsBinding;
import com.reminder.app.utils.Common;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private static final int AUTOCOMPLETE_REQUEST_CODE = 1;
    ActivityMapsBinding binding;
    double lat;
    double lng;
    String address;
    Dialog dialog;
    boolean callResume = true;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private double wayLatitude = 0.0, wayLongitude = 0.0;
    private LatLng mCenterLatLong;
    private PlacesClient placesClient;
    private List<Place.Field> placeFields;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps);
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), "AIzaSyByn1rXXpdw36XN7olVJRLIZn7dlJ6nKuA", Locale.US);
        }
        placesClient = Places.createClient(this);
        placeFields = Arrays.asList(Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        /*Check location permission*/
        if (requestLocationPermission()) {
            return;
        }

        if (!isLocationEnabled()) {
            Common.showToast(this, "Turn your location on.");
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
            finish();
//            return;
        }


//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);

        /*request location*/
        requestLocation();
    }

    private void requestLocation() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, location -> {
            if (location != null) {
                wayLatitude = location.getLatitude();
                wayLongitude = location.getLongitude();
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                binding.progressBar.setVisibility(View.GONE);
                mMap.animateCamera(cameraUpdate);
                getAddress(location.getLatitude(), location.getLongitude());

            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
        if (dialog != null) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                /*dialog not dismiss*/
            } else {
                dialog.dismiss();
            }
        }
        if (callResume) {
            requestLocation();
        }

    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER
        );
    }

    private boolean requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Common.permissionCode.locationPermission);
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Common.permissionCode.locationPermission:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    requestLocation();
                    if (!isLocationEnabled()) {
                        Common.showToast(this, "Turn your location on.");
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                        return;
                    }

                } else {
                    subscribePopUp();
//                    Common.showToast(this, "Permission denied");
//                    finish();
                }
                break;
        }
    }

    private void subscribePopUp() {
        dialog = new Dialog(this, R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.permission_pop, null);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        TextView txtAllow = dialogView.findViewById(R.id.txtAllow);
        TextView txtClose = dialogView.findViewById(R.id.txtClose);
        txtAllow.setOnClickListener(view -> {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivity(intent);

        });

        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Common.showToast(MapsActivity.this, "Permission denied");
                finish();

            }
        });
        dialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnCameraChangeListener(cameraPosition -> {
            Log.d("Camera postion change" + "", cameraPosition + "");
            mCenterLatLong = cameraPosition.target;
            mMap.clear();
            try {
                Location mLocation = new Location("");
                mLocation.setLatitude(mCenterLatLong.latitude);
                mLocation.setLongitude(mCenterLatLong.longitude);
//                binding.locationMarkertext.setText("Lat : " + mCenterLatLong.latitude + "," + "Long : " + mCenterLatLong.longitude);

                getAddress(mCenterLatLong.latitude, mCenterLatLong.longitude);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /*get address from lat long android*/
    private void getAddress(double latitude, double longitude) {
        try {
            Geocoder geo = new Geocoder(MapsActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
            if (addresses.isEmpty()) {
                binding.locationMarkertext.setText("Waiting for Location");
            } else {
                lat = latitude;
                lng = longitude;
                if (addresses.size() > 0) {
                    if (addresses.get(0).getThoroughfare() != null) {
                        binding.locationMarkertext.setText(addresses.get(0).getFeatureName() + ", " + addresses.get(0).getThoroughfare() + ", " + addresses.get(0).getSubLocality());
                        address = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getThoroughfare() + ", " + addresses.get(0).getSubLocality() + ", " + addresses.get(0).getAdminArea();
                    } else {
                        binding.locationMarkertext.setText(addresses.get(0).getFeatureName() + ", " + addresses.get(0).getSubLocality());
                        address = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getSubLocality() + ", " + addresses.get(0).getAdminArea();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.btnCurrenLocation:
                try {
                    Intent locInt = new Intent();
                    locInt.putExtra(Common.key.address, address);
                    locInt.putExtra(Common.key.lat, String.valueOf(lat));
                    locInt.putExtra(Common.key.lng, String.valueOf(lng));
                    setResult(Common.onActivityCode.resultLocation, locInt);
                    finish();
                } catch (Exception e) {
                    Common.showToast(this, "Please Try Again");
                    finish();
                }

                break;
            case R.id.edtLocation:
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(this);
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);

//                Intent intent = new Autocomplete.IntentBuilder(
//                        AutocompleteActivityMode.FULLSCREEN, placeFields)
//                        .setTypeFilter(TypeFilter.ADDRESS)
//                        .build(this);
//                startActivityForResult(intent, Common.onActivityCode.requestPlaceAuto);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                callResume = false;
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i("AutoComplete", "Place: " + place.getName() + ", " + place.getId());
                address = place.getAddress();
                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15);
                binding.progressBar.setVisibility(View.GONE);
                mMap.animateCamera(cameraUpdate);

                getAddress(lat, lng);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("AutoComplete", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
            return;
        }

//        if (requestCode == Common.onActivityCode.requestPlaceAuto && resultCode == RESULT_OK) {
//            Place place = Autocomplete.getPlaceFromIntent(data);
////            latitude = place.getLatLng().latitude;
////            longitude = place.getLatLng().longitude;
//            String location = place.getAddress();
//            String name = place.getName();
//        }
    }
}