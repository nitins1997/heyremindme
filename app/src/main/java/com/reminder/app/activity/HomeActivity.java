package com.reminder.app.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.reminder.app.R;
import com.reminder.app.fragment.FragmentCalendar;
import com.reminder.app.fragment.FragmentReminders;
import com.reminder.app.fragment.SettingFragment;
import com.reminder.app.service.ConnectionService;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;


public class HomeActivity extends BaseActivity implements BillingProcessor.IBillingHandler {
    private static final String LOG_TAG = "iabv3";
    // SUBSCRIPTION IDS
    private static final String SUBSCRIPTION_ID = "remindme";
    private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnhiq0+xWum3mfUSB/OPeJjJ8/P+TzZwledz9Zln1Xj8HHq7qqCJnYJCBe52bFZBokVbTceXZPA72RpX0VMeSRQ7764EfARgqMm5KmNLp5rlyWsowa9+dMbo+euqQsHLbTBtA06PtJjm27gi3svDVtRmpaefl+AAnfoWeE1ZO+W/8nBv4zy37i8pS4pKPByR6yJ6gCdoK0o1APp/XHu/p/FHKL9oyORAgRINa47TK24hn+dOUTLqjZLnuYM+5Y3spFDf3KjPcOay5IXFmNR6AtW/ASGXN38+3fpY2XEcUqFg+JJcaVyM62QP3KxH5TjbvrKc/sGQDU7tczZAvdkYGhQIDAQAB"; // PUT YOUR MERCHANT KEY HERE;
    // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
    // if filled library will provide protection against Freedom alike Play Market simulators
    private static final String MERCHANT_ID = null;
    Fragment selectedFragment = null;
    private final BottomNavigationView.OnNavigationItemSelectedListener navListener = item -> {
        // By using switch we can easily get
        // the selected fragment
        // by using there id.
        switch (item.getItemId()) {
            case R.id.algorithm:
                selectedFragment = new FragmentReminders();
                break;
            case R.id.course:
                selectedFragment = new FragmentCalendar();
                break;
            case R.id.profile:
                selectedFragment = new SettingFragment();
                break;
        }
        // It will help to replace the one fragment to other.
        changeFragment(selectedFragment);
        return true;
    };
    MediaPlayer mediaPlayer;
    String reminderData = "";
    TextView txtTime;
    TextView txtAddVoice;
    CountDownTimer countDownTimer;
    int second = -1, minute, hour;
    String isPremium = "0";
    private BillingProcessor bp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        /*billing subscription value initialize*/
        billingInitialize();

        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        selectedFragment = new FragmentReminders();
        changeFragment(selectedFragment);
        if (!Common.getInstance().isMyServiceRunning(this, ConnectionService.class))
            startService(new Intent(this, ConnectionService.class));


        if (getIntent().getStringExtra(Common.key.isFrom) != null) {
            if (getIntent().getStringExtra(Common.key.isFrom).equals(Common.key.signUp)) {
                subscribePopUp();
            } else if (getIntent().getStringExtra(Common.key.isFrom).equals("recordPlay")) {
                reminderData = getIntent().getStringExtra("reminderdata");
                playRecordingPopup();
            }
        }


    }


    private void billingInitialize() {
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            Common.showToast(this, "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16");
        }
        bp = new BillingProcessor(this, LICENSE_KEY, this);

        bp.initialize();


    }

    @Override
    public void onDestroy() {
        if (bp != null)
            bp.release();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        callAPIGetProfile();

    }

    /*use to add reminder*/
    private void callAPIGetProfile() {
        HashMap<String, String> request = new HashMap<>();
        new RetroService().callWithProgressStatus(this, MainApp.getInstance().getApiServices().getProfile(request),
                result -> {
                    if (result.isStatus()) {
                        isPremium = result.getData().getIs_premium();
                        Common.setPreferences(this, Common.key.isPremium, isPremium);
                        if (isPremium.equals("0")) {
                            subscribePopMove();
                        }
                    } else {
                        Common.showToast(this, result.getMessage());
                    }
                }, false);
    }

    private void playRecordingPopup() {
        Dialog dialog = new Dialog(this, R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.record_play_pop, null);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        txtAddVoice = dialogView.findViewById(R.id.txtAddVoice);
        txtTime = dialogView.findViewById(R.id.txtTime);
        ImageView imgClose = dialogView.findViewById(R.id.imgClose);


        imgClose.setOnClickListener(v -> {
            dialog.dismiss();
            stopAudio();
        });

        txtAddVoice.setOnClickListener(v -> {
            if (txtAddVoice.getText().toString().equals(getString(R.string.play_recording))) {
                txtAddVoice.setText(getString(R.string.stop_recording));
                playAudio();
//                showTimer();
            } else {
//                stopTime();
                txtAddVoice.setText(getString(R.string.play_recording));
                stopAudio();
            }
        });
        dialog.show();
    }


    //display recording time
    public void showTimer() {
        countDownTimer = new CountDownTimer(Long.MAX_VALUE, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                second++;
                txtTime.setText(recorderTime());
                if (second == 59) {
                    if (txtAddVoice.getText().toString().equals(getString(R.string.stop_recording))) {
                        txtAddVoice.setText(getString(R.string.play_recording));
                    }
                }
            }

            public void onFinish() {

            }
        };
        countDownTimer.start();
        startTime();
    }

    //recorder time
    public String recorderTime() {
        if (second == 60) {
            minute++;
            second = 0;
        }
        if (minute == 60) {
            hour++;
            minute = 0;
        }
        return String.format("%02d:%02d", minute, second);
    }

    //recorder time
    @SuppressLint("DefaultLocale")
    private void startTime() {
        if (second == 60) {
            minute++;
            second = 0;
        }

        if (minute == 60) {
            hour++;
            minute = 0;
        }
        txtTime.setText(String.format("%02d:%02d", minute, second));
    }

    private void stopTime() {
        second = -1;
        minute = 0;
        hour = 0;
        txtTime.setText("");

        countDownTimer.cancel();
    }

    private void playAudio() {

        String audioUrl = reminderData;

        // initializing media player
        mediaPlayer = new MediaPlayer();

        // below line is use to set the audio
        // stream type for our media player.
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        // below line is use to set our
        // url to our media player.
        try {
            mediaPlayer.setDataSource(audioUrl);
            // below line is use to prepare
            // and start our media player.
            mediaPlayer.prepare();

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    txtAddVoice.setText(getString(R.string.play_recording));
                }
            });
            mediaPlayer.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
        // below line is use to display a toast message.
        Toast.makeText(this, "Audio started playing..", Toast.LENGTH_SHORT).show();
    }


    private void stopAudio() {
        if (mediaPlayer.isPlaying()) {
            // pausing the media player if media player
            // is playing we are calling below line to
            // stop our media player.
            mediaPlayer.stop();
            mediaPlayer.reset();
            mediaPlayer.release();

            // below line is to display a message
            // when media player is paused.
            Toast.makeText(this, "Audio has been paused", Toast.LENGTH_SHORT).show();
        } else {
            // this method is called when media
            // player is not playing.
            Toast.makeText(this, "Audio has not played", Toast.LENGTH_SHORT).show();
        }
    }


    private void subscribePopMove() {
        Dialog dialog = new Dialog(this, R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.subscribe_pop, null);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        TextView txtSubscribe = dialogView.findViewById(R.id.txtSubscribe);
        TextView txtresult = dialogView.findViewById(R.id.txtresult);
        txtresult.setText(getString(R.string.substext));

        txtSubscribe.setOnClickListener(v -> {
            startActivityForResult(new Intent(this, SubscriptionPlansActivity.class), 414);
            dialog.dismiss();
        });


        dialog.show();
    }


    private void subscribePopUp() {
        Dialog dialog = new Dialog(this, R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.subscribe_pop, null);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        TextView txtSubscribe = dialogView.findViewById(R.id.txtSubscribe);


        txtSubscribe.setOnClickListener(v -> {
            dialog.dismiss();
        });


        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Common.onActivityCode.requestCodeSpeech) {/*google speech text*/
            if (resultCode == Activity.RESULT_OK && null != data) {
                selectedFragment.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == 414) {/*google speech text*/
            if (resultCode == 415) {
                if (data.getBooleanExtra("isPurchase", false)) {
//                    subscribePopMove();
                }
            }
        }
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);

    }


    private void changeFragment(Fragment selectedFragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, selectedFragment)
                .commit();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {
        bp.setPurchaseHistoryRestored();

        boolean isPuchase = false;
        for (String sku : bp.listOwnedSubscriptions()) {
            if (sku.equals(SUBSCRIPTION_ID)) {
                isPuchase = true;
            }
            Log.d(LOG_TAG, "Owned Subscription: " + sku);

        }

//        if (!isPuchase) {
//            HashMap<String, String> hashMap = new HashMap<>();
//            new RetroService().call(this, MainApp.getInstance().getApiServices().cancelPlan(hashMap), result -> {
//                if(result.isStatus()){
//                    subscribePopMove();
//                }
//            });
//        }

        if (isPremium.equals("1")) {
            try {
                if (Objects.requireNonNull(bp.getSubscriptionTransactionDetails(SUBSCRIPTION_ID)).purchaseInfo == null) {
                    callCancelApi();
                }
            } catch (Exception e) {
                e.printStackTrace();
//                callCancelApi();
            }
        }
    }

    private void callCancelApi() {
        HashMap<String, String> hashMap = new HashMap<>();
        new RetroService().call(this, MainApp.getInstance().getApiServices().cancelPlan(hashMap), result -> {
            if (result.isStatus()) {
                subscribePopMove();
            }
        });
    }
}

