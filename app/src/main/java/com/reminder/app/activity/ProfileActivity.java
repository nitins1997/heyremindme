package com.reminder.app.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.reminder.app.R;
import com.reminder.app.databinding.ActivityProfileBinding;
import com.reminder.app.models.UserResponse;
import com.reminder.app.utils.Common;

public class ProfileActivity extends AppCompatActivity {

    ActivityProfileBinding binding = null;
    UserResponse userResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);


        binding.imgBack.setOnClickListener(view -> {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });

        binding.imgEdit.setOnClickListener(view -> {

            startActivity(new Intent(this, EditProfileActivity.class));
            overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
        });

        binding.btnEdit.setOnClickListener(view -> {
            startActivity(new Intent(this, EditProfileActivity.class));
            overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
        });

    }

    @Override
    protected void onResume() {
        userResponse = new Gson().fromJson(Common.getPreferences(this, "data"), UserResponse.class);
        binding.setUserresponse(userResponse);
        Common.INIT_CONTACT_PICASSO(this, userResponse.getData().getProfile_image(), binding.cvProfile);
        if(!userResponse.getData().getPhone().equalsIgnoreCase("")) {
            binding.edtPass.setText("+" + userResponse.getData().getCountry_code() + "-" + userResponse.getData().getPhone());
        }else{
            binding.edtPass.setText("+" + "1" +  userResponse.getData().getPhone());

        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}