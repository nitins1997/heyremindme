package com.reminder.app.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.reminder.app.R;
import com.reminder.app.constant.BackToolbar;
import com.reminder.app.databinding.ActivityResetPasswordBinding;
import com.reminder.app.network.ApiClass;
import com.reminder.app.network.BaseRequestData;
import com.reminder.app.network.RequestedServiceDataModel;
import com.reminder.app.network.ResponseDelegate;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;

import org.json.JSONException;




import static com.reminder.app.network.ResponseType.CHANGE;
import static com.reminder.app.network.ResponseType.FORGOT;

public class ResetPasswordActivity extends AppCompatActivity implements ResponseDelegate {


    AppCompatActivity activity;

    ActivityResetPasswordBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_reset_password);
        activity = this;

        binding.imgBack.setOnClickListener(view -> {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });

        binding.btnChangePassword.setOnClickListener(view -> {
            if (binding.edtCurrentPassword.getText().toString().trim().equalsIgnoreCase("")) {
                Common.showSnackbar(view, getString(R.string.enter_current_password));
            } else if (binding.edtCurrentPassword.getText().length() < 8) {
                Common.showSnackbar(view, getString(R.string.current_password_should_be_min));
            } else if (binding.edtCurrentPassword.getText().toString().equals(Common.getPreferences(this,"password"))) {
                Common.showSnackbar(view, getString(R.string.old_pass_not_matched));
            } else if (binding.edtNewPassword.getText().toString().trim().equalsIgnoreCase("")) {
                Common.showSnackbar(view, getString(R.string.enter_new_password));
            } else if (binding.edtNewPassword.getText().length() < 8) {
                Common.showSnackbar(view, getString(R.string.new_password_should_be_min));
            } else if (!Common.isValidPassword(binding.edtNewPassword.getText().toString())) {
                Common.showSnackbar(view, getString(R.string.new_password_should_be_alphanemuric));
            } else if (!binding.edtConfirmPassword.getText().toString().trim().equals(binding.edtNewPassword.getText().toString())) {
                Common.showSnackbar(view, getString(R.string.password_not_match));
            } else {
                callAPIForgot();
            }

        });
    }


    private void callAPIForgot() {
        RequestedServiceDataModel request = new RequestedServiceDataModel(activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setTag(CHANGE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);

        request.putQurry(ApiClass.getmApiClass().OLD_PASSWORD, binding.edtCurrentPassword.getText().toString());
        request.putQurry(ApiClass.getmApiClass().NEW_PASSWORD, binding.edtNewPassword.getText().toString());
        request.putQurry(ApiClass.getmApiClass().PASSWORD_CONFIRMATION, binding.edtConfirmPassword.getText().toString());

        baseRequestData.setApiType(Constant.CHANGEPASSWORD);
        request.setBaseRequestData(baseRequestData);
        request.execute(this);
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {
        Common.showToast(activity, message);

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) throws JSONException {

        if (baseRequestData.getTag() == CHANGE) {
            Common.showToast(activity, message);
            finish();
        }

    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        Common.showToast(activity, message);
    }
}