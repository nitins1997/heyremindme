package com.reminder.app.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.reminder.app.R;
import com.reminder.app.databinding.ActivityWebBinding;
import com.reminder.app.utils.Constant;


public class WebActivity extends AppCompatActivity {

    String from;

    ActivityWebBinding binding;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web);
        from = getIntent().getStringExtra("from");
        binding.setToolbartext(from);
        WebView w = findViewById(R.id.web);
        if (from.equalsIgnoreCase("Terms & Conditions")) {
            w.loadUrl(Constant.BASE_URL + "terms-conditions");
        } else if (from.equalsIgnoreCase("Privacy Policy")) {
            w.loadUrl(Constant.BASE_URL + "privacy-policy");
        } else {
            w.loadUrl(Constant.BASE_URL + "about-us");
        }
        w.getSettings().setJavaScriptEnabled(true);
        w.setWebViewClient(new WebViewClient());
        binding.imgBack.setOnClickListener(view -> onBackPressed());
    }
}
