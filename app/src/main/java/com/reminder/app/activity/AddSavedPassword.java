package com.reminder.app.activity;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.reminder.app.R;
import com.reminder.app.databinding.ActivityAddSavedPasswordBinding;
import com.reminder.app.network.ApiClass;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;

import java.util.HashMap;
import java.util.Map;

public class AddSavedPassword extends BaseActivity implements View.OnClickListener {

    ActivityAddSavedPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_saved_password);

        if (getIntent().getStringExtra(Common.key.description) != null) {
            binding.edtDesc.setText(getIntent().getStringExtra(Common.key.description));
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.btnSave:
                validation();
                break;
        }

    }

    private void validation() {
        if (binding.edtTitle.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_enter_username));
        } else if (binding.edtPass.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_enter_password));
        } else if (binding.edtAccountType.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_enter_accountType));
        } else if (binding.edtDesc.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_enter_description));
        } else {
            Map<String, String> apiRequest = new HashMap<>();
            apiRequest.put(ApiClass.Username, binding.edtTitle.getText().toString());
            apiRequest.put(ApiClass.Password, binding.edtPass.getText().toString());
            apiRequest.put(ApiClass.AccountType, binding.edtAccountType.getText().toString());//optional
            apiRequest.put(ApiClass.STATUS, "0");//optional
            apiRequest.put(ApiClass.DESCRIPTION, binding.edtDesc.getText().toString());
            new RetroService().call(this, MainApp.getInstance().getApiServices().savePassword(apiRequest),
                    result -> {
                        if (result.isStatus()) {
                            setResult(Common.onActivityCode.savePassResult);
                            finish();
                        }
                        Common.showToast(this, result.getMessage());
                    });
        }

    }
}