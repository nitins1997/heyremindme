package com.reminder.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.reminder.app.R;
import com.reminder.app.databinding.ActivitySubscriptionPlansBinding;
import com.reminder.app.network.ApiClass;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;

import java.util.HashMap;

public class SubscriptionPlansActivity extends BaseActivity implements BillingProcessor.IBillingHandler {
    /*subscription parameter*/
    private static final String LOG_TAG = "iabv3";
    // SUBSCRIPTION IDS
    private static final String SUBSCRIPTION_ID = "remindme";
    private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnhiq0+xWum3mfUSB/OPeJjJ8/P+TzZwledz9Zln1Xj8HHq7qqCJnYJCBe52bFZBokVbTceXZPA72RpX0VMeSRQ7764EfARgqMm5KmNLp5rlyWsowa9+dMbo+euqQsHLbTBtA06PtJjm27gi3svDVtRmpaefl+AAnfoWeE1ZO+W/8nBv4zy37i8pS4pKPByR6yJ6gCdoK0o1APp/XHu/p/FHKL9oyORAgRINa47TK24hn+dOUTLqjZLnuYM+5Y3spFDf3KjPcOay5IXFmNR6AtW/ASGXN38+3fpY2XEcUqFg+JJcaVyM62QP3KxH5TjbvrKc/sGQDU7tczZAvdkYGhQIDAQAB"; // PUT YOUR MERCHANT KEY HERE;
    // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
    // if filled library will provide protection against Freedom alike Play Market simulators
    private static final String MERCHANT_ID = null;
    ActivitySubscriptionPlansBinding binding;
    private BillingProcessor bp;
    private boolean readyToPurchase = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_subscription_plans);
        clicks();

        /*billing subscription value initialize*/
        billingInitialize();

//        callAPIListReminder();
    }

    /*use to add reminder*/
    private void callAPIListReminder() {
        HashMap<String, String> request = new HashMap<>();
        request.put(ApiClass.PAGE, "com.monthlyplan.app");
        new RetroService().call(this, MainApp.getInstance().getApiServices().getPlan(request),
                result -> {
                    if (!result.isStatus()) {
                        Common.showToast(this, result.getMessage());
                    }
                });
    }

    private void billingInitialize() {
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            Common.showToast(this, "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16");
        }
        bp = new BillingProcessor(this, LICENSE_KEY, this);

        bp.initialize();

    }

    @Override
    public void onDestroy() {
        if (bp != null)
            bp.release();
        super.onDestroy();
    }

    private void callAPiforSubs(String s, String transactionid) {
        HashMap<String, String> request = new HashMap<>();
        request.put(ApiClass.transaction_id, s);
        request.put(ApiClass.product_id, "com.monthlyplan.app");
        request.put(ApiClass.amount, "4.99");
        request.put(ApiClass.platform, "Android");
        request.put(ApiClass.receipt, transactionid);
        request.put(ApiClass.payment_time, Common.getCurrentDate());
        request.put(ApiClass.device_token, Common.getIntro(this, "device_token"));
        new RetroService().call(this, MainApp.getInstance().getApiServices().update_plan(request),
                result -> {
                    if (result.isStatus()) {
                        finishExtra(true);
                    }
                    Common.showToast(this, result.getMessage());
                });
    }

    /*"transaction_id" -> "GPA.3337-9855-0462-84288"
"amount" -> "4.99"
"product_id" -> "com.monthlyplan.app"
"payment_time" -> "19-04-2021"
"device_token" -> "eAHEaff6RN-6Nh0FTXsjIk:APA91bE-t52adlxXqM5T69834-yb48G5T5uL1roirL8hZX6rCe-SmxYp1yFcHJRCY3Eq_Lt8aS52Bh0ePPQ5r4dnXus4w1VQ-6G6lj0vBYThXAyXBj7i0e-2zXbfwhm7cseHMKQ4KnUF"
"receipt" -> "GPA.3337-9855-0462-84288"
"platform" -> "Android"*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
        else super.onActivityResult(requestCode, resultCode, data);
    }

    private void clicks() {
        binding.imgBack.setOnClickListener(view -> finishExtra(false));
        binding.btnSubscribe.setOnClickListener(view -> {
            if (binding.btnSubscribe.getText().toString().equals("Subscription Available")) {
                Common.showToast(this, "You are already subscribed");
            } else {
                bp.subscribe(this, SUBSCRIPTION_ID);
            }


        });
    }

    @Override
    public void onBackPressed() {
        finishExtra(false);

    }

    @Override
    public void onProductPurchased(@NonNull String productId, @androidx.annotation.Nullable TransactionDetails details) {
        assert details != null;
        String transactionid = details.purchaseInfo.purchaseData.orderId;
        String purchaseToken = details.purchaseInfo.purchaseData.purchaseToken;
        System.out.println("transcation id--- " + transactionid);

//        finishExtra(true);
        callAPiforSubs(transactionid, details.purchaseInfo.signature);
    }

    private void finishExtra(boolean isPurchase) {
        Intent intent = new Intent();
        intent.putExtra("isPurchase", isPurchase);
        setResult(415, intent);
        finish();
    }

    @Override
    public void onPurchaseHistoryRestored() {
        Common.showToast(SubscriptionPlansActivity.this, "onPurchaseHistoryRestored");
        for (String sku : bp.listOwnedProducts())
            Log.d(LOG_TAG, "Owned Managed Product: " + sku);
        for (String sku : bp.listOwnedSubscriptions())
            Log.d(LOG_TAG, "Owned Subscription: " + sku);

        TransactionDetails premiumTransactionDetails = bp.getPurchaseTransactionDetails("premium_id");



        if (premiumTransactionDetails == null) {
            Log.i(LOG_TAG, "onPurchaseHistoryRestored(): Havn't bought premium yet.");
//            binding.btnSubscribe.setClickable(true);
        } else {
            Log.i(LOG_TAG, "onPurchaseHistoryRestored(): Already purchases premium.");

//            binding.btnSubscribe.setClickable(false);
//            binding.btnSubscribe.setText("Subscribed");
        }

    }

    @Override
    public void onBillingError(int errorCode, @androidx.annotation.Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {
        readyToPurchase = true;
        bp.setPurchaseHistoryRestored();

        if (Common.getPreferences(this, Common.key.isPremium).equals("0")) {
            try {
                if (bp.getSubscriptionTransactionDetails(SUBSCRIPTION_ID).purchaseInfo != null) {
//                    binding.btnSubscribe.setText("Subscription Available");
                } else {
                    binding.btnSubscribe.setClickable(true);
//                    binding.btnSubscribe.setText("Subscribe");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
//            binding.btnSubscribe.setText("Subscription Available");
        }


//        for (String sku : bp.listOwnedSubscriptions()) {
//            if (sku.equals(SUBSCRIPTION_ID)) {
//                binding.btnSubscribe.setClickable(false);
//                binding.btnSubscribe.setText("Already Subscribe");
//            } else {
//                binding.btnSubscribe.setClickable(true);
//                binding.btnSubscribe.setText("Subscribe");
//
//            }
//            Log.d(LOG_TAG, "Owned Subscription: " + sku);
//
//        }


    }

}
