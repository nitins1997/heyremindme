package com.reminder.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.maps.model.LatLng;
import com.reminder.app.R;
import com.reminder.app.databinding.ActivityAddReminderDetailBinding;
import com.reminder.app.databinding.ActivityEditReminderBinding;
import com.reminder.app.models.FollowList;
import com.reminder.app.models.ReminderListModel;
import com.reminder.app.network.ApiClass;
import com.reminder.app.network.BaseRequestData;
import com.reminder.app.network.RequestedServiceDataModel;
import com.reminder.app.network.ResponseDelegate;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;

import org.json.JSONException;

import java.io.File;

import static com.reminder.app.network.ResponseType.UPDATEPROFILE;

public class EditReminderActivity extends BaseActivity implements View.OnClickListener, ResponseDelegate {
    ActivityEditReminderBinding binding;
    FollowList selectionList;

    String reminderId="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_reminder);

        if (getIntent() != null) {
            if (getIntent().getSerializableExtra(Common.key.remindData) != null) {
                ReminderListModel.DataBean remiderSearchModel = (ReminderListModel.DataBean) getIntent().getSerializableExtra(Common.key.remindData);
                reminderId= String.valueOf(remiderSearchModel.getId());
                binding.setTitle(remiderSearchModel.getTitle());
                binding.setDescription(remiderSearchModel.getDescription());
                binding.setDate(remiderSearchModel.getReminder_date());
                binding.setTime(Common.formatDateList(remiderSearchModel.getReminder_time()));
                binding.setLocation(remiderSearchModel.getAddress());

//                if (remiderSearchModel.getAddress()==null) {
//                    binding.txtLoc.setVisibility(View.GONE);
//                    binding.flLoc.setVisibility(View.GONE);
//                }
//                binding.setReminderData(remiderSearchModel.getre());

                String members = "";
                for (int i = 0; i < remiderSearchModel.getOthers().size(); i++) {
                    if (members.isEmpty()) {
                        members = remiderSearchModel.getOthers().get(i).getName();
                    } else {
                        members = members + ", " + remiderSearchModel.getOthers().get(i).getName();
                    }
                }
                binding.setMembers(members);
//                if (members.isEmpty()) {
//                    binding.flMember.setVisibility(View.GONE);
//                    binding.txtMemberTitle.setVisibility(View.GONE);
//                }



            }
        }
    }
    String usersId = "";
    String lat = "", lng = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Common.onActivityCode.requestCodeSpeech) {/*google speech text*/
            if (resultCode == Activity.RESULT_OK && null != data) {
                String speechText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0);
                binding.txtAddVoice.setText(speechText);
            }
        } else if (requestCode == Common.onActivityCode.reqestLocation && resultCode == Common.onActivityCode.resultLocation) {
            Log.d("address", data.getStringExtra(Common.key.address));
            lat = data.getStringExtra(Common.key.lat);
            lng = data.getStringExtra(Common.key.lng);
            binding.edtLocation.setText(data.getStringExtra(Common.key.address));
        } else if (requestCode == Common.onActivityCode.requestUser && resultCode == Common.onActivityCode.resultUser) {
            selectionList = (FollowList) data.getSerializableExtra(Common.key.selectionList);
            String usersName = "";
            usersId = "";
            for (int i = 0; i < selectionList.getData().size(); i++) {
                if (usersName.isEmpty()) {
                    usersName = selectionList.getData().get(i).getGetfollower().getName();
                    usersId = selectionList.getData().get(i).getGetfollower().getId() + "";
                } else {
                    usersName = usersName + ", " + selectionList.getData().get(i).getGetfollower().getName();
                    usersId = usersId + ", " + selectionList.getData().get(i).getGetfollower().getId();
                }
            }

            binding.txtMember.setText(usersName);

        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.edtDate:
                Common.dateSelection(this, binding.edtDate, binding.edtTime);
                break;
//            case R.id.imgClear:
//                binding.txtAddVoice.setText(getString(R.string.add_audio_file));
//                AudioSavePathInDevice="";
//                binding.imgClear.setVisibility(View.GONE);
//                break;
            case R.id.imgRecordAudio:
//                recordAudio();
                break;
            case R.id.edtTime:
                if (binding.edtDate.getText().toString().isEmpty()) {
                    Common.showToast(this, "Please Select Date First");
                } else {
                    Common.timePicker(this, binding.edtTime, binding.edtDate.getText().toString());
                }
                break;
            case R.id.edtLocation:
                startActivityForResult(new Intent(this, MapsActivity.class), Common.onActivityCode.reqestLocation);
                break;
            case R.id.imgBack:
                finish();
                break;
//            case R.id.txtAddVoice://open google voice typing
//                if (binding.txtAddVoice.getText().toString().equals(getString(R.string.add_audio_file))) {
//                    binding.imgClear.setVisibility(View.GONE);
//                    recordAudio();
//                    binding.txtAddVoice.setText(getString(R.string.stop_recording));
//                } else if (binding.txtAddVoice.getText().toString().equals(getString(R.string.stop_recording))) {
//                    stopRecording();
//                    binding.txtAddVoice.setText(getString(R.string.play_recording));
//                    binding.imgClear.setVisibility(View.VISIBLE);
//                } else if (binding.txtAddVoice.getText().toString().equals(getString(R.string.play_recording))) {
//                    playAudio();
//                    binding.txtAddVoice.setText(getString(R.string.stop_playing));
//                    binding.imgClear.setVisibility(View.GONE);
//                } else if (binding.txtAddVoice.getText().toString().equals(getString(R.string.stop_playing))) {
//                    stopPlayingAudio();
//                    binding.txtAddVoice.setText(getString(R.string.play_recording));
//                    binding.imgClear.setVisibility(View.VISIBLE);
//                }
////                Common.voiceTyping(this);
//                break;
            case R.id.btnDone:
                validation();
                break;
            case R.id.imgAddFried:
            case R.id.txtMember:
                startActivityForResult(new Intent(this, FollowListActivity.class).putExtra(Common.key.isFrom, Common.key.addReminder).putExtra(Common.key.selectionList, selectionList), Common.onActivityCode.requestUser);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + id);
        }
    }

    /*use to check the validation before adding reminder*/
    private void validation() {
        if (binding.edtTitle.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_enter_title));
        } else if (binding.edtDate.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_select_date));
        } else if (binding.edtTime.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_select_time));
        } else if (binding.edtDesc.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_enter_description));
        } else {
            callAPIAddReminder();
        }
    }

    private RequestedServiceDataModel request;

    /*use to add reminder*/
    private void callAPIAddReminder() {
//
//
//
//
//        Map<String, String> apiRequest = new HashMap<>();
////        apiRequest.put(ApiClass.REMINDERDATA, binding.txtAddVoice.getText().toString());
////        apiRequest.put(ApiClass.REMINDERDATA, binding.edtDesc.getText().toString());
//        apiRequest.put(ApiClass.TITLE, binding.edtTitle.getText().toString());
//        apiRequest.put(ApiClass.REMINDER_DATE, Common.FormatDateApi(binding.edtDate.getText().toString()));
//        apiRequest.put(ApiClass.REMINDER_TIME, Common.formatDateForApi(binding.edtTime.getText().toString()));
//        apiRequest.put(ApiClass.DESCRIPTION, binding.edtDesc.getText().toString());
//        apiRequest.put(ApiClass.OTHER_USER_ID, "[" + usersId + "]");//optional
////        apiRequest.put(ApiClass.OTHER_USER_NAME, binding.txtMember.getText().toString());//optional
//        apiRequest.put(ApiClass.ADDRESS, binding.edtLocation.getText().toString());
//        apiRequest.put(ApiClass.REMINDER, binding.edtDesc.getText().toString());
//        apiRequest.put(ApiClass.LAT, lat);
//        apiRequest.put(ApiClass.LNG, lng);
//
//        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new File(AudioSavePathInDevice));
//
//        MultipartBody.Part reminderData= MultipartBody.Part.createFormData(ApiClass.REMINDERDATA, AudioSavePathInDevice, requestFile);
//
//        new RetroService().call(this,
//                MainApp.getInstance().getApiServices().addReminder(reminderData,apiRequest),
//                result -> {
//                    if (result.isStatus()) {
//                        try {
//                            startGeofencing(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)), 1);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        Common.showToast(AddReminder.this, result.getMessage());
//                        setResult(141);
//                        finish();
//                    } else {
//                        Common.showToast(AddReminder.this, result.getMessage());
//                    }
//                });


        request = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setTag(UPDATEPROFILE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);

        request.putQurry(ApiClass.TITLE, binding.edtTitle.getText().toString());
        request.putQurry(ApiClass.REMINDER_DATE, Common.FormatDateUtcApi(binding.edtDate.getText().toString()));
//        request.putQurry(ApiClass.REMINDER_DATE, Common.FormatDateApi(binding.edtDate.getText().toString()));
        request.putQurry(ApiClass.reminder_id, reminderId);
        request.putQurry(ApiClass.REMINDER_TIME, Common.formatDateForApi(binding.edtTime.getText().toString()));
        request.putQurry(ApiClass.DESCRIPTION, binding.edtDesc.getText().toString());
        request.putQurry(ApiClass.OTHER_USER_ID, "[" + usersId + "]");//optional
//      request.putQurryut(ApiClass.OTHER_USER_NAME, binding.txtMember.getText().toString());//optional
        request.putQurry(ApiClass.ADDRESS, binding.edtLocation.getText().toString());
        request.putQurry(ApiClass.REMINDER, binding.edtDesc.getText().toString());
        request.putQurry(ApiClass.LAT, lat);
        request.putQurry(ApiClass.LNG, lng);
        request.putQurry(ApiClass.timeZone, Common.getCurrentTimeZone());

//        request.putFiles(ApiClass.REMINDERDATA, new File(AudioSavePathInDevice));
        baseRequestData.setApiType(Constant.EDIT_REMINDER);
        request.setBaseRequestData(baseRequestData);
        request.execute(this);

    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) throws JSONException {
        try {
//            startGeofencing(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Common.showToast(this, message);
        setResult(141);
        finish();
    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        Common.showToast(this, message);

    }

}