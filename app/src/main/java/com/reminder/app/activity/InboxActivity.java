package com.reminder.app.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reminder.app.R;
import com.reminder.app.adapter.GeneralAdapter;
import com.reminder.app.databinding.ActivityInboxNewBinding;
import com.reminder.app.models.ChatModel;
import com.reminder.app.models.EventBusModel;
import com.reminder.app.models.UserResponse;
import com.reminder.app.service.ConnectionService;
import com.reminder.app.service.EventsName;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.reminder.app.service.EventsName.INBOX_LIST;


public class InboxActivity extends Activity implements View.OnClickListener {
    ActivityInboxNewBinding binding;
    GeneralAdapter<ChatModel> adapter;
    ChatModel chatModel;
    UserResponse userModel;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_inbox_new);
        EventBus.getDefault().register(this);

        userModel = new Gson().fromJson(Common.getPreferences(this, "data"), UserResponse.class);

        setAdapter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!Common.getInstance().isMyServiceRunning(this, ConnectionService.class))
            startService(new Intent(this, ConnectionService.class));
        getHistory();
    }

    private void getHistory() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", userModel.getData().getId());
            MainApp.getInstance().getSocket().emit(INBOX_LIST, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAdapter() {
        ArrayList<ChatModel> list = new ArrayList<>();
        adapter = new GeneralAdapter<>(R.layout.row_chat, list, (view, object) -> {
            chatModel = (ChatModel) object;
            Intent intent = new Intent(this, MessageActivity.class);
            intent.putExtra(Common.socketParameter.other_user_id, chatModel.getFinalId() + "");
            intent.putExtra(Common.socketParameter.other_user_name, chatModel.getFinalName());
            intent.putExtra(Common.socketParameter.other_user_image, chatModel.getProfile_image());
            startActivity(intent);
        });
        binding.recv.setLayoutManager(new LinearLayoutManager(this));
        binding.recv.setAdapter(adapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(EventBusModel model) {
        switch (model.getAction()) {
            case EventsName.INBOX_LIST_RESPONSE:
                List<ChatModel> list = new Gson().fromJson(model.getResponse()
                        , new TypeToken<List<ChatModel>>() {
                        }.getType());
                adapter.removeAll();
                adapter.add(list);

                if (list.size() == 0) {
                    binding.llNoData.setVisibility(View.VISIBLE);
                } else {
                    binding.llNoData.setVisibility(View.GONE);
                }
                break;
            case EventsName.RECEIVE_MESSAGE:
            case EventsName.READ_MESSAGE_UPDATE_RESPONSE:
                getHistory();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.imgAdd:
            case R.id.btnFollow:
                startActivity(new Intent(this, AllUserChatListActivity.class).putExtra(Common.key.isFrom, Common.key.inboxList));
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;

        }

    }
}