package com.reminder.app.activity;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.speech.RecognizerIntent;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.reminder.app.R;
import com.reminder.app.databinding.ActivityAddReminderBinding;
import com.reminder.app.models.FollowList;
import com.reminder.app.network.ApiClass;
import com.reminder.app.network.BaseRequestData;
import com.reminder.app.network.RequestedServiceDataModel;
import com.reminder.app.network.ResponseDelegate;
import com.reminder.app.service.GeofenceBroadcastReceiver;
import com.reminder.app.service.GeofenceRegistrationService;
import com.reminder.app.service.LocationService;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.reminder.app.network.ResponseType.UPDATEPROFILE;

public class AddReminder extends BaseActivity implements ResponseDelegate, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final String TAG = "AddReminderActivity";
    final int RC_LOCATION = 1002;
    final int RC_AUDIO = 1003;
    ActivityAddReminderBinding binding;
    String lat = "", lng = "";
    String usersId = "";
    FollowList selectionList;
    boolean isGoogleApiClientConnected = false;
    GoogleApiClient googleApiClient;
    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder;
    Random random;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    MediaPlayer mediaPlayer;
    CountDownTimer countDownTimer;
    int second = -1, minute, hour;
    private GeofencingClient geofencingClient;
    private boolean isMonitoring = false;
    private GeofencingRequest geofencingRequest;
    private PendingIntent pendingIntent;
    private RequestedServiceDataModel request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_reminder);

        /*if add remider is from the caledar date selectio*/
        if (getIntent() != null) {
            if (getIntent().getStringExtra(Common.key.isFrom) != null) {/*is from speech reminder*/
                if (getIntent().getStringExtra(Common.key.isFrom).equals(Common.key.addReminder)) {
                    binding.edtDate.setText(getIntent().getStringExtra(Common.key.date));
                    binding.edtDesc.setText(getIntent().getStringExtra(Common.key.remindData));
                    binding.edtTime.setText(getIntent().getStringExtra(Common.key.time));
                }
            } else {
                binding.edtDate.setText(getIntent().getStringExtra(Common.key.date));
            }
        }

        getLocationPermission();
        initGoogleApiClient();
        googleApiClient.reconnect();
        sendBroadcast(new Intent(this, GeofenceBroadcastReceiver.class));
        geofencingClient = LocationServices.getGeofencingClient(this);


    }

    public String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
//        while (i < string) {
//            stringBuilder.append(RandomAudioFileName.
//                    charAt(random.nextInt(RandomAudioFileName.length())));
//
//            i++;
//        }
        return "sd1";
    }

    //display recording time
    public void showTimer() {
        countDownTimer = new CountDownTimer(Long.MAX_VALUE, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                second++;
                binding.txtTime.setText(recorderTime());
                if (second == 59) {
                    if (binding.txtAddVoice.getText().toString().equals(getString(R.string.stop_recording))) {
                        stopRecording();
                        binding.txtAddVoice.setText(getString(R.string.play_recording));
                    }
                }
            }

            public void onFinish() {

            }
        };
        countDownTimer.start();
        startTime();
    }

    //recorder time
    public String recorderTime() {
        if (second == 60) {
            minute++;
            second = 0;
        }
        if (minute == 60) {
            hour++;
            minute = 0;
        }
        return String.format("%02d:%02d", minute, second);
    }

    //recorder time
    private void startTime() {
        if (second == 60) {
            minute++;
            second = 0;
        }
        if (minute == 60) {
            hour++;
            minute = 0;
        }
        binding.txtTime.setText(String.format("%02d:%02d", minute, second));
    }

    private void stopTime() {
        second = -1;
        minute = 0;
        hour = 0;
        binding.txtTime.setText("");
        countDownTimer.cancel();
    }

    @AfterPermissionGranted(RC_AUDIO)
    public void recordAudio() {
        String[] perms = {WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};
        if (EasyPermissions.hasPermissions(this, perms)) {
            AudioSavePathInDevice =
                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                            CreateRandomAudioFileName(1) + "AudioRecording.3gp";

            MediaRecorderReady();

            try {
                mediaRecorder.prepare();
                mediaRecorder.start();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//            buttonStart.setEnabled(false);
//            buttonStop.setEnabled(true);

            showTimer();
            Toast.makeText(AddReminder.this, "Recording started",
                    Toast.LENGTH_LONG).show();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.audio_rationale),
                    RC_AUDIO, perms);
        }


    }

    public void stopRecording() {
        mediaRecorder.stop();
        Toast.makeText(AddReminder.this, "Recording Completed",
                Toast.LENGTH_LONG).show();
        stopTime();
    }

    public void stopPlayingAudio() {
        try {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
                MediaRecorderReady();
                stopTime();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void playAudio() {
//            buttonStop.setEnabled(false);
//            buttonStart.setEnabled(false);
//            buttonStopPlayingRecording.setEnabled(true);

        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(AudioSavePathInDevice);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                stopTime();
                binding.txtAddVoice.setText(getString(R.string.play_recording));
                binding.imgClear.setVisibility(View.VISIBLE);

            }

        });

        mediaPlayer.start();
        Toast.makeText(AddReminder.this, "Recording Playing",
                Toast.LENGTH_LONG).show();
        showTimer();

    }

    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    private void iniatlizedgoogleApi() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(AddReminder.this)
                .addOnConnectionFailedListener(this).build();
    }

    private void checkGooglePLayService() {
        int response = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(AddReminder.this);
        if (response != ConnectionResult.SUCCESS) {
            Log.d(TAG, "Google Play Service Not Available");
            GoogleApiAvailability.getInstance().getErrorDialog(AddReminder.this, response, 1).show();
        } else {
            Log.d(TAG, "Google play service available");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        googleApiClient.disconnect();
        try {
            mediaPlayer.stop();
            mediaPlayer.release();
            MediaRecorderReady();
            countDownTimer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @AfterPermissionGranted(RC_LOCATION)
    private void getLocationPermission() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            startService(new Intent(this, LocationService.class));
        } else {
            showLocationDialog(this,RC_LOCATION);


        }
    }


    private void showLocationDialog(Activity activity, final int requestCode) {
        LayoutInflater factory = LayoutInflater.from(activity);
        final View deleteDialogView = factory.inflate(R.layout.dialog_location, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(activity).create();
        deleteDialog.setView(deleteDialogView);
        ((TextView) (deleteDialogView.findViewById(R.id.tvTitle))).setMovementMethod(new ScrollingMovementMethod());


        deleteDialogView.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
                activity.finish();
            }
        });
        deleteDialogView.findViewById(R.id.btnRetry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
                String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION};

                EasyPermissions.requestPermissions(activity, activity.getString(R.string.location_rationale),
                        RC_LOCATION, perms);
            }
        });

        deleteDialog.show();

    }




    private void initGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
    }

    private Geofence getGeofence(LatLng latLng, int id) {
        return new Geofence.Builder()
                .setRequestId(id + "")
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setCircularRegion(latLng.latitude, latLng.longitude, 200)
                .setNotificationResponsiveness(1000)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT
                )
                .build();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.edtDate:
                Common.dateSelection(this, binding.edtDate, binding.edtTime);
                break;
            case R.id.imgClear:
                binding.txtAddVoice.setText(getString(R.string.add_audio_file));
                AudioSavePathInDevice="";
                binding.imgClear.setVisibility(View.GONE);
                break;
            case R.id.imgRecordAudio:
//                recordAudio();
                break;
            case R.id.edtTime:
                stopPlayingAudio();
                if (binding.edtDate.getText().toString().isEmpty()) {
                    Common.showToast(this, "Please Select Date First");
                } else {
                    Common.timePicker(this, binding.edtTime, binding.edtDate.getText().toString());
                }
                break;
            case R.id.edtLocation:
                startActivityForResult(new Intent(this, MapsActivity.class), Common.onActivityCode.reqestLocation);
                break;
            case R.id.imgBack:
                finish();
                break;
            case R.id.txtAddVoice://open google voice typing
                if (binding.txtAddVoice.getText().toString().equals(getString(R.string.add_audio_file))) {
                    binding.imgClear.setVisibility(View.GONE);
                    recordAudio();
                    binding.txtAddVoice.setText(getString(R.string.stop_recording));
                } else if (binding.txtAddVoice.getText().toString().equals(getString(R.string.stop_recording))) {
                    stopRecording();
                    binding.txtAddVoice.setText(getString(R.string.play_recording));
                    binding.imgClear.setVisibility(View.VISIBLE);
                } else if (binding.txtAddVoice.getText().toString().equals(getString(R.string.play_recording))) {
                    playAudio();
                    binding.txtAddVoice.setText(getString(R.string.stop_playing));
                    binding.imgClear.setVisibility(View.GONE);
                } else if (binding.txtAddVoice.getText().toString().equals(getString(R.string.stop_playing))) {
                    stopPlayingAudio();
                    binding.txtAddVoice.setText(getString(R.string.play_recording));
                    binding.imgClear.setVisibility(View.VISIBLE);
                }
//                Common.voiceTyping(this);
                break;
            case R.id.btnDone:
                validation();
                break;
            case R.id.imgAddFried:
            case R.id.txtMember:
                startActivityForResult(new Intent(this, FollowListActivity.class).putExtra(Common.key.isFrom, Common.key.addReminder).putExtra(Common.key.selectionList, selectionList), Common.onActivityCode.requestUser);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + id);
        }
    }

    /*use to add reminder*/
    private void callAPIAddReminder() {
        request = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setTag(UPDATEPROFILE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);

        request.putQurry(ApiClass.TITLE, binding.edtTitle.getText().toString());
//        request.putQurry(ApiClass.REMINDER_DATE, Common.FormatDateApi(binding.edtDate.getText().toString()));
        request.putQurry(ApiClass.REMINDER_DATE, Common.FormatDateUtcApi(binding.edtDate.getText().toString()+binding.edtTime.getText().toString()));
        request.putQurry(ApiClass.REMINDER_TIME, Common.formatDateForApi(binding.edtTime.getText().toString()));
        request.putQurry(ApiClass.DESCRIPTION, binding.edtDesc.getText().toString());
        request.putQurry(ApiClass.OTHER_USER_ID, "[" + usersId + "]");//optional
//      request.putQurryut(ApiClass.OTHER_USER_NAME, binding.txtMember.getText().toString());//optional
        request.putQurry(ApiClass.ADDRESS, binding.edtLocation.getText().toString());
        request.putQurry(ApiClass.REMINDER, binding.edtDesc.getText().toString());
        request.putQurry(ApiClass.LAT, lat);
        request.putQurry(ApiClass.timeZone, Common.getCurrentTimeZone());

        request.putQurry(ApiClass.LNG, lng);
        request.putFiles(ApiClass.REMINDERDATA, new File(AudioSavePathInDevice));
        baseRequestData.setApiType(Constant.ADD_REMINDER);
        request.setBaseRequestData(baseRequestData);
        request.execute(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Common.onActivityCode.requestCodeSpeech) {/*google speech text*/
            if (resultCode == Activity.RESULT_OK && null != data) {
                String speechText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0);
                binding.txtAddVoice.setText(speechText);
            }
        } else if (requestCode == Common.onActivityCode.reqestLocation && resultCode == Common.onActivityCode.resultLocation) {
            Log.d("address", data.getStringExtra(Common.key.address));
            lat = data.getStringExtra(Common.key.lat);
            lng = data.getStringExtra(Common.key.lng);
            binding.edtLocation.setText(data.getStringExtra(Common.key.address));
        } else if (requestCode == Common.onActivityCode.requestUser && resultCode == Common.onActivityCode.resultUser) {
            selectionList = (FollowList) data.getSerializableExtra(Common.key.selectionList);
            String usersName = "";
            usersId = "";
            for (int i = 0; i < selectionList.getData().size(); i++) {
                if (usersName.isEmpty()) {
                    usersName = selectionList.getData().get(i).getGetfollower().getName();
                    usersId = selectionList.getData().get(i).getGetfollower().getId() + "";
                } else {
                    usersName = usersName + ", " + selectionList.getData().get(i).getGetfollower().getName();
                    usersId = usersId + ", " + selectionList.getData().get(i).getGetfollower().getId();
                }
            }

            binding.txtMember.setText(usersName);

        }
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (pendingIntent != null) {
            return pendingIntent;
        }
//        Intent intent = new Intent(this, GeofenceBroadcastReceiver.class);
        Intent intent = new Intent(this, GeofenceRegistrationService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return pendingIntent;
    }

    private void startGeofencing(LatLng latLng, int id) {
        Log.d(TAG, "Start geofencing monitoring call");
        pendingIntent = getGeofencePendingIntent();
        geofencingRequest = new GeofencingRequest.Builder()
                .setInitialTrigger(Geofence.GEOFENCE_TRANSITION_ENTER)
                .addGeofence(getGeofence(latLng, id))
                .build();

        if (!googleApiClient.isConnected()) {
            Log.d(TAG, "Google API client not connected");
        } else {
            try {
                LocationServices.GeofencingApi.addGeofences(googleApiClient
                        , geofencingRequest
                        , pendingIntent).setResultCallback(status -> {
                    if (status.isSuccess()) {
                        Log.d(TAG, "Successfully Geofencing Connected");
                    } else {
                        Log.d(TAG, "Failed to add Geofencing " + status.getStatus());
                    }
                });
            } catch (SecurityException e) {
                Log.d(TAG, e.getMessage());
            }
        }
        isMonitoring = true;
        invalidateOptionsMenu();
    }

    /*use to check the validation before adding reminder*/
    private void validation() {
        if (binding.edtTitle.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_enter_title));
        } else if (binding.txtAddVoice.getText().toString().equals(getString(R.string.add_audio_file))) {
            Common.showToast(this, getString(R.string.please_add_voice_note));
        } else if (binding.txtAddVoice.getText().toString().equals(getString(R.string.stop_recording))) {
            Common.showToast(this, getString(R.string.first_stop_recording));
        } else if (binding.edtDate.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_select_date));
        } else if (binding.edtTime.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_select_time));
        } else if (binding.edtDesc.getText().toString().isEmpty()) {
            Common.showToast(this, getString(R.string.please_enter_description));
        } else {
            callAPIAddReminder();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "Google Api Client Connected");
        isMonitoring = true;
        isGoogleApiClientConnected = true;
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Google Connection Suspended");
        isGoogleApiClientConnected = false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        isMonitoring = false;
        isGoogleApiClientConnected = false;
        Log.e(TAG, "Connection Failed:" + connectionResult.getErrorMessage());
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {
        Common.showToast(AddReminder.this, message);

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) throws JSONException {
        try {
            startGeofencing(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Common.showToast(AddReminder.this, message);
        setResult(141);
        finish();
    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {

    }
}