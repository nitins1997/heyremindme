package com.reminder.app.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.reminder.app.R;
import com.reminder.app.adapter.GeneralAdapter;
import com.reminder.app.databinding.ActivityFollowListBinding;
import com.reminder.app.models.FollowList;
import com.reminder.app.network.ApiClass;
import com.reminder.app.utils.BaseView;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;

import java.util.HashMap;

public class FollowListActivity extends BaseActivity implements View.OnClickListener {

    int page = 1;
    boolean loadAgain = false;
    ActivityFollowListBinding binding;
    FollowList allUserList;

    FollowList selectionList = new FollowList();
    private GeneralAdapter generalAdapter;
    private String isFrom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_follow_list);

        if (getIntent().getStringExtra(Common.key.isFrom) != null) {
            isFrom = getIntent().getStringExtra(Common.key.isFrom);
            if ((FollowList) getIntent().getSerializableExtra(Common.key.selectionList) != null) {
                selectionList = (FollowList) getIntent().getSerializableExtra(Common.key.selectionList);
                if (selectionList.getData().size() != 0) {
                    binding.btnDone.setVisibility(View.VISIBLE);
                }
            }


            if (isFrom.equals(Common.key.inboxList)) {
                binding.txtHeader.setText("Inbox List");
            }
        }
        callFollowList();
        initScrollListener();

    }


    private void callFollowList() {
        HashMap<String, String> request = new HashMap<>();
        request.put(ApiClass.PAGE, String.valueOf(page));

        new RetroService().call((BaseView) this, MainApp.getInstance().getApiServices().followList(request),
                result -> {
                    if (result.isStatus()) {
                        if (allUserList == null) {
                            allUserList = result.getData();
                            if (allUserList.getData().size() == 0) {
                                binding.recv.setVisibility(View.GONE);
                                binding.llNoData.setVisibility(View.VISIBLE);

                            } else {
                                binding.recv.setVisibility(View.VISIBLE);
                                binding.llNoData.setVisibility(View.GONE);

                                /*set previous selction true*/
                                setSelectionTrue(allUserList);
                                setAdapterOnRecycleView();
                                loadAgain = true;

                            }
                        } else {
                            binding.recv.setVisibility(View.VISIBLE);
                            FollowList reminderListModel1 = result.getData();
                            if (reminderListModel1.getData().size() == 0) {
                                loadAgain = false;
                            } else {
                                setSelectionTrue(reminderListModel1);

                                generalAdapter.add(reminderListModel1.getData());
                                loadAgain = true;

                            }
                        }
                    } else {
                        Common.showToast(this, result.getMessage());
                    }
                });
    }

    private void setSelectionTrue(FollowList allUserList) {
        if (selectionList.getData().size() != 0) {

            for (int i = 0; i < allUserList.getData().size(); i++) {
                for (int j = 0; j < selectionList.getData().size(); j++) {
                    if (allUserList.getData().get(i).getGetfollower().getId() == selectionList.getData().get(j).getGetfollower().getId()) {
                        allUserList.getData().get(i).setSelection(true);
                    }
                }

            }
        }
    }


    private void setAdapterOnRecycleView() {
        generalAdapter = new GeneralAdapter(R.layout.row_follow, allUserList.getData(), (view, object1) -> {
            FollowList.DataBean singleUser = (FollowList.DataBean) object1;
            switch (view.getId()) {
                case R.id.txtBtnFollow:
                    HashMap<String, String> requestFollow = new HashMap<>();
                    requestFollow.put(ApiClass.OTHER_USER_ID, singleUser.getGetfollower().getId() + "");
                    if (singleUser.getStatus() == 1) {
                        requestFollow.put(ApiClass.STATUS, "0");
                        singleUser.setStatus(0);
                    } else {
                        requestFollow.put(ApiClass.STATUS, "1");
                        singleUser.setStatus(1);
                    }
                    new RetroService().call((BaseView) this, MainApp.getInstance().getApiServices().followUnfollow(requestFollow), result1 -> {
                        Common.showToast(FollowListActivity.this, result1.getMessage());
                    });
                    break;
                case R.id.clAll:
                    if (isFrom.equals(Common.key.addReminder)) {/*work this when come from the add remind screen*/
                        if (singleUser.isSelection()) {
                            singleUser.setSelection(false);
                            deleteSelection(singleUser);
                        } else {
                            singleUser.setSelection(true);
                            selectionList.getData().add(singleUser);
                        }
                        binding.btnDone.setVisibility(View.VISIBLE);
                    } else if (isFrom.equals(Common.key.inboxList)) {
                        Intent intent=new Intent(this,MessageActivity.class);
                        intent.putExtra(Common.socketParameter.other_user_id,singleUser.getGetfollower().getId()+"");
                        intent.putExtra(Common.socketParameter.other_user_image,singleUser.getGetfollower().getProfile_image());
                        intent.putExtra(Common.socketParameter.other_user_name,singleUser.getGetfollower().getName());
                        startActivity(intent);

//                        Uri sms_uri = Uri.parse("smsto:" + singleUser.getGetfollower().getPhone());
//                        Intent sms_intent = new Intent(Intent.ACTION_SENDTO, sms_uri);
//                        sms_intent.putExtra("sms_body", "Good Morning ! how r U ?");
//                        startActivity(sms_intent);

                    }

            }

        });
        binding.recv.setLayoutManager(new LinearLayoutManager(this));
        binding.recv.setAdapter(generalAdapter);

    }


    /*use to delte the selection item */
    void deleteSelection(FollowList.DataBean singleUser) {
        for (int i = 0; i < selectionList.getData().size(); i++) {
            if (selectionList.getData().get(i).getGetfollower().getId() == singleUser.getGetfollower().getId()) {
                selectionList.getData().remove(i);
                break;
            }
        }
    }

    private void initScrollListener() {
        binding.recv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == allUserList.getData().size() - 1) {
                        Log.d("lastitem", "visible");
                        if (loadAgain) {
                            page = page + 1;
                            callFollowList();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Common.onActivityCode.requestUser && resultCode == Common.onActivityCode.resultUser) {
            selectionList = new FollowList();
            if (generalAdapter != null) {
                generalAdapter.removeAll();
            }
            allUserList = null;
            page = 1;
            loadAgain = false;
            callFollowList();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgAdd:
            case R.id.btnFollow:
                startActivityForResult(new Intent(this, AllUserListActivity.class), Common.onActivityCode.requestUser);
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.btnDone:
                if (selectionList.getData().size() == 0) {
                    finish();
                } else {
                    Intent intent = new Intent();
                    intent.putExtra(Common.key.selectionList, selectionList);
                    setResult(Common.onActivityCode.resultUser, intent);
                    finish();
                }
                break;
        }
    }

}