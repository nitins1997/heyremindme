package com.reminder.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.reminder.app.R;
import com.reminder.app.adapter.GeneralAdapter;
import com.reminder.app.databinding.ActivityAllUserListBinding;
import com.reminder.app.models.AllUserList;
import com.reminder.app.network.ApiClass;
import com.reminder.app.utils.BaseView;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;

import java.util.HashMap;

public class AllUserChatListActivity extends BaseActivity implements View.OnClickListener {

    int page = 1;
    boolean loadAgain = false;
    ActivityAllUserListBinding binding;
    GeneralAdapter generalAdapter;
    AllUserList allUserList;
    String searchTxt = "";

    boolean isChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_all_user_list);

        callAllUserList(true);
        initScrollListener();

        binding.edtSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchTxt = charSequence.toString();
                if (searchTxt.length() >= 3) {
                    allUserList = null;
                    page = 1;
                    callAllUserList(false);
                }

                if (searchTxt.isEmpty()) {
                    allUserList = null;
                    page = 1;
                    callAllUserList(true);
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

    }

    private void callAllUserList(boolean isShow) {
        HashMap<String, String> request = new HashMap<>();
        request.put(ApiClass.PAGE, String.valueOf(page));

        request.put(ApiClass.KEYWORD, searchTxt);

        new RetroService().callWithProgressStatus((BaseView) this, MainApp.getInstance().getApiServices().allUser(request),
                result -> {
                    if (result.isStatus()) {
                        if (allUserList == null) {
                            allUserList = result.getData();
                            if (allUserList.getData().size() == 0) {
                                binding.recv.setVisibility(View.GONE);

                            } else {
                                binding.recv.setVisibility(View.VISIBLE);
                                setAdapterOnRecycleView();
                                loadAgain = true;

                            }
                        } else {
                            binding.recv.setVisibility(View.VISIBLE);
                            AllUserList reminderListModel1 = result.getData();
                            if (reminderListModel1.getData().size() == 0) {
                                loadAgain = false;
                            } else {
                                generalAdapter.add(reminderListModel1.getData());
                                loadAgain = true;

                            }
                        }
                    } else {
                        Common.showToast(this, result.getMessage());
                    }
                }, isShow);
    }

    private void setAdapterOnRecycleView() {
        generalAdapter = new GeneralAdapter(R.layout.row_chat_all_item, allUserList.getData(), (view, object1) -> {
            AllUserList.DataBean singleUser = (AllUserList.DataBean) object1;
            switch (view.getId()) {
                case R.id.txtBtnFollow:
                    HashMap<String, String> requestFollow = new HashMap<>();
                    requestFollow.put(ApiClass.OTHER_USER_ID, singleUser.getId() + "");
                    if (singleUser.getFollow_status() == 1) {
                        requestFollow.put(ApiClass.STATUS, "0");
                        singleUser.setFollow_status(0);
                    } else {
                        requestFollow.put(ApiClass.STATUS, "1");
                        singleUser.setFollow_status(1);
                    }
                    new RetroService().call((BaseView) this, MainApp.getInstance().getApiServices().followUnfollow(requestFollow), result1 -> {
                        Common.showToast(AllUserChatListActivity.this, result1.getMessage());
                        isChange = true;

                    });


                    break;
                case R.id.cardAll:
                    Intent intent = new Intent(this, MessageActivity.class);
                    intent.putExtra(Common.socketParameter.other_user_id, singleUser.getId() + "");
                    intent.putExtra(Common.socketParameter.other_user_image, singleUser.getProfile_image());
                    intent.putExtra(Common.socketParameter.other_user_name, singleUser.getName());
                    startActivity(intent);
                    break;
            }

        });
        binding.recv.setLayoutManager(new LinearLayoutManager(this));
        binding.recv.setAdapter(generalAdapter);

    }


    private void initScrollListener() {
        binding.recv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                try {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == allUserList.getData().size() - 1) {
                        Log.d("lastitem", "visible");
                        if (loadAgain) {
                            page = page + 1;
                            callAllUserList(true);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isChange) {
            setResult(Common.onActivityCode.resultUser);
        }
        finish();
    }
}