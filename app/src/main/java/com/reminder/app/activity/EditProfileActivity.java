package com.reminder.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.fxn.pix.Pix;
import com.google.gson.Gson;
import com.reminder.app.R;
import com.reminder.app.databinding.ActivityEditBinding;
import com.reminder.app.models.UserResponse;
import com.reminder.app.network.ApiClass;
import com.reminder.app.network.BaseRequestData;
import com.reminder.app.network.RequestedServiceDataModel;
import com.reminder.app.network.ResponseDelegate;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.reminder.app.network.ResponseType.UPDATEPROFILE;
import static com.reminder.app.network.ResponseType.UPDATEPROFILEIMAGE;

public class EditProfileActivity extends AppCompatActivity implements ResponseDelegate {

    ActivityEditBinding binding = null;
    AppCompatActivity activity;
    File profileFile;
    UserResponse userResponse;
    private RequestedServiceDataModel request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit);
        activity = this;

        userResponse = new Gson().fromJson(Common.getPreferences(this, "data"), UserResponse.class);
        binding.setUserresponse(userResponse);


        if (userResponse.getData().getEmail().equalsIgnoreCase("")) {
            binding.edtEmail.setEnabled(true);
            binding.edtEmail.setInputType(InputType.TYPE_CLASS_TEXT);
            binding.edtEmail.setFocusable(true);
        } else {

            binding.edtEmail.setEnabled(false);
            binding.edtEmail.setInputType(InputType.TYPE_NULL);
            binding.edtEmail.setFocusable(false);

        }

        Common.INIT_CONTACT_PICASSO(this, userResponse.getData().getProfile_image(), binding.cvProfile);
        try {

        } catch (Exception e) {
            binding.countryCode.setCountryForPhoneCode(Integer.parseInt(userResponse.getData().getCountry_code()));
            e.printStackTrace();
        }
        binding.imgBack.setOnClickListener(view -> {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });

        binding.cvProfile.setOnClickListener(view -> {
            Common.INIT_PIX(activity, 454);

        });


        binding.btnUpdate.setOnClickListener(view -> {
            if (binding.edtName.getText().toString().isEmpty()) {
                Common.showSnackbar(view, getString(R.string.empty_name));
            } else if (TextUtils.isEmpty(binding.edtEmail.getText().toString())) {
                Common.showSnackbar(view, getString(R.string.enter_email_address));
            } else if (!Common.isValidEmail(binding.edtEmail.getText().toString())) {
                Common.showSnackbar(view, getString(R.string.invalid_email));
            } else if (!Common.isValidEmail(binding.edtEmail.getText().toString())) {
                Common.showSnackbar(view, getString(R.string.invalid_email));
            } else if (!binding.edtPhone.getText().toString().trim().equalsIgnoreCase("") && binding.edtPhone.getText().length() != 10) {
                Common.showSnackbar(view, getString(R.string.invalid_phone));
            } else {
                callAPIUpdate();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 454 && resultCode == RESULT_OK) {
            ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
            profileFile = new File(returnValue.get(0));

            callAPIUpdateProfile();
        }
    }

    private void callAPIUpdate() {
        request = new RequestedServiceDataModel(activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setTag(UPDATEPROFILE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);

        request.putQurry(ApiClass.getmApiClass().NAME, binding.edtName.getText().toString());


        if (userResponse.getData().getEmail().equalsIgnoreCase("")) {
            request.putQurry(ApiClass.getmApiClass().EMAIL, binding.edtEmail.getText().toString());
        }
        request.putQurry(ApiClass.getmApiClass().COUNTRY_CODE, binding.countryCode.getSelectedCountryCode());
        request.putQurry(ApiClass.getmApiClass().PHONE, binding.edtPhone.getText().toString().trim());


        baseRequestData.setApiType(Constant.UPDATEPROFILE);
        request.setBaseRequestData(baseRequestData);
        request.execute(this);
    }

    private void callAPIUpdateProfile() {
        request = new RequestedServiceDataModel(activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setTag(UPDATEPROFILEIMAGE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);

        request.putFiles(ApiClass.getmApiClass().PROFILE_IMAGE, profileFile);

        baseRequestData.setApiType(Constant.UPDATEPROFILEIMAGE);
        request.setBaseRequestData(baseRequestData);
        request.execute(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {
        Common.showToast(this, message);
    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) throws JSONException {

        try {
            if (baseRequestData.getTag() == UPDATEPROFILE) {


                JSONObject json = new JSONObject(jsondata);

                if (json.has("data")) {

                    JSONObject dataObject = json.optJSONObject("data");

                    if (dataObject != null) {
                        Common.setPreferences(activity, "data", jsondata);
                        Common.showToast(this, message);
                        finish();
                        //Do things with object.

                    } else {

                        JSONArray array = json.optJSONArray("data");
                        Common.showToast(this, message);

                        //Do things with array
                    }
                } else {
                    // Do nothing or throw exception if "data" is a mandatory field
                }


               /* Object json = new JSONTokener(jsondata).nextValue();
                if (json instanceof JSONObject){
                    Common.setPreferences(activity, "data", jsondata);
                }
                //you have an object
                else if (json instanceof JSONArray){
                    Common.showToast(this, message);
                }

                finish();*/
            } else if (baseRequestData.getTag() == UPDATEPROFILEIMAGE) {


                JSONObject json = new JSONObject(jsondata);

                if (json.has("data")) {

                    JSONObject dataObject = json.optJSONObject("data");

                    if (dataObject != null) {
                        Common.showToast(this, message);
                        Common.setPreferences(activity, "data", jsondata);
                        UserResponse userResponse = new Gson().fromJson(Common.getPreferences(activity, "data"), UserResponse.class);
                        Common.INIT_CIRCLE_CONTACT_PICASSO(activity, userResponse.getData().getProfile_image(), binding.cvProfile);
//                        finish();
                        //Do things with object.

                    } else {

                        JSONArray array = json.optJSONArray("data");
                        Common.showToast(this, message);

                        //Do things with array
                    }
                } else {
                    // Do nothing or throw exception if "data" is a mandatory field
                }


            }
        } catch (Exception e) {
            Common.showToast(this, "here");
        }
    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        Common.showToast(this, message);
    }
}