package com.reminder.app.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;
import com.reminder.app.R;
import com.reminder.app.models.UserResponse;
import com.reminder.app.network.ApiClass;
import com.reminder.app.network.BaseRequestData;
import com.reminder.app.network.RequestedServiceDataModel;
import com.reminder.app.network.ResponseDelegate;
import com.reminder.app.network.ResponseType;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import retrofit2.Call;

import static com.reminder.app.network.ResponseType.SIGNUP;

public class SignUpActivity extends AppCompatActivity implements ResponseDelegate {
    ImageView img_pas_show;

    ImageView img_pas_hide;

    EditText edtName;

    EditText edtEmail;

    EditText edtPass;

    Button btnSignUp;

    TextView txt_sign_up;

    AppCompatActivity activity;

    ImageView img_google;

    ImageView img_facebook;

    ImageView imgBack;
    int GoogleSiginReq = 101;
    private RequestedServiceDataModel request;
    private String fullName = "";
    private String emaill = "";
    private String socialId = "";
    private String profilePicture = "";
    private String socialType = "";
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        /*findviewss*/
        findViews();
        activity = SignUpActivity.this;
        facebookSetup();
        googleSetup();
        mAuth = FirebaseAuth.getInstance();

        /*password show hide*/
        passShowHide();

        /*button click*/
        buttonClick();

        /*twitter login*/
        twitterLogin();

    }

    private void findViews() {
        img_pas_show=findViewById(R.id.img_pas_show);
        img_pas_hide=findViewById(R.id.img_pas_hide);
        edtName=findViewById(R.id.edtName);
        edtEmail=findViewById(R.id.edtEmail);
        edtPass=findViewById(R.id.edtPass);
        btnSignUp=findViewById(R.id.btnSignUp);
        txt_sign_up=findViewById(R.id.txt_sign_up);
        img_google=findViewById(R.id.img_google);
        img_facebook=findViewById(R.id.img_facebook);
        imgBack=findViewById(R.id.imgBack);
        img_twitter=findViewById(R.id.img_twitter);
    }

    private TwitterAuthClient client;
    private TwitterLoginButton twitterLoginButton;

    private void twitterLogin() {
        //initialize twitter auth client
        client = new TwitterAuthClient();

        //find the id of views
        twitterLoginButton = findViewById(R.id.default_twitter_login_button);


        //NOTE : calling default twitter login in OnCreate/OnResume to initialize twitter callback
//        defaultLoginTwitter();
    }



    /*Facebook login setup here*/
    void facebookSetup() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        getFacebookData(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        exception.printStackTrace();
                    }
                });


        LoginManager.getInstance().logOut();
    }


    //get all the facebook data and access
    void getFacebookData(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            Log.e("Facebook", object.toString());
            try {
                fullName = object.getString("name");
                if (object.has("email"))
                    emaill = object.getString("email");
                socialId = object.getString("id");
                if (object.has("picture"))
                    profilePicture = object.getJSONObject("picture").getJSONObject("data").getString("url");
                socialType = "FACEBOOK";
//                new GetFileFromUrl().execute(profilePicture);
                callSocialChcekuserApi();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture.width(150).height(150),birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    //google login
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GoogleSiginReq);
    }

    /*Google login setup here*/
    void googleSetup() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    //google signout
    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, task -> {
                    // ...
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            if (socialType.equals("FACEBOOK")) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            } else if (socialType.equalsIgnoreCase("GMAIL")) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    Log.d("Gmail Login", "firebaseAuthWithGoogle:" + account.getId());
                    firebaseAuthWithGoogle(account.getIdToken());
                } catch (ApiException e) {
                    // Google Sign In failed, update UI appropriately
                    Log.w("Gmail Login", "Google sign in failed", e);
                    // ...
                }

//                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//                handleSignInResult(task);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("Gmial login", "signInWithCredential:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        fullName = user.getDisplayName();
                        emaill = user.getEmail();
                        socialId = user.getUid();
                        socialType = "GMAIL";
                        profilePicture = user.getPhotoUrl().toString();
//                        new GetFileFromUrl().execute(profilePicture);
                        callSocialChcekuserApi();
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("Gmial Logon", "signInWithCredential:failure", task.getException());
//                            Snackbar.make(mBinding.mainLayout, "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
//                            updateUI(null);
                    }

                    // ...
                });
    }


    //Caliing signup Api
    private void callSocialChcekuserApi() {
        request = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setTag(ResponseType.SOCIAL_SIGNIN);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        request.putQurry(ApiClass.NAME, fullName);
        request.putQurry(ApiClass.SOCIAL_ID, socialId);
        request.putQurry(ApiClass.SOCIAL_TYPE, socialType);
        request.putQurry(ApiClass.timeZone, Common.getCurrentTimeZone());
        request.putQurry(ApiClass.SOCIAL_TYPE, socialType);
        if (emaill != null && !emaill.equals("null")) {
            request.putQurry(ApiClass.EMAIL, emaill);
        }
//        if (profileFile != null) {
//            request.putFiles(ApiClass.PROFILE_PIC, profileFile);
//        }
        request.putQurry(ApiClass.PROFILE_PIC, profilePicture);
        request.putQurry("is_premium", "1");
        request.putQurry(ApiClass.DEVICE_TYPE, Constant.DEVICE_TYPE);
        request.putQurry(ApiClass.DEVICE_TOKEN, Common.getIntro(activity, "device_token"));
        baseRequestData.setApiType(Constant.SOCIALLOGIN);
        request.setBaseRequestData(baseRequestData);
        request.execute(this);

    }

    ImageView img_twitter;

    private void buttonClick() {
        txt_sign_up.setOnClickListener(view -> {

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
        });

        img_twitter.setOnClickListener(view -> {
            socialType = "TWITTER";
            customLoginTwitter(view);
        });
        imgBack.setOnClickListener(view -> finish());
        img_facebook.setOnClickListener(view -> {
            socialType = "FACEBOOK";
            LoginManager.getInstance().logOut();
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));

        });

        img_google.setOnClickListener(view -> {
            socialType = "GMAIL";
            signOut();
            signIn();
        });

        btnSignUp.setOnClickListener(view -> {
            if (edtName.getText().toString().isEmpty()) {
                Common.showSnackbar(view, getString(R.string.empty_name));
            } else if (TextUtils.isEmpty(edtEmail.getText().toString())) {
                Common.showSnackbar(view, getString(R.string.enter_email_address));
            } else if (!Common.isValidEmail(edtEmail.getText().toString())) {
                Common.showSnackbar(view, getString(R.string.invalid_email));
            } else if (!Common.isValidEmail(edtEmail.getText().toString())) {
                Common.showSnackbar(view, getString(R.string.invalid_email));
            } else if (edtPass.getText().toString().trim().equalsIgnoreCase("")) {
                Common.showSnackbar(view, getString(R.string.enter_your_password));
            } else if (edtPass.getText().length() < 8) {
                Common.showSnackbar(view, getString(R.string.password_should_be_min));
            } else if (!Common.isValidPassword(edtPass.getText().toString())) {
                Common.showSnackbar(view, getString(R.string.password_should_be_alphanemuric));
            } else {
                callAPISignUp();
            }
        });
    }

    private void callAPISignUp() {
        request = new RequestedServiceDataModel(activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setTag(SIGNUP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);

        request.putQurry(ApiClass.getmApiClass().NAME, edtName.getText().toString());
        request.putQurry(ApiClass.getmApiClass().EMAIL, edtEmail.getText().toString());
        request.putQurry(ApiClass.getmApiClass().PASSWORD, edtPass.getText().toString().trim());
        request.putQurry("is_premium", "1");
        request.putQurry(ApiClass.timeZone, Common.getCurrentTimeZone());
        request.putQurry(ApiClass.getmApiClass().DEVICE_TOKEN, Common.getIntro(activity, "device_token"));
        request.putQurry("device_type", "ANDROID");


        baseRequestData.setApiType(Constant.SIGNUP);
        request.setBaseRequestData(baseRequestData);
        request.execute(this);
    }


    public void customLoginTwitter(View view) {
        //check if user is already authenticated or not
        if (getTwitterSession() == null) {

            //if user is not authenticated start authenticating
            client.authorize(this, new Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> result) {

                    // Do something with result, which provides a TwitterSession for making API calls
                    TwitterSession twitterSession = result.data;

                    //call fetch email only when permission is granted
                    fetchTwitterEmail(twitterSession);
                }

                @Override
                public void failure(TwitterException e) {
                    // Do something on failure
                    Toast.makeText(SignUpActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            //if user is already authenticated direct call fetch twitter email api
//            Toast.makeText(this, "User already authenticated", Toast.LENGTH_SHORT).show();
            fetchTwitterEmail(getTwitterSession());
        }
    }


    public void fetchTwitterEmail(final TwitterSession twitterSession) {
        client.requestEmail(twitterSession, new Callback<String>() {
            @Override
            public void success(Result<String> result) {
                //here it will give u only email and rest of other information u can get from TwitterSession
//                userDetailsLabel.setText("User Id : " + twitterSession.getUserId() + "\nScreen Name : " + twitterSession.getUserName() + "\nEmail Id : " + result.data);
                Log.d("twitter", "User Id : " + twitterSession.getUserId() + "\nScreen Name : " + twitterSession.getUserName() + "\nEmail Id : " + result.data);

//                Toast.makeText(SignUpActivity.this, "User Id : " + twitterSession.getUserId() + "\nScreen Name : " + twitterSession.getUserName() + "\nEmail Id : " + result.data, Toast.LENGTH_SHORT).show();

                socialId=twitterSession.getUserId()+"";
                emaill=result.data+"";
                fullName=twitterSession.getUserName()+"";

                fetchTwitterImage(img_twitter);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(SignUpActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void fetchTwitterImage(View view) {
        //check if user is already authenticated or not
        if (getTwitterSession() != null) {

            //fetch twitter image with other information if user is already authenticated

            //initialize twitter api client
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();

            //Link for Help : https://developer.twitter.com/en/docs/accounts-and-users/manage-account-settings/api-reference/get-account-verify_credentials

            //pass includeEmail : true if you want to fetch Email as well
            Call<User> call = twitterApiClient.getAccountService().verifyCredentials(true, false, true);
            call.enqueue(new Callback<User>() {
                @Override
                public void success(Result<User> result) {
                    User user = result.data;
//                    userDetailsLabel.setText("User Id : " + user.id + "\nUser Name : " + user.name + "\nEmail Id : " + user.email + "\nScreen Name : " + user.screenName);

                    String imageProfileUrl = user.profileImageUrl;
                    Log.e("hi", "Data : " + imageProfileUrl);
                    //NOTE : User profile provided by twitter is very small in size i.e 48*48
                    //Link : https://developer.twitter.com/en/docs/accounts-and-users/user-profile-images-and-banners
                    //so if you want to get bigger size image then do the following:
                    imageProfileUrl = imageProfileUrl.replace("_normal", "");
                    profilePicture = imageProfileUrl.replace("_normal", "");


                    callSocialChcekuserApi();

                    ///load image using Picasso
//                    Picasso.with(MainActivity.this)
//                            .load(imageProfileUrl)
//                            .placeholder(R.mipmap.ic_launcher_round)
//                            .into(userProfileImageView);
                }

                @Override
                public void failure(TwitterException exception) {
                    Toast.makeText(SignUpActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            //if user is not authenticated first ask user to do authentication
            Toast.makeText(this, "First to Twitter auth to Verify Credentials.", Toast.LENGTH_SHORT).show();
        }

    }


    private TwitterSession getTwitterSession() {
        TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();

        //NOTE : if you want to get token and secret too use uncomment the below code
        /*TwitterAuthToken authToken = session.getAuthToken();
        String token = authToken.token;
        String secret = authToken.secret;*/

        return session;
    }

    private void passShowHide() {
        img_pas_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtPass.setTransformationMethod(new PasswordTransformationMethod());
                img_pas_show.setVisibility(View.GONE);
                img_pas_hide.setVisibility(View.VISIBLE);
                setSelectLast();
            }
        });
        img_pas_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtPass.setTransformationMethod(null);
                img_pas_show.setVisibility(View.VISIBLE);
                img_pas_hide.setVisibility(View.GONE);
                setSelectLast();
            }
        });


    }

    private void setSelectLast() {
        try {
            edtPass.setSelection(edtPass.getText().toString().length());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

        Common.showToast(this, message);
    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) throws JSONException {
        if (baseRequestData.getTag() == SIGNUP) {
            JSONObject jsonObject = new JSONObject(jsondata);
            String status = jsonObject.getString("status");
            JSONObject data = jsonObject.getJSONObject("data");

            String token = data.getString("token");
            Common.setPreferences(activity, "data", jsondata);
            Common.setPreferences(activity, "token", token);
            Common.setPreferences(activity, "password", edtPass.getText().toString());
            Common.setPreferences(this, "isLogin", "true");
            moveToHome();
            finishAffinity();

        } else if (baseRequestData.getTag() == ResponseType.SOCIAL_SIGNIN) {
            JSONObject jsonObject = new JSONObject(jsondata);
            String status = jsonObject.getString("status");
            JSONObject data = jsonObject.getJSONObject("data");

            String token = data.getString("token");
            Common.setPreferences(activity, "data", jsondata);
            Common.setPreferences(activity, "token", token);
            Common.setPreferences(this, "isLogin", "true");
            Common.setPreferences(activity, "isSocial", "true");

            moveToHome();

            finishAffinity();
        } else {
            Common.showToast(this, message);

        }
    }

    private void moveToHome() {
        startActivity(new Intent(this, HomeActivity.class).putExtra(Common.key.isFrom,Common.key.signUp));
    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        Common.showToast(this, message);
    }

    private class GetFileFromUrl extends AsyncTask<String, Void, String> {
        private GetFileFromUrl() {
        }

        protected String doInBackground(String... params) {
            Bitmap bbmm = getResizedBitmap(getBitmapFromURL(SignUpActivity.this.profilePicture), 500, 500);
            profileFile = persistImage(bbmm, System.currentTimeMillis() + "", SignUpActivity.this);


//            Looper.prepare();


//            returnValue.add(profileFile.getAbsolutePath());
            return null;
        }

        protected void onPostExecute(String s) {
            SignUpActivity.this.callSocialChcekuserApi();
            super.onPostExecute(s);

        }
    }

    private File profileFile;

    public static File persistImage(Bitmap bitmap, String name, Context context) {
        File filesDir = context.getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

        try {
            OutputStream os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception var7) {
            Log.e("Error", "Error writing bitmap", var7);
        }

        return imageFile;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = (float) newWidth / (float) width;
        float scaleHeight = (float) newHeight / (float) height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException var5) {
            return null;
        }
    }


}