package com.reminder.app.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.reminder.app.R;
import com.reminder.app.adapter.GeneralAdapter;
import com.reminder.app.databinding.ActivityLastMessageBinding;
import com.reminder.app.models.ChatModel;
import com.reminder.app.models.UserResponse;
import com.reminder.app.service.ConnectionService;
import com.reminder.app.utils.BaseView;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;

import java.util.HashMap;


public class LastMessageActivity extends BaseActivity implements View.OnClickListener {
    ActivityLastMessageBinding binding;
    GeneralAdapter<ChatModel> adapter;
    UserResponse userModel;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_last_message);
        userModel = new Gson().fromJson(Common.getPreferences(this, "data"), UserResponse.class);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!Common.getInstance().isMyServiceRunning(this, ConnectionService.class))
            startService(new Intent(this, ConnectionService.class));
        getHistory();
    }

    private void getHistory() {
        HashMap<String, String> request = new HashMap<>();

        new RetroService().call((BaseView) this, MainApp.getInstance().getApiServices().lastChat(request),
                result -> {
                    if (result.isStatus()) {
                        binding.setItem(result.getData());
                    } else {
                        Common.showToast(this, result.getMessage());
                    }
                });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.llAll:
                Intent intent = new Intent(this, MessageActivity.class);
                intent.putExtra(Common.socketParameter.other_user_id, binding.getItem().getGetuser().getId() + "");
                intent.putExtra(Common.socketParameter.other_user_name, binding.getItem().getGetuser().getName());
                intent.putExtra(Common.socketParameter.other_user_image, binding.getItem().getGetuser().getProfile_image());
                startActivity(intent);
                break;
            case R.id.btnFollow:
                startActivity(new Intent(this, AllUserChatListActivity.class).putExtra(Common.key.isFrom, Common.key.inboxList));
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;

        }

    }
}