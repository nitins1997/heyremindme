package com.reminder.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.reminder.app.R;
import com.reminder.app.adapter.GeneralAdapter;
import com.reminder.app.databinding.ActivitySavedPasswordBinding;
import com.reminder.app.models.SavePassword;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;

import java.util.List;
public class SavedPasswordListActivity extends BaseActivity {


    ActivitySavedPasswordBinding binding;
    List<SavePassword> savePasswordsList;
    String isFrom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_saved_password);
        isFrom = getIntent().getStringExtra(Common.key.isFrom);

        binding.imgBack.setOnClickListener(view -> {
            finish();
        });

        /*call api to get password list*/
        callApiPasswordList();
    }

    private void savedPassword() {
        binding.recv.setLayoutManager(new LinearLayoutManager(this));
        binding.recv.setAdapter(new GeneralAdapter(R.layout.row_save_password, savePasswordsList, (view, object) -> {
            SavePassword savePassword = (SavePassword) object;
            switch (view.getId()) {
                case R.id.llAll:
                    Intent intent = new Intent(this, ShowPasswordActivity.class);
                    intent.putExtra(Common.key.accountType, savePassword.getAccount_type());
                    intent.putExtra(Common.key.password, savePassword.getPassword());
                    intent.putExtra(Common.key.username, savePassword.getUsername());
                    intent.putExtra(Common.key.description, savePassword.getDescription());
                    startActivity(intent);
                    break;
            }

        }));
    }

    private void callApiPasswordList() {
        new RetroService().call(this, MainApp.getInstance().getApiServices().savedPasswordList(),
                result -> {
                    savePasswordsList = result.getData();
                    if (savePasswordsList.size() == 0) {
                        binding.txtNoData.setVisibility(View.VISIBLE);
                        binding.recv.setVisibility(View.GONE);

                    } else {
                        if (isFrom.equals(Common.key.audioSearch)) {
                            searchByAudio();
                        } else {
                            setAdapterViews();
                        }

                    }

                });

    }

    private void searchByAudio() {
        setAdapterViews();
        for (int i = 0; i < savePasswordsList.size(); i++) {
            if(savePasswordsList.get(i).getAccount_type().contains(" ")){
                String str = savePasswordsList.get(i).getAccount_type();
                String[] splited = str.split("\\s+");
                for (int j = 0; j < splited.length; j++) {
                    if(getIntent().getStringExtra(Common.key.data).toLowerCase().contains(splited[j].toLowerCase())){
                        if(!splited[j].toLowerCase().equals("account")){
                            Intent intent = new Intent(this, ShowPasswordActivity.class);
                            intent.putExtra(Common.key.accountType, savePasswordsList.get(i).getAccount_type());
                            intent.putExtra(Common.key.password, savePasswordsList.get(i).getPassword());
                            intent.putExtra(Common.key.username, savePasswordsList.get(i).getUsername());
                            intent.putExtra(Common.key.description, savePasswordsList.get(i).getDescription());
                            startActivity(intent);
                            break;
                        }

                    }
                }
            }else {
                if (getIntent().getStringExtra(Common.key.data).toLowerCase().contains(savePasswordsList.get(i).getAccount_type().toLowerCase())) {
                    Intent intent = new Intent(this, ShowPasswordActivity.class);
                    intent.putExtra(Common.key.accountType, savePasswordsList.get(i).getAccount_type());
                    intent.putExtra(Common.key.password, savePasswordsList.get(i).getPassword());
                    intent.putExtra(Common.key.username, savePasswordsList.get(i).getUsername());
                    intent.putExtra(Common.key.description, savePasswordsList.get(i).getDescription());
                    startActivity(intent);
                    break;
                }
            }

        }


    }

    private void setAdapterViews() {
        binding.txtNoData.setVisibility(View.GONE);
        binding.recv.setVisibility(View.VISIBLE);
        savedPassword();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Common.onActivityCode.savePassResult && requestCode == Common.onActivityCode.savePassRequest) {
            /*call api to get password list*/
            callApiPasswordList();
        }
    }


    public void onItemClicked(View view) {
        switch (view.getId()) {
            case R.id.addpassword:
            case R.id.imgAdd:
                Intent intent3 = new Intent(this, AddSavedPassword.class);
                startActivityForResult(intent3, Common.onActivityCode.savePassRequest);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.imgBack:
                finish();
                // getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                // do something here
                break;

        }
    }


}