package com.reminder.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.reminder.app.R;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llTnC:
                startActivity(new Intent(this, WebActivity.class).putExtra("from", "Terms & Conditions"));
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llPP:
                startActivity(new Intent(this, WebActivity.class).putExtra("from", "Privacy Policy"));
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llAbout:
                startActivity(new Intent(this, WebActivity.class).putExtra("from", "About Us"));
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.imgBack:
                onBackPressed();
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
        }

    }
}