package com.reminder.app.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.reminder.app.R;
import com.reminder.app.constant.BackToolbar;
import com.reminder.app.databinding.ActivityForgotBinding;
import com.reminder.app.network.ApiClass;
import com.reminder.app.network.BaseRequestData;
import com.reminder.app.network.RequestedServiceDataModel;
import com.reminder.app.network.ResponseDelegate;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;

import org.json.JSONException;

import java.util.List;




import static com.reminder.app.network.ResponseType.FORGOT;
import static com.reminder.app.network.ResponseType.SIGNIN;

public class ForgotPasswordActivity extends AppCompatActivity implements ResponseDelegate {


    AppCompatActivity activity;
    private RequestedServiceDataModel request;

    ActivityForgotBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_forgot);
        activity = ForgotPasswordActivity.this;

        binding.imgBack.setOnClickListener(view -> {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });

        binding.txtSignUp.setOnClickListener(view -> {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });



        binding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(binding.edtEmail.getText().toString())) {
                    Common.showSnackbar(view, getString(R.string.enter_email_address));
                } else if (!Common.isValidEmail(binding.edtEmail.getText().toString())) {
                    Common.showSnackbar(view, getString(R.string.invalid_email));
                } else if (!Common.isValidEmail(binding.edtEmail.getText().toString())) {
                    Common.showSnackbar(view, getString(R.string.invalid_email));
                } else {
                    callAPIForgot();
                }

//                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            }
        });
    }

    private void callAPIForgot() {
        request = new RequestedServiceDataModel(activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setTag(FORGOT);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        request.putQurry(ApiClass.getmApiClass().EMAIL, binding.edtEmail.getText().toString());
        request.putQurry(ApiClass.getmApiClass().DEVICE_TOKEN, Common.getIntro(activity, "device_token"));
        request.putQurry("device_type", "ANDROID");
        baseRequestData.setApiType(Constant.FORGOTPASSWORD);
        request.setBaseRequestData(baseRequestData);
        request.execute(this);
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {
        Common.showToast(this, message);
    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) throws JSONException {
        Common.showToast(this, message);
        finish();

    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        Common.showToast(this, message);
    }
}