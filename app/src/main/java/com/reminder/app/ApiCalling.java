package com.reminder.app;


import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;
import com.reminder.app.webserviceRetro.ErrorType;
import com.reminder.app.webserviceRetro.response.ApiResponse;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApiCalling extends Activity {
    public static final String TAG = "RetroService***";
    private ErrorListener errorListener;


    public ApiCalling setErrorListener(ErrorListener errorListener) {
        this.errorListener = errorListener;
        return this;
    }


    public void callApi(Activity activity, OkHttpClient httpClient, HashMap<String, String> apiParams, String apiName, SuccessListener<ApiResponse> successListener) {
        FormBody.Builder mBuilder = new FormBody.Builder();
        for (Map.Entry<String, String> me : apiParams.entrySet()) {
            mBuilder.add(me.getKey(), me.getValue());
            Log.d(me.getKey(), me.getValue());
        }


        Request request;
        RequestBody formBody = mBuilder.build();

        String token = Common.getPreferences(activity, "token");

        request = new Request.Builder()
                .url(Constant.BASE_URL + apiName)
                .header("Authorization", "Bearer " + token)
                .post(formBody)
                .build();

//        if (!TextUtils.isEmpty(token)) {
//            mBuilder.addHeader("Authorization", "Bearer " + token);
//        }
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(() -> {
                    ErrorType error = ErrorType.getErrorTypeByVolleyError(505);
                    if (error == null) return;
                    if (errorListener != null) {
                        errorListener.onError(error);
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull final Response response) {
                runOnUiThread(() -> {
                    if (response.isSuccessful()) {
                        ApiResponse apiResponse = null;
                        try {
                            apiResponse = new Gson().fromJson(response.body().string(), ApiResponse.class);
//                            Log.d("rgg", response.body().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        successListener.onSuccess(apiResponse);
                    } else {
                        ErrorType error = ErrorType.getErrorTypeByVolleyError(response.code());
                        if (error == null) return;
                        if (errorListener != null) {
                            errorListener.onError(error);
                        }
                    }
                });
            }
        });
    }

    public interface SuccessListener<T> {
        void onSuccess(T result);
    }


    public interface ErrorListener {
        void onError(ErrorType error);
    }

}
