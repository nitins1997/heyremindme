package com.reminder.app.network;


import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reminder.app.utils.AppLog;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Nitin on 14/12/2020.
 */

public class ServiceGenerator {

    private ServiceGenerator() {
    }

    public static Retrofit getStripClient(String baseUrl) {
        Retrofit rtr;
        //   InternalServerErrorInterceptor interceptor = new InternalServerErrorInterceptor();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build();

        rtr = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return rtr;
    }

    public static <S> Retrofit createService(Context context, Class<S> serviceClass) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(loggingInterceptor);
        httpClient.connectTimeout(100, TimeUnit.MINUTES);
        httpClient.readTimeout(100, TimeUnit.MINUTES);

        String security_token = Common.getPreferences(context,"token");
        AppLog.SHOW_LOG(security_token);

        if (security_token != null && !security_token.equalsIgnoreCase("") && !security_token.equalsIgnoreCase("0")) {

            httpClient.networkInterceptors().add(chain -> {
                Request original = chain.request();
                Request request = original.newBuilder()
                        .header(ApiClass.getmApiClass().AUTHORIZATION, "Bearer " + security_token)
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            });
        }
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        return retrofit;
    }


    public static <S> Retrofit createBlogService(Context context, Class<S> serviceClass) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(loggingInterceptor);
        httpClient.connectTimeout(100, TimeUnit.MINUTES);
        httpClient.readTimeout(100, TimeUnit.MINUTES);

        String security_token = Common.getPreferences(context,"token");
        AppLog.SHOW_LOG(security_token);

        if (security_token != null && !security_token.equalsIgnoreCase("") && !security_token.equalsIgnoreCase("0")) {

            httpClient.networkInterceptors().add(chain -> {
                Request original = chain.request();
                Request request = original.newBuilder()
                        .header(ApiClass.getmApiClass().AUTHORIZATION, "Bearer " + security_token)
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            });
        }
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(" ")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        return retrofit;
    }

}
