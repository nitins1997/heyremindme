package com.reminder.app.network;

/**
 * Created by user47 on 26/12/17.
 */

public interface ResponseType {

    int SIGNUP = 100;
    int SIGNIN = 101;
    int FORGOT = 102;
    int UPDATEPROFILE = 103;
    int UPDATEPROFILEIMAGE = 104;
    int CHANGE = 105;
    int SOCIAL_SIGNIN = 106;
    int ADD_REMINDER = 107;
    int REMINDER_LIST = 108;


}
