package com.reminder.app.network;


import com.google.gson.internal.LinkedTreeMap;
import com.reminder.app.network.cardList.CCPozo;
import com.reminder.app.network.cardsaved.CardSaved;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;



public interface RetroDataRequest {


    @FormUrlEncoded
    @POST("api/{path1}")
    Observable<LinkedTreeMap> dataRequest(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action, @FieldMap(encoded = true) Map<String, String> fieldValue);

    @Multipart
    @POST("api/{path1}")
    Observable<LinkedTreeMap> dataRequestPost(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action, @PartMap Map<String, RequestBody> fieldValue);

    @Multipart
    @POST("api/{path1}")
    Observable<LinkedTreeMap> dataRequestMultiPart(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action, @PartMap Map<String, RequestBody> fieldValue, @Part List<MultipartBody.Part> files);

    @GET("api/{path1}")
    Observable<LinkedTreeMap> dataRequestGet(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action);



    @FormUrlEncoded
    @POST("{path1}")
    Observable<LinkedTreeMap> dataRequest1(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action, @FieldMap(encoded = true) Map<String, String> fieldValue);

    @Multipart
    @POST("{path1}")
    Observable<LinkedTreeMap> dataRequestPost1(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action, @PartMap Map<String, RequestBody> fieldValue);

    @Multipart
    @POST("{path1}")
    Observable<LinkedTreeMap> dataRequestMultiPart1(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action, @PartMap Map<String, RequestBody> fieldValue, @Part List<MultipartBody.Part> files);

    @GET("{path1}")
    Observable<LinkedTreeMap> dataRequestGet1(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action);

    @GET("sources")
    Call<CCPozo> cardSaved(@Header("Authorization") String header, @Query("object") String country_code, @Query("limit") String lang);
    @POST("sources")
    @FormUrlEncoded
    Call<CardSaved> cardAdd(@Header("Authorization") String header, @FieldMap Map<String, String> parmas);
    @DELETE("sources/{card_id}")
    Call<DeleteCard> deleteCard(@Header("Authorization") String header, @Path("card_id") String userid);


}
