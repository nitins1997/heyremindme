package com.reminder.app.network;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Nitin on 14/12/2020.
 */
public class RequestBodyUtils {


    public static  MultipartBody.Part getRequestBodyImage(File file, String key)
    {
       RequestBody requestFile= RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData(key, file.getName(), requestFile);
        return body;
    }
    public static RequestBody getRequestBodyString(String text)
    {
        return RequestBody.create(MediaType.parse("text/plain"), text);
    }

}
