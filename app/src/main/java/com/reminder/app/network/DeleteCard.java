package com.reminder.app.network;

import java.io.Serializable;

/**
 * Created by nine on 13/7/18.
 */

public class DeleteCard implements Serializable {
    private String deleted;
    private String id;

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
