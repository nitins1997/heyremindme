package com.reminder.app.network;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.internal.LinkedTreeMap;
import com.reminder.app.R;
import com.reminder.app.activity.Splash;
import com.reminder.app.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import retrofit2.Retrofit;

/**
 * Created by Nitin on 14/12/2020.
 */

public class ApiResult {

    private final Dialog progressdialog;
    private Context context;
    private RequestedServiceDataModel requestedServiceDataModel;


    public ApiResult(Context context, RequestedServiceDataModel requestedServiceDataModel) {
        this.context = context;
        this.requestedServiceDataModel = requestedServiceDataModel;
        progressdialog = new Dialog(context);
        progressdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressdialog.setContentView(R.layout.view_progress);
        progressdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressdialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

    }

    public void executeBolgRequestWithoutProgressbar() {
      //  showProgressBar();
        Retrofit userService = ServiceGenerator.createBlogService(context, RetroDataRequest.class);


        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                    .flatMap(retroDataRequest -> {

                        Observable<LinkedTreeMap> couponsObservable
                                = retroDataRequest.dataRequestGet1(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction()).subscribeOn(Schedulers.io());

                        return Observable.concatArray(couponsObservable);
                    }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);

        } else if (requestedServiceDataModel.getFile() == null) {
            if (requestedServiceDataModel.getQurry() == null || requestedServiceDataModel.getQurry().size() == 0) {
                Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                        .flatMap(retroDataRequest -> {
                            Observable<LinkedTreeMap> couponsObservable
                                    = retroDataRequest.dataRequest1(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction(), requestedServiceDataModel.getQurry()).subscribeOn(Schedulers.io());


                            return Observable.concatArray(couponsObservable);
                        }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);

            } else {

                Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                        .flatMap(retroDataRequest -> {
                            Observable<LinkedTreeMap> couponsObservable
                                    = retroDataRequest.dataRequestPost1(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction(), getMultiPartDataText()).subscribeOn(Schedulers.io());


                            return Observable.concatArray(couponsObservable);
                        }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);
            }
        } else {
            Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                    .flatMap(retroDataRequest -> {
                        Observable<LinkedTreeMap> couponsObservable
                                = retroDataRequest.dataRequestMultiPart1(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction(), getMultiPartDataText(), getMultiPartDataImage()).subscribeOn(Schedulers.io());

                        return Observable.concatArray(couponsObservable);
                    }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);
        }


    }


    public void executeRequest() {

        showProgressBar();
        Retrofit userService = ServiceGenerator.createService(context, RetroDataRequest.class);

        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                    .flatMap(retroDataRequest -> {

                        Observable<LinkedTreeMap> couponsObservable
                                = retroDataRequest.dataRequestGet(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction()).subscribeOn(Schedulers.io());
                        return Observable.concatArray(couponsObservable);
                    }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);

        } else if (requestedServiceDataModel.getFile() == null) {
            if (requestedServiceDataModel.getQurry() == null || requestedServiceDataModel.getQurry().size() == 0) {
                Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                        .flatMap(retroDataRequest -> {
                            Observable<LinkedTreeMap> couponsObservable
                                    = retroDataRequest.dataRequest(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction(), requestedServiceDataModel.getQurry()).subscribeOn(Schedulers.io());


                            return Observable.concatArray(couponsObservable);
                        }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);

            } else {

                Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                        .flatMap(retroDataRequest -> {
                            Observable<LinkedTreeMap> couponsObservable
                                    = retroDataRequest.dataRequestPost(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction(), getMultiPartDataText()).subscribeOn(Schedulers.io());


                            return Observable.concatArray(couponsObservable);
                        }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);
            }
        } else {
            Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                    .flatMap(retroDataRequest -> {
                        Observable<LinkedTreeMap> couponsObservable
                                = retroDataRequest.dataRequestMultiPart(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction(), getMultiPartDataText(), getMultiPartDataImage()).subscribeOn(Schedulers.io());

                        return Observable.concatArray(couponsObservable);
                    }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);
        }


    }

    public void executeRequestWithoutProgressbar() {

        Retrofit userService = ServiceGenerator.createService(context, RetroDataRequest.class);


        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                    .flatMap(retroDataRequest -> {

                        Observable<LinkedTreeMap> couponsObservable
                                = retroDataRequest.dataRequestGet(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction()).subscribeOn(Schedulers.io());

                        return Observable.concatArray(couponsObservable);
                    }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);

        } else if (requestedServiceDataModel.getFile() == null) {
            if (requestedServiceDataModel.getQurry() == null || requestedServiceDataModel.getQurry().size() == 0) {
                Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                        .flatMap(retroDataRequest -> {
                            Observable<LinkedTreeMap> couponsObservable
                                    = retroDataRequest.dataRequest(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction(), requestedServiceDataModel.getQurry()).subscribeOn(Schedulers.io());


                            return Observable.concatArray(couponsObservable);
                        }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);

            } else {

                Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                        .flatMap(retroDataRequest -> {
                            Observable<LinkedTreeMap> couponsObservable
                                    = retroDataRequest.dataRequestPost(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction(), getMultiPartDataText()).subscribeOn(Schedulers.io());


                            return Observable.concatArray(couponsObservable);
                        }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);
            }
        } else {
            Observable.just(userService.create(RetroDataRequest.class)).retry(3).subscribeOn(Schedulers.computation())
                    .flatMap(retroDataRequest -> {
                        Observable<LinkedTreeMap> couponsObservable
                                = retroDataRequest.dataRequestMultiPart(requestedServiceDataModel.getBaseRequestData().getApiType(), getAction(), getMultiPartDataText(), getMultiPartDataImage()).subscribeOn(Schedulers.io());

                        return Observable.concatArray(couponsObservable);
                    }).observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleResults, this::handleError);
        }


    }

    private void showProgressBar() {
        progressdialog.setCancelable(false);
        progressdialog.show();
    }


    public HashMap<String, String> getAction() {

        HashMap<String, String> qurryMap = new HashMap<>();
        ArrayList<String> strings = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();

        for (Map.Entry<String, String> stringStringEntry : requestedServiceDataModel.getQurry().entrySet()) {
            strings.add(stringStringEntry.getKey());
            if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
                try {
                    jsonObject.put(stringStringEntry.getKey(), stringStringEntry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        JSONArray jsonArray = new JSONArray();
        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            jsonArray.put(jsonObject);
            qurryMap.put("data", jsonArray.toString());
            requestedServiceDataModel.setQurry(new HashMap<>());
        }
        return qurryMap;
    }

    private void handleError(Throwable throwable) {
        progressEnd();
        Log.e("error", "error");
        //  Common.showToast(context,"Oops! Something went wrong. Try again");
        if (throwable instanceof HttpException) {
            throwable.printStackTrace();
        } else {
            Log.e("error", "error");
        }
        throwable.printStackTrace();
    }

    private void handleResults(LinkedTreeMap string) {
        progressEnd();
        try {
            JSONObject jsonObject = new JSONObject(string);
            Log.e("JsonObject", jsonObject.toString());
            if (jsonObject.getString("status").equalsIgnoreCase("true") || jsonObject.getBoolean("status")==true) {
                if (jsonObject.has("message")) {
                    requestedServiceDataModel.getResponseDelegate().onSuccess(jsonObject.toString(), jsonObject.getString("message"), requestedServiceDataModel.getBaseRequestData());
                } else if (jsonObject.has("msg")) {
                    requestedServiceDataModel.getResponseDelegate().onSuccess(jsonObject.toString(), jsonObject.getString("msg"), requestedServiceDataModel.getBaseRequestData());

                } else {
                    requestedServiceDataModel.getResponseDelegate().onSuccess(jsonObject.toString(), "No msg", requestedServiceDataModel.getBaseRequestData());

                }
            } else if (jsonObject.getString("status").equalsIgnoreCase("false") || jsonObject.getBoolean("status")==false) {

                if (jsonObject.has("error") && (jsonObject.getString("error").equalsIgnoreCase("userInactive") || (jsonObject.getString("error").equalsIgnoreCase("userDeleted")))) {
                    //  Common.showToast(context, jsonObject.getString("message"));
                    context.getSharedPreferences("prefs_login", Activity.MODE_PRIVATE).edit().clear().commit();
                    Intent intent = new Intent(context, Splash.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                } else {
                    if (jsonObject.has("message") || jsonObject.has("msg")) {
                        requestedServiceDataModel.getResponseDelegate().onFailure(jsonObject.toString(), jsonObject.getString("message"), requestedServiceDataModel.getBaseRequestData());
                    } else if (jsonObject.has("msg")) {
                        requestedServiceDataModel.getResponseDelegate().onFailure(jsonObject.toString(), jsonObject.getString("msg"), requestedServiceDataModel.getBaseRequestData());

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void progressEnd() {

        if (progressdialog.isShowing()) {
            try {
                progressdialog.dismiss();
            } catch (Exception e) {

            }

        }
    }

    public HashMap<String, RequestBody> getMultiPartDataText() {
        HashMap<String, RequestBody> bodyHashMap = new HashMap<>();
        for (Map.Entry<String, String> stringStringEntry : requestedServiceDataModel.getQurry().entrySet()) {
            bodyHashMap.put(stringStringEntry.getKey(), RequestBodyUtils.getRequestBodyString(stringStringEntry.getValue()));
        }
        return bodyHashMap;
    }

    public List<MultipartBody.Part> getMultiPartDataImage() {
        List<MultipartBody.Part> bodyHashMap = new ArrayList<>();
        for (Map.Entry<String, File> stringStringEntry : requestedServiceDataModel.getFile().entrySet()) {
            bodyHashMap.add(RequestBodyUtils.getRequestBodyImage(stringStringEntry.getValue(), stringStringEntry.getKey()));
        }
        return bodyHashMap;
    }

}
