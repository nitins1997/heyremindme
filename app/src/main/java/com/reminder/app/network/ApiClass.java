package com.reminder.app.network;

/**
 * Created by Nitin 14/12/2020.
 */

public class ApiClass {
    public static ApiClass mApiClass;
    public static final String AUTHORIZATION = "Authorization";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String DEVICE_TYPE = "device_type";
    public static final String NAME = "name";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String COUNTRY_CODE = "country_code";
    public static final String PHONE = "phone";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String PASSWORD_CONFIRMATION = "password_confirmation";
    public static final String SOCIAL_TYPE = "social_type";
    public static final String SOCIAL_ID = "social_id";
    public static final String REMINDER_DATE = "reminder_date";
    public static final String REMINDERDATA = "reminderdata";
    public static final String Username = "username";
    public static final String AccountType = "account_type";
    public static final String Password = "password";
    public static final String STATUS = "status";
    public static final String REMINDER_TIME = "reminder_time";
    public static final String DESCRIPTION = "description";
    public static final String OTHER_USER_ID = "other_user_id";
    public static final String OTHER_USER_NAME = "other_user_name";
    public static final String ADDRESS = "address";
    public static final String TITLE = "title";
    public static final String REMINDER = "reminder";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String PAGE = "page";
    public static final String timeZone = "timezone";
    public static final String reminder_id = "reminder_id";
    public static final String other_user_id = "other_user_id";
    public static final String transaction_id = "transaction_id";
    public static final String amount = "amount";
    public static final String product_id = "product_id";
    public static final String platform = "platform";
    public static final String receipt = "receipt";
    public static final String payment_time = "payment_time";
    public static final String device_token = "device_token";
    public static final String SEARCH = "search";
    public static final String DATE = "date";
    public static final String REMINDER_ID = "reminder_id";
    public static final String KEYWORD = "keyword";


    public static ApiClass getmApiClass() {
        if (mApiClass == null) {
            mApiClass = new ApiClass();
        }
        return mApiClass;
    }
}
