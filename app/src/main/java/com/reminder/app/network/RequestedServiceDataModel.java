package com.reminder.app.network;


import android.app.Activity;
import android.content.Context;

import com.reminder.app.R;
import com.reminder.app.utils.Common;


public class RequestedServiceDataModel extends DataModel {

    private ApiResult dostoomServerRequest;

    public RequestedServiceDataModel(Context context, ResponseDelegate delegate) {
        super(context, delegate);
    }

    public void execute(Activity activity) {

        if (Common.getConnectivityStatus(activity)) {
            dostoomServerRequest = new ApiResult(activity, this);
            try {
                dostoomServerRequest.executeRequest();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            if (isShowNetworkTost()) {
                Common.showToast(context, context.getString(R.string.internet_connection_msg));
            } else {
                if (responseDelegate != null) {
                    responseDelegate.onNoNetwork(context.getString(R.string.internet_connection_msg), getBaseRequestData());
                    Common.showToast(context, context.getString(R.string.internet_connection_msg));

                }
            }

        }

    }


    public void executeWithoutProgressbar(Context activity) {

        if (Common.getConnectivityStatus(activity)) {
            dostoomServerRequest = new ApiResult(activity, this);
            try {
                dostoomServerRequest.executeRequestWithoutProgressbar();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            if (isShowNetworkTost()) {
                Common.showToast(activity, context.getString(R.string.internet_connection_msg));
            } else {
                if (responseDelegate != null) {
                    responseDelegate.onNoNetwork(activity.getString(R.string.internet_connection_msg), getBaseRequestData());
                }
            }

        }
    }






}
