package com.reminder.app.network;

import org.json.JSONException;

/**
 * Created by Nitin on 14/12/2020.
 */
public interface ResponseDelegate {

    void onNoNetwork(String message, BaseRequestData baseRequestData);

    void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) throws JSONException;

    void onFailure(String jsondata, String message, BaseRequestData baseRequestData);

}
