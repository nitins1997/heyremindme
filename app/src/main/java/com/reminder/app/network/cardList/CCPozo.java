
package com.reminder.app.network.cardList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CCPozo {

    @SerializedName("object")
    @Expose
    private String object;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("has_more")
    @Expose
    private Boolean hasMore;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getHasMore() {
        return hasMore;
    }

    public void setHasMore(Boolean hasMore) {
        this.hasMore = hasMore;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

}
