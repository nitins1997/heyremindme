package com.reminder.app.models;

import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("k")
	private String K;

	public void setK(String K){
		this.K = K;
	}

	public String getK(){
		return K;
	}
}