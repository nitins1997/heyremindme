package com.reminder.app.models;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.reminder.app.enums.MessageType;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;
import com.reminder.app.utils.MainApp;


public class ChatModel extends BaseModel {

    @SerializedName("messages")
    private String messages;

    @SerializedName("date")
    private String date;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("message_type")
    private String messageType = MessageType.TEXT.getType();

    @SerializedName("profile_image")
    private String profile_image;

    @SerializedName("user_id")
    private int userid;

    @SerializedName("is_blocked_by_me")
    private int isBlockedByMe;

    @SerializedName("is_read")
    private int isRead;

    @SerializedName("is_blocked")
    private int isBlocked;

    @SerializedName("other_user_id")
    private int otherUserId;
    @SerializedName("other_profile_image")
    private String other_profile_image;

    @SerializedName("conversation_id")
    private int conversationId;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    @SerializedName("time")
    private String time;

    @SerializedName("unread_message")
    private int unreadMessage;

    @SerializedName("other_phone_number")
    private String other_phone_number;

    @SerializedName("phone_number")
    private String phone_number;


    @SerializedName("other_name")
    private String other_name;

    public String getOther_name() {
        return other_name;
    }

    public void setOther_name(String other_name) {
        this.other_name = other_name;
    }

    public String getOther_phone_number() {
        return other_phone_number;
    }

    public void setOther_phone_number(String other_phone_number) {
        this.other_phone_number = other_phone_number;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getMessages() {
        return messages;
    }


    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getProfile_image() {
        UserResponse userModel = new Gson().fromJson(Common.getPreferences(MainApp.getInstance(), "data"), UserResponse.class);

        int id = userModel.getData().getId();
//        int id = Integer.parseInt(PrefSetup.getInstance().getUserDetail().getId());
        return otherUserId == id ? other_profile_image : profile_image;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getUserid() {
        return userid;
    }

    public void setIsBlockedByMe(int isBlockedByMe) {
        this.isBlockedByMe = isBlockedByMe;
    }

    public int getIsBlockedByMe() {
        return isBlockedByMe;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsBlocked(int isBlocked) {
        this.isBlocked = isBlocked;
    }

    public int getIsBlocked() {
        return isBlocked;
    }

    public void setOtherUserId(int otherUserId) {
        this.otherUserId = otherUserId;
    }

    public int getOtherUserId() {
        return otherUserId;
    }

    public void setConversationId(int conversationId) {
        this.conversationId = conversationId;
    }

    public int getConversationId() {
        return conversationId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getFinalName() {
        UserResponse userModel = new Gson().fromJson(Common.getPreferences(MainApp.getInstance(), "data"), UserResponse.class);

        int id = userModel.getData().getId();
//        int id = Integer.parseInt(PrefSetup.getInstance().getUserDetail().getId());
        return otherUserId == id ? other_name : name;
    }

    public int getFinalId(){
        UserResponse userModel = new Gson().fromJson(Common.getPreferences(MainApp.getInstance(), "data"), UserResponse.class);

        int id = userModel.getData().getId();
        return otherUserId == id ? userid : otherUserId;
    }
    public String getFinalPhone(){

        UserResponse userModel = new Gson().fromJson(Common.getPreferences(MainApp.getInstance(), "data"), UserResponse.class);

        int id = userModel.getData().getId();

//        int id = Integer.parseInt(PrefSetup.getInstance().getUserDetail().getId());
        return otherUserId == id ? other_phone_number : phone_number;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setUnreadMessage(int unreadMessage) {
        this.unreadMessage = unreadMessage;
    }

    public int getUnreadMessage() {
        return unreadMessage;
    }

    public String getRelativeTime() {
        return Common.getInstance().getRelativeTime(createdAt, Constant.MESSAGE_DATE_TIME_FORMAT_INPUT_DEMO);
    }
//    public String getRelativeTime() {
//        String dateTime = getDate() + " " + getTime();
//        String date = UtilDate.getInstance().changeFormat(dateTime,
//                Constant.CHAT_DATE_TIME_FORMAT_INPUT,
//                Constant.CHAT_DATE_TIME_FORMAT_INPUT,
//                TimeZone.getTimeZone("UTC"),
//                TimeZone.getDefault());
//        return UtilDate.getInstance().getRelativeTime(date, Constant.CHAT_DATE_TIME_FORMAT_INPUT);
//    }

    public boolean isMessageVisible() {
        return messageType == null || messageType.equals(MessageType.TEXT.getType()) || messageType.equals(MessageType.SCHEDULE.getType());
    }

    public boolean isImageMessageVisible() {
        return messageType.equals(MessageType.IMAGE.getType());
    }

    public boolean isLocationMessageVisible() {
        return messageType.equals(MessageType.LOCATION.getType());
    }
}