package com.reminder.app.models;

import java.io.Serializable;

public class UserResponse implements Serializable {
//    {"status":true,
//    "message":"Successfully Registered.",
//    "data":{"id":92,"name":"Nitin Sharma","email":"nitin@gmail.com","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9kZW1vMnNlcnZlci5jb21cL2hleXJlbWluZG1lXC9hcGlcL3JlZ2lzdGVyLXVzZXIiLCJpYXQiOjE2MDc5NTg5NDYsIm5iZiI6MTYwNzk1ODk0NiwianRpIjoiQkhEVndmOUl6VzloRk9ydyIsInN1YiI6OTIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.jvChGj7nD4yduRjhJESGYD0TA6D_wqeFCNnxEkpDA70","profile_image":"","country_code":"","phone":"","forgot_token":"276b091c4772aaa401850590f56bfb6e1607958945","status":"1","device_type":"ANDROID","device_token":"dvI1jRKtRkq_4HEPSOZ0uc:APA91bEaf6_qkrhxZ1b9JVhjRYjoFibsneZbYsXagzV2DvnXzF6H9buUgl_8_xKZqQGqytpesVPOtYJqJGzC5l3pnM5HJQnuVWId-qgYSWqgI58j-qtWeFRcEHr9RoVVnDCreuqxdYyi"}}

    boolean status;
    String message;
    Data data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        int id;
        int status;
        String name;
        String email;
        String token;
        String profile_image;
        String country_code;
        String phone;
        String forgot_token;
        String device_type;
        String device_token;

        public String getIs_premium() {
            return is_premium;
        }

        public void setIs_premium(String is_premium) {
            this.is_premium = is_premium;
        }

        String is_premium;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            if (email != null) {
                if (email.equals("null")) {
                    return "";
                } else {
                    return email;
                }

            } else {
                return "";
            }

        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getForgot_token() {
            return forgot_token;
        }

        public void setForgot_token(String forgot_token) {
            this.forgot_token = forgot_token;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

    }
}
