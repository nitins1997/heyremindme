package com.reminder.app.models;

import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;

public class ChatLastMessageModel {
    String id;
    String user_id;
    String other_user_id;
    String message;
    String type;
    String is_delivered;
    String is_seen;
    String single_group;
    String created_at;
    String updated_at;
    GetUser getuser;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOther_user_id() {
        return other_user_id;
    }

    public void setOther_user_id(String other_user_id) {
        this.other_user_id = other_user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIs_delivered() {
        return is_delivered;
    }

    public void setIs_delivered(String is_delivered) {
        this.is_delivered = is_delivered;
    }

    public String getIs_seen() {
        return is_seen;
    }

    public void setIs_seen(String is_seen) {
        this.is_seen = is_seen;
    }

    public String getSingle_group() {
        return single_group;
    }

    public void setSingle_group(String single_group) {
        this.single_group = single_group;
    }

    public String getCreated_at() {
        return  Common.getInstance().getRelativeTime(created_at, Constant.MESSAGE_DATE_TIME_FORMAT_INPUT_DEMO);
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public GetUser getGetuser() {
        return getuser;
    }

    public void setGetuser(GetUser getuser) {
        this.getuser = getuser;
    }

    public static class GetUser {
        String id;

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        String profile_image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail_verified_at() {
            return email_verified_at;
        }

        public void setEmail_verified_at(String email_verified_at) {
            this.email_verified_at = email_verified_at;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getOther_gender() {
            return other_gender;
        }

        public void setOther_gender(String other_gender) {
            this.other_gender = other_gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getPlan_id() {
            return plan_id;
        }

        public void setPlan_id(String plan_id) {
            this.plan_id = plan_id;
        }

        public String getIs_premium() {
            return is_premium;
        }

        public void setIs_premium(String is_premium) {
            this.is_premium = is_premium;
        }

        public String getOnline() {
            return online;
        }

        public void setOnline(String online) {
            this.online = online;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getForgot_token() {
            return forgot_token;
        }

        public void setForgot_token(String forgot_token) {
            this.forgot_token = forgot_token;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        String name;
        String email_verified_at;
        String country_code;
        String phone;
        String gender;
        String other_gender;
        String dob;
        String role_id;
        String updated_at;
        String created_at;
        String plan_id;
        String is_premium;
        String online;
        String status;
        String forgot_token;
        String token;
    }
}
