package com.reminder.app.models;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FollowList extends BaseModel implements Serializable {
    String current_page;
    List<DataBean> data=new ArrayList<>();

    public String getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(String current_page) {
        this.current_page = current_page;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public class DataBean extends BaseModel implements Serializable{
        int id;
        int user_id;
        int status;
        int other_user_id;
        String created_at;
        String updated_at;
        boolean selection = false;
        GetFollowDetail getfollower;

        @Bindable
        public boolean isSelection() {
            return selection;
        }

        public void setSelection(boolean selection) {
            this.selection = selection;
            notifyPropertyChanged(BR.selection);

        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        @Bindable
        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
            notifyPropertyChanged(BR.status);
        }

        public int getOther_user_id() {
            return other_user_id;
        }

        public void setOther_user_id(int other_user_id) {
            this.other_user_id = other_user_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public GetFollowDetail getGetfollower() {
            return getfollower;
        }

        public void setGetfollower(GetFollowDetail getfollower) {
            this.getfollower = getfollower;
        }

        public class GetFollowDetail extends BaseModel implements Serializable{
            int id;
            String name;
            String email;
            String email_verified_at;
            String country_code;
            String phone;
            String gender;
            String other_gender;
            String dob;
            String role_id;
            String relation_status;
            String profile_image;
            String location;
            String token;
            String forgot_token;
            String status;
            String online;
            String created_at;
            String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getEmail_verified_at() {
                return email_verified_at;
            }

            public void setEmail_verified_at(String email_verified_at) {
                this.email_verified_at = email_verified_at;
            }

            public String getCountry_code() {
                return country_code;
            }

            public void setCountry_code(String country_code) {
                this.country_code = country_code;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getOther_gender() {
                return other_gender;
            }

            public void setOther_gender(String other_gender) {
                this.other_gender = other_gender;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getRole_id() {
                return role_id;
            }

            public void setRole_id(String role_id) {
                this.role_id = role_id;
            }

            public String getRelation_status() {
                return relation_status;
            }

            public void setRelation_status(String relation_status) {
                this.relation_status = relation_status;
            }

            public String getProfile_image() {
                return profile_image;
            }

            public void setProfile_image(String profile_image) {
                this.profile_image = profile_image;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }

            public String getForgot_token() {
                return forgot_token;
            }

            public void setForgot_token(String forgot_token) {
                this.forgot_token = forgot_token;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getOnline() {
                return online;
            }

            public void setOnline(String online) {
                this.online = online;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

    }
}
