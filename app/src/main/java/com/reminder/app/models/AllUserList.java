package com.reminder.app.models;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.reminder.app.BR;

import java.util.List;

public class AllUserList extends BaseModel {
    String current_page;
    List<DataBean> data;

    public String getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(String current_page) {
        this.current_page = current_page;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public class DataBean extends BaseModel {
        int id;
        String name;
        String email;
        String phone;
        String gender;
        String role_id;
        String is_smoker;
        String relation_status;
        String token;
        String status;
        String online;
        String created_at;
        String updated_at;

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        String profile_image;
        int follow_status;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getIs_smoker() {
            return is_smoker;
        }

        public void setIs_smoker(String is_smoker) {
            this.is_smoker = is_smoker;
        }

        public String getRelation_status() {
            return relation_status;
        }

        public void setRelation_status(String relation_status) {
            this.relation_status = relation_status;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOnline() {
            return online;
        }

        public void setOnline(String online) {
            this.online = online;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        @Bindable
        public int getFollow_status() {
            return follow_status;
        }

        public void setFollow_status(int follow_status) {
            this.follow_status = follow_status;
            notifyPropertyChanged(BR.follow_status);
        }
    }
}
