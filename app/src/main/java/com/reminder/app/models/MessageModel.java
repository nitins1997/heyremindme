package com.reminder.app.models;

import com.google.gson.annotations.SerializedName;
import com.reminder.app.R;
import com.reminder.app.enums.MessageType;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.Constant;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.ApiServices;

import java.util.TimeZone;

public class MessageModel extends BaseModel{

    @SerializedName("message")
    private String message;

    @SerializedName("user_status")
    private int userStatus;

    @SerializedName("thumb_image")
    private String thumbImage;

    @SerializedName("other_data")
    private String otherData;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("type")
    private String type;

    @SerializedName("other_user_profile")
    private String otherUserProfile;

    @SerializedName("user_profile")
    private String userProfile;

    @SerializedName("is_read")
    private int isRead;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("user_id")
    private int userId;

    @SerializedName("other_user_id")
    private String otherUserId;

    @SerializedName("other_user_name")
    private String otherUserName;

    @SerializedName("conversation_id")
    private int conversationId;

    @SerializedName("name")
    private String name;

    @SerializedName("deleted_user_id")
    private int deletedUserId;


    @SerializedName("id")
    private int id;

    @SerializedName("location")
    private String location;


    //	local
    private boolean sent;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        if (type.equals(MessageType.IMAGE.getType())) {
            return ApiServices.BASE_IMAGE_URL + message;
        } else {
            return message;
        }
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setThumbImage(String thumbImage) {
        this.thumbImage = thumbImage;
    }

    public String getThumbImage() {
        return thumbImage;
    }

    public void setOtherData(String otherData) {
        this.otherData = otherData;
    }

    public String getOtherData() {
        return otherData;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setOtherUserProfile(String otherUserProfile) {
        this.otherUserProfile = otherUserProfile;
    }

    public String getOtherUserProfile() {
        return ApiServices.BASE_IMAGE_URL + otherUserProfile;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public String getUserProfile() {
        return ApiServices.BASE_IMAGE_URL + userProfile;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setOtherUserId(String otherUserId) {
        this.otherUserId = otherUserId;
    }

    public String getOtherUserId() {
        return otherUserId;
    }

    public void setOtherUserName(String otherUserName) {
        this.otherUserName = otherUserName;
    }

    public String getOtherUserName() {
        return otherUserName;
    }

    public void setConversationId(int conversationId) {
        this.conversationId = conversationId;
    }

    public int getConversationId() {
        return conversationId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDeletedUserId(int deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    public int getDeletedUserId() {
        return deletedUserId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public String getRelativeTime() {
//        String date = UtilDate.getInstance().changeFormat(getCreatedAt(),
//                Constant.MESSAGE_DATE_TIME_FORMAT_INPUT_DEMO,
//                Constant.MESSAGE_DATE_TIME_FORMAT_INPUT_DEMO,
//                TimeZone.getTimeZone("UTC"),
//                TimeZone.getDefault());
        return Common.getInstance().getRelativeTime(getCreatedAt(), Constant.MESSAGE_DATE_TIME_FORMAT_INPUT_DEMO);
    }

    public String getTimeText() {
        String text = null;
        if (type.equals(MessageType.SCHEDULE.getType())) {
            text = Common.getInstance().changeFormat(getCreatedAt(),
                    Constant.MESSAGE_DATE_TIME_FORMAT_INPUT,
                    Common.DateFormatsApp.DEFAULT_DATE_TIME,
                    TimeZone.getTimeZone("UTC"),
                    TimeZone.getDefault());
            text = MainApp.getAppRes().getString(R.string.scheduled) + " " + text;
        } else if (type.equals(MessageType.LOCATION.getType())) {
            text = MainApp.getAppRes().getString(R.string.scheduled_for) + location;
        }
        return text;
    }

    public boolean imageVisible() {
        return type.equals(MessageType.IMAGE.getType());
    }

    public boolean msgVisible() {
        return !type.equals(MessageType.IMAGE.getType());
    }

    public boolean isScheduleMessage() {
        return type.equals(MessageType.SCHEDULE.getType()) || type.equals(MessageType.LOCATION.getType());
    }
}
