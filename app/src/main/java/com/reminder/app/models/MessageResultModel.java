package com.reminder.app.models;

import com.google.gson.annotations.SerializedName;

public class MessageResultModel<T> {

    @SerializedName("data")
    private T data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private String status;

    @SerializedName("code")
    private int code;

    @SerializedName("total_record")
    private int totalRecord;

    @SerializedName("total_pages")
    private int total_pages;

    @SerializedName("total_unread_message")
    private int totalUnreadMessage;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public int getTotalUnreadMessage() {
        return totalUnreadMessage;
    }

    public void setTotalUnreadMessage(int totalUnreadMessage) {
        this.totalUnreadMessage = totalUnreadMessage;
    }
}