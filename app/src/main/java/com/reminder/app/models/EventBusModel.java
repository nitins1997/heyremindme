package com.reminder.app.models;

import java.util.List;

public class EventBusModel {
    private String action;
    private String response;
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}