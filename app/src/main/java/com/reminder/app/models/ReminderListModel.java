package com.reminder.app.models;

import androidx.databinding.Bindable;

import com.reminder.app.BR;
import com.reminder.app.utils.Common;

import java.io.Serializable;
import java.util.ArrayList;

public class ReminderListModel extends BaseModel {
    int current_page;

    ArrayList<DataBean> data;
    String first_page_url;
    String from;
    int last_page;
    String last_page_url;
    String next_page_url;
    String path;
    int per_page;
    String prev_page_url;
    double to;
    int total;

    public ArrayList<DataBean> getData() {
        return data;
    }

    public void setData(ArrayList<DataBean> data) {
        this.data = data;
    }

    public String getFirst_page_url() {
        return first_page_url;
    }

    public void setFirst_page_url(String first_page_url) {
        this.first_page_url = first_page_url;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public double getLast_page() {
        return last_page;
    }

    public void setLast_page(int last_page) {
        this.last_page = last_page;
    }

    public String getLast_page_url() {
        return last_page_url;
    }

    public void setLast_page_url(String last_page_url) {
        this.last_page_url = last_page_url;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public double getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public void setPrev_page_url(String prev_page_url) {
        this.prev_page_url = prev_page_url;
    }

    public double getTo() {
        return to;
    }

    public void setTo(double to) {
        this.to = to;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public double getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public class DataBean extends BaseModel implements Serializable {
        int id;
        int user_id;
        String title;

        public String getReminderdata() {
            return reminderdata;
        }

        public void setReminderdata(String reminderdata) {
            this.reminderdata = reminderdata;
        }

        String reminderdata;
        String reminder_date;
        String reminder_time;
        String description;
        String other_user_id;
        String self_id;
        String address;
        String lat;
        String lng;
        int status;
        String created_at;
        String updated_at;
        String createdBy;
        ArrayList<OtherUserData> others = new ArrayList<>();

        public String getCreatedBy() {
            return "Created By: " + createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public ArrayList<OtherUserData> getOthers() {
            return others;
        }

        public void setOthers(ArrayList<OtherUserData> others) {
            this.others = others;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getReminder_date() {
            return Common.formatCommentDate2(reminder_date);
        }

        public void setReminder_date(String reminder_date) {
            this.reminder_date = reminder_date;
        }

        public String getReminder_time() {
            return Common.formatDateList(reminder_time);
        }

        public void setReminder_time(String reminder_time) {
            this.reminder_time = reminder_time;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getOther_user_id() {
            return other_user_id;
        }

        public void setOther_user_id(String other_user_id) {
            this.other_user_id = other_user_id;
        }

        public String getSelf_id() {
            return self_id;
        }

        public void setSelf_id(String self_id) {
            this.self_id = self_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        @Bindable
        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
            notifyPropertyChanged(BR.status);

        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }

}
