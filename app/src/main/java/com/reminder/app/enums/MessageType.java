package com.reminder.app.enums;

public enum MessageType {
    IMAGE("IMAGE"),
    TEXT("TEXT"),
    LOCATION("LOCATION"),
    SCHEDULE("SCHEDULE"),
    ;

    private String type;

    public String getType() {
        return type;
    }

    MessageType(String type) {

        this.type = type;
    }
}