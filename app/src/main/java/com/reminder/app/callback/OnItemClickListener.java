package com.reminder.app.callback;

import android.view.View;


public interface OnItemClickListener<Object> {
    void onItemClick(View view, Object object);
}