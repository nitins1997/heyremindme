package com.reminder.app.callback;

import android.view.View;


public interface OnCheckChangeListener<Object> {
    void onCheckChange(View view, Object object, boolean checked);
}