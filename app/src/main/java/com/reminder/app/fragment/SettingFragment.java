package com.reminder.app.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.reminder.app.R;
import com.reminder.app.activity.EditProfileActivity;
import com.reminder.app.activity.FollowListActivity;
import com.reminder.app.activity.InboxActivity;
import com.reminder.app.activity.LoginActivity;
import com.reminder.app.activity.ProfileActivity;
import com.reminder.app.activity.ResetPasswordActivity;
import com.reminder.app.activity.SavedPasswordListActivity;
import com.reminder.app.activity.SettingActivity;
import com.reminder.app.activity.ShowActivity;
import com.reminder.app.activity.SubsciptionActivity;
import com.reminder.app.activity.SubscriptionPlansActivity;
import com.reminder.app.activity.WebActivity;
import com.reminder.app.databinding.FragmentSettingBinding;
import com.reminder.app.models.UserResponse;
import com.reminder.app.network.ApiClass;
import com.reminder.app.utils.BaseView;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Objects;

public class SettingFragment extends Fragment implements View.OnClickListener {

    UserResponse userResponse;
    private FragmentSettingBinding binding;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);
        binding.setPresenter(this);
        if(Common.getPreferences(Objects.requireNonNull(getActivity()),"isSocial").equals("true")){
            binding.llChangePass.setVisibility(View.GONE);
        }

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        userResponse = new Gson().fromJson(Common.getPreferences(Objects.requireNonNull(getActivity()), "data"), UserResponse.class);
        binding.setUserresponse(userResponse);
        Common.INIT_CONTACT_PICASSO(getActivity(), userResponse.getData().getProfile_image(), binding.cvProfile);

        super.onResume();
    }



   /* @OnClick({R.id.iv_userprofile})
    public void onItemClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_userprofile:
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                // do something here
                break;
        }
    }*/

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.llInbox:
                startActivity(new Intent(getActivity(), InboxActivity.class).putExtra(Common.key.isFrom, Common.key.inboxList));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llMyProfile:
                startActivity(new Intent(getActivity(), ProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llSubscription:
                startActivity(new Intent(getActivity(), SubscriptionPlansActivity.class));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llSetting:
                startActivity(new Intent(getActivity(), SettingActivity.class));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llLogout:
                showCustomDialog();
                break;
            case R.id.txtEdit:
                startActivity(new Intent(getActivity(), EditProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llChangePass:
                startActivity(new Intent(getActivity(), ResetPasswordActivity.class));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llRate:
                Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(myAppLinkToMarket);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), " unable to find market app", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.llSavedPass:
                startActivity(new Intent(getActivity(), SavedPasswordListActivity.class).putExtra(Common.key.isFrom,Common.key.setting));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llFollow:
                startActivity(new Intent(getActivity(), FollowListActivity.class));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llTnC:
                startActivity(new Intent(getActivity(), WebActivity.class).putExtra("from", "Terms & Conditions"));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llPP:
                startActivity(new Intent(getActivity(), WebActivity.class).putExtra("from", "Privacy Policy"));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            case R.id.llAbout:
                startActivity(new Intent(getActivity(), WebActivity.class).putExtra("from", "About Us"));
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
        }
    }

    private void showCustomDialog() {

        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        //ViewGroup viewGroup = Objects.requireNonNull(getView()).findViewById();
        ViewGroup viewGroup = getView().findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialog = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_logout, viewGroup, false);

        //Now we need an AlertDialog.Builder object
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity(), R.style.CustomAlertDialogPassword);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialog);

        Button btnYes = dialog.findViewById(R.id.btnYes);
        Button btnNo = dialog.findViewById(R.id.btnCancel);

        //finally creating the alert dialog and displaying it
        final android.app.AlertDialog Dialog = builder.create();

        Dialog.show();
        //To prevent dialog box from getting dismissed on back key pressed use this
        Dialog.setCancelable(false);
        //And to prevent dialog box from getting dismissed on outside touch use this
        Dialog.setCanceledOnTouchOutside(false);

        btnYes.setOnClickListener(v -> {
            HashMap<String, String> map = new HashMap<>();
            map.put(ApiClass.getmApiClass().DEVICE_TOKEN, Common.getIntro(getActivity(), "device_token"));
            map.put("token", Common.getIntro(getActivity(), "device_token"));

            new RetroService().call((BaseView) getActivity(), MainApp.getInstance().getApiServices().logout(map), result -> {
                if (result.isStatus()) {
                    Common.clearPreferences(getActivity());
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                } else {
                    Common.showToast(getActivity(), result.getMessage());
                }
            });
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog.dismiss();

            }
        });
    }
}
