package com.reminder.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.reminder.app.R;
import com.reminder.app.activity.AddReminder;
import com.reminder.app.activity.AddReminderDetailActivity;
import com.reminder.app.activity.AddSavedPassword;
import com.reminder.app.activity.EditReminderActivity;
import com.reminder.app.activity.LastMessageActivity;
import com.reminder.app.activity.ProfileActivity;
import com.reminder.app.activity.SavedPasswordListActivity;
import com.reminder.app.activity.YoutubePlayList;
import com.reminder.app.adapter.GeneralAdapter;
import com.reminder.app.callback.OnItemClickListener;
import com.reminder.app.databinding.FragmentReminderBinding;
import com.reminder.app.models.ReminderListModel;
import com.reminder.app.models.UserResponse;
import com.reminder.app.network.ApiClass;
import com.reminder.app.utils.BaseView;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FragmentReminders extends Fragment implements OnItemClickListener {
    ReminderListModel reminderListModel;
    int page = 1;
    boolean loadAgain = false;
    GeneralAdapter<ReminderListModel.DataBean> adapter;
    GeneralAdapter<ReminderListModel.DataBean> searchAdapter;
    FragmentReminderBinding binding;
    UserResponse userResponse;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reminder, container, false);
        callAPIListReminder();
        binding.setOnItemClickListener(this);

        binding.txtCurretDate.setText(Common.formatCommentDate(Common.getCurrentDate()));

        initScrollListener();

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        userResponse = new Gson().fromJson(Common.getPreferences(getActivity(), "data"), UserResponse.class);
        Common.INIT_CONTACT_PICASSO(getActivity(), userResponse.getData().getProfile_image(), binding.ivUserprofile);
        super.onResume();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Common.onActivityCode.requestCodeSpeech) {/*google speech text*/
            if (resultCode == Activity.RESULT_OK && null != data) {
                String speechText = Objects.requireNonNull(data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)).get(0);
                if (speechText.toLowerCase().contains("tomorrow") && speechText.toLowerCase().contains("reminder")) {
                    String time = "";
                    String removeTime = "";

                    time = getTimeSpeechText(speechText);
                    removeTime = getTimeSpeechText(speechText);

                    /*use to get the time Formate*/
                    String timeFormate = "";
                    if (speechText.toLowerCase().contains("pm")) {
                        removeTime = removeTime + " " + "pm";
                        timeFormate = Common.getPm();
                    } else if (speechText.toLowerCase().contains("p.m")) {
                        removeTime = removeTime + " " + "p.m.";
                        timeFormate = Common.getPm();
                    } else if (speechText.toLowerCase().contains("am")) {
                        removeTime = removeTime + " " + "am";
                        timeFormate = Common.getAm();
                    } else if (speechText.toLowerCase().contains("a.m")) {
                        removeTime = removeTime + " " + "a.m.";
                        timeFormate = Common.getAm();
                    }

                    /*time form speech text*/
                    time = time + " " + timeFormate;
                    System.out.println("time_match " + ": " + time);

                    Intent intent2 = new Intent(getActivity(), AddReminder.class);
                    intent2.putExtra(Common.key.isFrom, Common.key.addReminder);
                    intent2.putExtra(Common.key.date, Common.getTomorrowDate());

                    intent2.putExtra(Common.key.time, time);
                    intent2.putExtra(Common.key.remindData, (((((((((speechText.replace("tomorrow", "")
                            .replace(removeTime, ""))
                            .replace("Add a reminder", ""))
                            .replace("Add reminder", ""))
                            .replace("add a reminder", ""))
                            .replace("add reminder", ""))
                            .replace("add reminder", "")
                            .replace("reminder", ""))
                            .replace("Reminder", ""))
                            .replace("for", ""))
                            .replace("at", "")).trim()
                    );
                    startActivityForResult(intent2, 23);
                    getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                } else if (speechText.toLowerCase().contains("reminder")) {/*use to add reminder*/
                    String monthDate = "";
                    String year = "";
                    String time = "";
                    String removeTime = "";
                    String removeMonth = "";
                    String removeEnd = "";
                    String removeYear = "";
                    /*get the time From speech text*/
                    time = getTimeSpeechText(speechText);
                    removeTime = getTimeSpeechText(speechText);

                    /*use to get the time Formate*/
                    String timeFormate = "";
                    if (speechText.toLowerCase().contains("pm")) {
                        removeTime = removeTime + " " + "pm";
                        timeFormate = Common.getPm();
                    } else if (speechText.toLowerCase().contains("p.m")) {
                        removeTime = removeTime + " " + "p.m.";
                        timeFormate = Common.getPm();
                    } else if (speechText.toLowerCase().contains("am")) {
                        removeTime = removeTime + " " + "am";
                        timeFormate = Common.getAm();
                    } else if (speechText.toLowerCase().contains("a.m")) {
                        removeTime = removeTime + " " + "a.m.";
                        timeFormate = Common.getAm();
                    }

                    /*time form speech text*/
                    time = time + " " + timeFormate;
                    System.out.println("time_match " + ": " + time);



                    /*get the date and month*/
                    String monthDateEnd = getMonthDateEnd(speechText);
                    String monthDateStart = getMonthDateStart(speechText);
                    System.out.println("time_match_month_end " + ": " + monthDateEnd);
                    System.out.println("time_match_month_start " + ": " + monthDateStart);


                    /*use to get the year*/
                    year = getYear(speechText);
                    removeYear = getYear(speechText);


                    if (isMonth(monthDateStart)) {
                        monthDate = Common.formatDateStart(monthDateStart + " " + year);
                        removeMonth = monthDateStart;
                    } else if (isMonth(monthDateEnd)) {
                        monthDate = Common.formatDateEnd(monthDateEnd + " " + year);
                        removeMonth = Common.formatDateEnd(monthDateEnd + " " + year);
                    }
                    removeEnd = Common.formatDateEnd(monthDateEnd + " " + year);


                    Intent intent2 = new Intent(getActivity(), AddReminder.class);
                    intent2.putExtra(Common.key.isFrom, Common.key.addReminder);
                    if (!monthDate.isEmpty()) {
                        intent2.putExtra(Common.key.date, monthDate);
                    }
                    intent2.putExtra(Common.key.time, time);


                    intent2.putExtra(Common.key.remindData, (((((((((((((speechText.replace(removeTime, ""))
                            .replace(removeMonth, ""))
                            .replace(removeYear, ""))
                            .replace(removeEnd, ""))
                            .replace(removeTime, ""))
                            .replace("Add a reminder", ""))
                            .replace("Add reminder", ""))
                            .replace("add a reminder", ""))
                            .replace("add reminder", ""))
                            .replace("reminder", ""))
                            .replace("Reminder", ""))
                            .replace("for", ""))
                            .replace("at", "")).trim()
                    );
                    startActivityForResult(intent2, 23);
                    getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                } else if (speechText.toLowerCase().contains("password") && speechText.toLowerCase().contains("save") && !speechText.toLowerCase().contains("saved")) {
                    Intent showPassword = new Intent(getActivity(), AddSavedPassword.class);
                    showPassword.putExtra(Common.key.description, speechText);
                    startActivity(showPassword);
                } else if (speechText.toLowerCase().contains("password")) {
                    Intent showPassword = new Intent(getActivity(), SavedPasswordListActivity.class);
                    showPassword.putExtra(Common.key.isFrom, Common.key.audioSearch);
                    showPassword.putExtra(Common.key.data, speechText);
                    startActivity(showPassword);
                } else if (speechText.toLowerCase().contains("last") && speechText.toLowerCase().contains("message")) {
                    Intent showPassword = new Intent(getActivity(), LastMessageActivity.class);
                    showPassword.putExtra(Common.key.isFrom, Common.key.audioSearch);
                    showPassword.putExtra(Common.key.data, speechText);
                    startActivity(showPassword);
                } else if (speechText.toLowerCase().contains("youtube")) {
                    Intent showPassword = new Intent(getActivity(), YoutubePlayList.class);
                    showPassword.putExtra(Common.key.description, speechText);
                    startActivity(showPassword);
                }

                /*use for the searching*/
//                Intent intent2 = new Intent(getActivity(), AddReminder.class);
//                startActivityForResult(intent2, 23);
//                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
//                llSearch.setVisibility(View.VISIBLE);
//                txtSearch.setText(speechText);
//                callApiSearchReminder(speechText);
            } else {

                Common.showToast(getActivity(), "Try Again");
            }
        } else if (resultCode == 141 && requestCode == 23) {/*use to refresh the list if ew remider add*/
            if (adapter != null) {
                adapter.removeAll();
            }
            reminderListModel = null;
            page = 1;
            loadAgain = false;
            callAPIListReminder();
        }
    }

    private String getYear(String speechText) {
        final String regex = "\\d+";
        String dateMonth = "";

        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(speechText);
        while (matcher.find()) {
//            System.out.println("Full match: " + matcher.group(0));
            dateMonth = matcher.group(0);
            assert dateMonth != null;
            if (dateMonth.length() == 4) {
                return dateMonth;
            }

            for (int i = 1; i <= matcher.groupCount(); i++) {
//                System.out.println("Group " + i + ": " + matcher.group(i));
                dateMonth = i + ": " + matcher.group(i);
            }
        }
        if (dateMonth.length() != 4) {
            return Common.getCurrentYear();
        } else {
            return dateMonth;
        }
    }

    private String getMonthDateEnd(String speechText) {
        final String regex = "\\w+\\s+\\d+";
        String dateMonth = "";

        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(speechText);
        while (matcher.find()) {
//            System.out.println("Full match: " + matcher.group(0));
            dateMonth = matcher.group(0);
            assert dateMonth != null;
            if (isMonth(dateMonth)) {
                return dateMonth;
            }

            for (int i = 1; i <= matcher.groupCount(); i++) {
//                System.out.println("Group " + i + ": " + matcher.group(i));
                dateMonth = i + ": " + matcher.group(i);
            }
        }
        return dateMonth;
    }

    /*use to check that string have month*/
    private boolean isMonth(String text) {
        if (text.toLowerCase().contains("jan")) {
            return true;
        } else if (text.toLowerCase().contains("feb")) {
            return true;
        } else if (text.toLowerCase().contains("mar")) {
            return true;
        } else if (text.toLowerCase().contains("apr")) {
            return true;
        } else if (text.toLowerCase().contains("may")) {
            return true;
        } else if (text.toLowerCase().contains("jun")) {
            return true;
        } else if (text.toLowerCase().contains("jul")) {
            return true;
        } else if (text.toLowerCase().contains("aug")) {
            return true;
        } else if (text.toLowerCase().contains("sep")) {
            return true;
        } else if (text.toLowerCase().contains("oct")) {
            return true;
        } else if (text.toLowerCase().contains("nov")) {
            return true;
        } else return text.toLowerCase().contains("dec");
    }

    private String getMonthDateStart(String speechText) {
        final String regex = "\\d+\\s+\\w+";
        String dateMonth = "";

        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(speechText);
        while (matcher.find()) {
//            System.out.println("Full match: " + matcher.group(0));
            dateMonth = matcher.group(0);
            assert dateMonth != null;
            if (isMonth(dateMonth)) {
                return dateMonth;
            }

            for (int i = 1; i <= matcher.groupCount(); i++) {
//                System.out.println("Group " + i + ": " + matcher.group(i));
                dateMonth = i + ": " + matcher.group(i);
            }
        }
        return dateMonth;
    }

    private String getTimeSpeechText(final String speechText) {
        final String regex = "\\d+:\\d+";
        String time = "";

        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(speechText);

        while (matcher.find()) {
//            System.out.println("time_match" + matcher.group(0));
            time = matcher.group(0);
//            for (int i = 1; i <= matcher.groupCount(); i++) {
//                System.out.println("time_match " + i + ": " + matcher.group(i));
//                time = i + ": " + matcher.group(i);

//            }
        }
        return time;
    }

    /*use to add reminder*/
    private void callAPIListReminder() {
        HashMap<String, String> request = new HashMap<>();
        request.put(ApiClass.PAGE, String.valueOf(page));
        request.put(ApiClass.timeZone, Common.getCurrentTimeZone());

        new RetroService().call((BaseView) getActivity(), MainApp.getInstance().getApiServices().reminderList(request),
                result -> {
                    if (result.isStatus()) {
                        if (reminderListModel == null) {
                            reminderListModel = result.getData();
                            if (reminderListModel.getData().size() == 0) {
                                binding.txtNoData.setVisibility(View.VISIBLE);
                                binding.rv.setVisibility(View.GONE);
//                                imgRecord.setVisibility(View.GONE);
                            } else {
                                binding.txtNoData.setVisibility(View.GONE);
                                binding.rv.setVisibility(View.VISIBLE);
                                reminderAdapter();
                                loadAgain = true;

                            }
                        } else {
                            binding.txtNoData.setVisibility(View.GONE);
                            binding.rv.setVisibility(View.VISIBLE);
                            ReminderListModel reminderListModel1 = result.getData();
                            if (reminderListModel1.getData().size() == 0) {
                                loadAgain = false;
                            } else {
                                adapter.add(reminderListModel1.getData());
                            }
                        }
                    } else {
                        Common.showToast(getActivity(), result.getMessage());
                    }
                });
    }


    /*use to search reminder*/
    private void callApiSearchReminder(String search) {
        HashMap<String, String> request = new HashMap<>();
        request.put(ApiClass.SEARCH, search);
        new RetroService().call((BaseView) getActivity(), MainApp.getInstance().getApiServices().reminderList(request),
                result -> {
                    if (result.isStatus()) {
                        ReminderListModel searchreminderListModel = result.getData();
                        if (searchreminderListModel.getData().size() == 0) {
                            binding.txtNoData.setText("No Search Result Found..");
                            binding.txtNoData.setVisibility(View.VISIBLE);
                            binding.rv.setVisibility(View.GONE);
                            binding.rvSearch.setVisibility(View.GONE);
                        } else {
                            binding.txtNoData.setVisibility(View.GONE);
                            binding.rv.setVisibility(View.GONE);
                            binding.rvSearch.setVisibility(View.VISIBLE);
                            searchReminderAdapter(searchreminderListModel);

                        }
                    } else {
                        Common.showToast(getActivity(), result.getMessage());
                    }
                });
    }

    private void initScrollListener() {
        binding.rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == reminderListModel.getData().size() - 1) {
                        Log.d("lastitem", "visible");
                        if (loadAgain) {
                            page = page + 1;
                            callAPIListReminder();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


    }

    /*search reminder adapter*/
    private void searchReminderAdapter(ReminderListModel searchreminderListModel) {
        searchAdapter = new GeneralAdapter<>(R.layout.row_search_reminder, searchreminderListModel.getData(), (view, object) -> {
            ReminderListModel.DataBean remiderSearchModel = (ReminderListModel.DataBean) object;
            switch (view.getId()) {
                case R.id.checkbox:
                    searchAdapter.remove(remiderSearchModel);

//                    if (remiderSearchModel.getStatus() == 0) {/*update status if status is pedig*/
//                        HashMap<String, String> request = new HashMap<>();
//                        request.put(ApiClass.REMINDER_ID, remiderSearchModel.getId() + "");
//                        request.put(ApiClass.STATUS, "1");
//                        new RetroService().call((BaseView) getActivity(), MainApp.getInstance().getApiServices().updateStaus(request),
//                                result -> {
//                                    Common.showToast(getActivity(), result.getMessage());
//                                    remiderSearchModel.setStatus(1);
//                                    /*use to update the status i9 ew list*/
//                                    for (int i = 0; i < reminderListModel.getData().size(); i++) {
//                                        if (remiderSearchModel.getId() == reminderListModel.getData().get(i).getId()) {
//                                            reminderListModel.getData().get(i).setStatus(1);
//
//                                        }
//                                    }
//                                });
//                    }


                    break;
                case R.id.llAll:
                    Intent intent = new Intent(getActivity(), AddReminderDetailActivity.class);
                    intent.putExtra(Common.key.remindData, remiderSearchModel);
                    startActivity(intent);
                    break;
//                case R.id.imgDelete:
////                    remove(remiderSearchModel);
//                    break;

            }

        });
        binding.rvSearch.setAdapter(searchAdapter);
        binding.rvSearch.setLayoutManager(new LinearLayoutManager(getContext()));

    }


    /*reminder adapter*/
    private void reminderAdapter() {
        adapter = new GeneralAdapter<>(R.layout.row_reminder, reminderListModel.getData(), (view, object) -> {
            ReminderListModel.DataBean reminderListModel = (ReminderListModel.DataBean) object;
            switch (view.getId()) {
                case R.id.checkbox:
//                    if (reminderListModel.getStatus() == 0) {/*update status if status is pedig*/
//                        HashMap<String, String> request = new HashMap<>();
//                        request.put(ApiClass.REMINDER_ID, reminderListModel.getId() + "");
//                        request.put(ApiClass.STATUS, "1");
//                        new RetroService().call((BaseView) getActivity(), MainApp.getInstance().getApiServices().updateStaus(request),
//                                result -> {
//                                    Common.showToast(getActivity(), result.getMessage());
//                                    reminderListModel.setStatus(1);
//                                });
//                    }
                    break;
                case R.id.llAll:
                    Intent intent = new Intent(getActivity(), AddReminderDetailActivity.class);
                    intent.putExtra(Common.key.remindData, reminderListModel);
                    startActivity(intent);
                    break;
                case R.id.imgEdt:
                    Intent intent5 = new Intent(getActivity(), EditReminderActivity.class);
                    intent5.putExtra(Common.key.remindData, reminderListModel);
                    startActivityForResult(intent5, 23);
                    getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                    // do something here
                    break;

                case R.id.imgDelete:


                    //before inflating the custom alert dialog layout, we will get the current activity viewgroup
                    //ViewGroup viewGroup = Objects.requireNonNull(getView()).findViewById();
                    ViewGroup viewGroup = getView().findViewById(android.R.id.content);

                    //then we will inflate the custom alert dialog xml that we created
                    final View dialog = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_delete, viewGroup, false);

                    //Now we need an AlertDialog.Builder object
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity(), R.style.CustomAlertDialogPassword);

                    //setting the view of the builder to our custom view that we already inflated
                    builder.setView(dialog);

                    Button btnYes = dialog.findViewById(R.id.btnYes);
                    Button btnNo = dialog.findViewById(R.id.btnCancel);

                    //finally creating the alert dialog and displaying it
                    final android.app.AlertDialog Dialog = builder.create();
                    Dialog.show();
                    Dialog.setCancelable(false);
                    Dialog.setCanceledOnTouchOutside(false);

                    btnYes.setOnClickListener(v -> {
                        HashMap<String, String> request = new HashMap<>();
                        request.put(ApiClass.reminder_id, String.valueOf(reminderListModel.getId()));
                        if (reminderListModel.getUser_id() != userResponse.getData().getId()) {
                            request.put(ApiClass.other_user_id, String.valueOf(userResponse.getData().getId())); }
                        new RetroService().call((BaseView) getActivity(), MainApp.getInstance().getApiServices().deleteApi(request),
                                result -> {
                                    Common.showToast(getActivity(), result.getMessage());
                                    adapter.remove(reminderListModel);

                                });
                        Dialog.dismiss();
                    });

                    btnNo.setOnClickListener(v -> Dialog.dismiss());
                    break;
            }

        });
        binding.rv.setAdapter(adapter);
        binding.rv.setLayoutManager(new LinearLayoutManager(getContext()));

    }


//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case iv_userprofile:
//                Intent intent = new Intent(getActivity(), ProfileActivity.class);
//                startActivity(intent);
//                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
//                // do something here
//                break;
//            case R.id.fltButton:
//                Intent intent2 = new Intent(getActivity(), AddReminder.class);
//                startActivityForResult(intent2, 23);
//                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
//                // do something here
//                break;
//
//            case R.id.imgRecord:
//                Common.voiceTyping(getActivity());
//                break;
//            case R.id.imgClear:
//                binding.llSearch.setVisibility(View.GONE);
//                binding.rvSearch.setVisibility(View.GONE);
//                binding.txtNoData.setVisibility(View.GONE);
//                binding.rv.setVisibility(View.VISIBLE);
//                break;
//
//
//            default:
//                throw new IllegalStateException("Unexpected value: " + view.getId());
//        }
//    }

    @Override
    public void onItemClick(View view, Object object) {
        switch (view.getId()) {
            case R.id.iv_userprofile:
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                // do something here
                break;
            case R.id.fltButton:
                Intent intent2 = new Intent(getActivity(), AddReminder.class);
                startActivityForResult(intent2, 23);
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                // do something here
                break;


            case R.id.imgRecord:
                Common.voiceTyping(getActivity());
                break;
            case R.id.imgClear:
                binding.llSearch.setVisibility(View.GONE);
                binding.rvSearch.setVisibility(View.GONE);
                binding.txtNoData.setVisibility(View.GONE);
                binding.rv.setVisibility(View.VISIBLE);
                break;


            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }
}
