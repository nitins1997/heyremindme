package com.reminder.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.reminder.app.R;
import com.reminder.app.activity.AddReminder;
import com.reminder.app.activity.AddReminderDetailActivity;
import com.reminder.app.adapter.CalendarAdapter;
import com.reminder.app.callback.OnItemClickListener;
import com.reminder.app.databinding.FragmentCalendarBinding;
import com.reminder.app.models.ReminderListModel;
import com.reminder.app.network.ApiClass;
import com.reminder.app.utils.BaseView;
import com.reminder.app.utils.Common;
import com.reminder.app.utils.MainApp;
import com.reminder.app.webserviceRetro.RetroService;
import com.shrikanthravi.collapsiblecalendarview.data.Day;
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;





public class FragmentCalendar extends Fragment {



    public FragmentCalendarBinding binding;
    String dateAddRemider;
    String currentDate;
    CalendarAdapter generalAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_calendar, container, false);



        final CollapsibleCalendar collapsibleCalendar = binding.getRoot().findViewById(R.id.calendarView);
        collapsibleCalendar.setCalendarListener(new CollapsibleCalendar.CalendarListener() {
            @Override
            public void onDayChanged() {

            }

            @Override
            public void onClickListener() {

            }

            @Override
            public void onDaySelect() {
                Day day = collapsibleCalendar.getSelectedDay();
                Log.i(getClass().getName(), "Selected Day: "
                        + day.getYear() + "/" + (day.getMonth() + 1) + "/" + day.getDay());

                dateAddRemider = Common.FormatDate(day.getYear() + "/" + (day.getMonth() + 1) + "/" + day.getDay());
                binding.txtDate.setText(Common.FormatDate(day.getYear() + "/" + (day.getMonth() + 1) + "/" + day.getDay()));

                callApiSearchReminder(day.getYear() + "-" + (day.getMonth() + 1) + "-" + day.getDay());

            }

            @Override
            public void onItemClick(View view) {

            }

            @Override
            public void onDataUpdate() {

            }

            @Override
            public void onMonthChange() {

            }

            @Override
            public void onWeekChange(int i) {

            }
        });

        GregorianCalendar today = new GregorianCalendar();
        binding.txtDate.setText(Common.FormatDate(today.get(Calendar.YEAR) + "/" + (today.get(Calendar.MONTH) + 1) + "/" + today.get(Calendar.DAY_OF_MONTH)));
        dateAddRemider = Common.FormatDate(today.get(Calendar.YEAR) + "/" + (today.get(Calendar.MONTH) + 1) + "/" + today.get(Calendar.DAY_OF_MONTH));
        currentDate = Common.FormatDate(today.get(Calendar.YEAR) + "/" + (today.get(Calendar.MONTH) + 1) + "/" + today.get(Calendar.DAY_OF_MONTH));

        callApiSearchReminder(today.get(Calendar.YEAR) + "-" + (today.get(Calendar.MONTH) + 1) + "-" + today.get(Calendar.DAY_OF_MONTH));


        /*add reminder to event*/
        binding.imgAdd.setOnClickListener(view1 -> {
            try {
                if (currentDate.equals(dateAddRemider)) {
                    startActivityForResult(new Intent(getActivity(), AddReminder.class).putExtra(Common.key.date, dateAddRemider), 23);
                } else if (new SimpleDateFormat("EEEE, MMM dd, yyyy").parse(dateAddRemider).after(new Date())) {
                    startActivityForResult(new Intent(getActivity(), AddReminder.class).putExtra(Common.key.date, dateAddRemider), 23);
                } else {
                    Common.showToast(getActivity(), "You can't add previous date reminder.");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        });

        binding.llSearch.setOnClickListener(view1 -> {
            try {
                if (currentDate.equals(dateAddRemider)) {
                    startActivityForResult(new Intent(getActivity(), AddReminder.class).putExtra(Common.key.date, dateAddRemider), 23);
                } else if (new SimpleDateFormat("EEEE, MMM dd, yyyy").parse(dateAddRemider).after(new Date())) {
                    startActivityForResult(new Intent(getActivity(), AddReminder.class).putExtra(Common.key.date, dateAddRemider), 23);
                } else {
                    Common.showToast(getActivity(), "You can't add previous date reminder.");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        });

        /*clisck*/
        clicks();
        return binding.getRoot();
    }

    private void clicks() {
        binding.llSearch.setOnClickListener(view -> {
            binding.llSearchTop.setVisibility(View.VISIBLE);
        });
        binding.imgCloseSearch.setOnClickListener(view -> {
            binding.llSearchTop.setVisibility(View.GONE);
            if (generalAdapter != null) {
                binding.edtSearchText.setText("");
                generalAdapter.notifyDataSetChanged();

            }

            try {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                //Find the currently focused view, so we can grab the correct window token from it.
                //If no view currently has focus, create a new one, just so we can grab a window token from it
                if (view == null) {
                    view = new View(getActivity());
                }
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }catch (Exception e){
                e.printStackTrace();
            }
        });

        binding.edtSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (generalAdapter != null) {
                    generalAdapter.getFilter().filter(charSequence);
                    generalAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 141 && requestCode == 23) {/*use to refresh the list if ew remider add*/
            callApiSearchReminder(Common.FormatDateApi(dateAddRemider));
        }
    }

    /*use to search reminder*/
    private void callApiSearchReminder(String date) {
        HashMap<String, String> request = new HashMap<>();
        request.put(ApiClass.DATE, Common.formatPollDateNew(date));
        request.put(ApiClass.timeZone, Common.getCurrentTimeZone());
        new RetroService().call((BaseView) getActivity(), MainApp.getInstance().getApiServices().reminderList(request),
                result -> {
                    if (result.isStatus()) {
                        ReminderListModel searchreminderListModel = result.getData();
                        if (searchreminderListModel.getData().size() == 0) {
//                            txtNoData.setText("No Search Result Found..");
                            binding.txtNoData.setVisibility(View.VISIBLE);
                            binding.recyclerView.setVisibility(View.GONE);
                        } else {
                            binding.txtNoData.setVisibility(View.VISIBLE);
                            binding.recyclerView.setVisibility(View.VISIBLE);

                            generalAdapter = new CalendarAdapter(this, searchreminderListModel.getData(), searchreminderListModel, new OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, Object object) {
                                    ReminderListModel.DataBean reminderListModel = (ReminderListModel.DataBean) object;
                                    switch (view.getId()) {
                                        case R.id.clAll:
                                            Intent intent = new Intent(getActivity(), AddReminderDetailActivity.class);
                                            intent.putExtra(Common.key.remindData, reminderListModel);
                                            startActivity(intent);
                                            break;
                                    }
                                }
                            });
                            binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            binding.recyclerView.setAdapter(generalAdapter);
                        }
                    } else {
                        Common.showToast(getActivity(), result.getMessage());
                    }
                });
    }
}